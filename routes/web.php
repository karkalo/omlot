<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::post('comment', 'HomeController@storeComment')->name('comment');
Route::post('contacts', 'HomeController@storeContact')->name('contact');
Route::post('newsletter', 'HomeController@storeNewsLetter')->name('newsletter');
Route::get('sitemap', 'HomeController@sitemap')->name('sitemap');
Route::get('sitemap.xml', 'HomeController@sitemap')->name('sitemap');

Auth::routes();
Route::get('/backup', 'HomeController@backup')->middleware('auth'); 
Route::get('/refresh', 'HomeController@refresh')->middleware('auth');
Route::group(['middleware' => ['auth', 'permission'], 'prefix' => 'backend', 'namespace' => 'Backend'], function() {

	Route::get('/', 'DashboardController@index')->name('backend.dashboard');
	Route::resource('category', 'CategoryController');
	Route::resource('page', 'PageController');
	Route::resource('post', 'PostController');
	Route::resource('gallery', 'GalleryController');
	Route::resource('user', 'UserController');
	Route::resource('role', 'RoleController');
	Route::resource('faq', 'FaqController');
	Route::resource('partner', 'PartnerController');
	Route::resource('testimonial', 'TestimonialController');
  Route::resource('job', 'JobController');
  Route::resource('worker', 'WorkerController');
  Route::resource('order', 'OrderController');
	Route::get('contact', 'DashboardController@allContact')->name('contact.index');
	Route::get('contact/{id}', 'DashboardController@getContact')->name('contact.show');
	Route::get('comment', 'DashboardController@allComment')->name('comment.index');
	Route::get('comment/{id}', 'DashboardController@getComment')->name('comment.show');
	Route::get('setting', 'SettingController@index')->name('setting.index');
	Route::post('setting', 'SettingController@store')->name('setting.store');
});
/*Front Route*/
Route::get('/{slug}', 'RouteController@index')->name('url');


