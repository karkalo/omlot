<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Worker;
use App\Models\User;

class WorkerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = Worker::paginate(10);
        return view('backend.workers.index',['workers' => $workers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $users = User::pluck('email','id');
        $users->prepend('Select User', '');
        return view('backend.workers.create', ['users'=> $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateFormData($request);
        $new_worker = Worker::create($validatedData);

        if(!$new_worker){
          App::abort(500, 'Error');
        }
        return redirect('/backend/worker/'.$new_worker->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $worker = Worker::find($id);
        return view('backend.workers.show', ['worker' => $worker ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $worker = Worker::find($id);
        $users = User::pluck('email','id');
        $users->prepend('Select User', '');
        return view('backend.workers.edit', ['worker' => $worker, 'users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $worker = Worker::find($id);
        $validatedData = $this->validateFormData($request);
        $worker->update($validatedData);
        return view('backend.workers.show', ['worker' => $worker]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $worker = Worker::find($id);
        $worker->delete();
        return redirect('/backend/worker');
    }

    private function validateFormData($request)
    {
      return $request->validate([
        'user_id' => 'required',
        'hour_rate' => 'nullable',
        'working_hour_per_day' => 'nullable',
        'working_day_per_month' => 'nullable',
        'availability' => 'nullable',
        'rate_type' => 'nullable',
        'slug' => 'nullable',
        'status' => 'nullable'
      ]);
    }
}
