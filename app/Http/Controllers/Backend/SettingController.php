<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Setting $setting)
    {
        $this->middleware('auth');
        $this->setting = $setting;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::get();
        return view('backend.setting', compact('settings'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        if($request->has('update')) {
            foreach($request->all() as $key => $value) {
                $setting= '';
                $setting = Setting::where('key', $key)->first();
                if($setting) {
                    $slug = str_slug($setting->key);
                    if($setting->type == "file") {
                        if ($image = $request->file($setting->key.'-file')) {
                            $ext =  $image->getClientOriginalExtension();
                            $file_path ='/setting';
                            $name = $slug.'.'.$ext;
                            $path = $image->storeAs($file_path, $name, 'upload');
                            $value = asset('storage/'.$path);
                        }
                    }
                    $setting->update(['value' => $value]);
                }
            }
            return back()->with('success', 'Setting updated successfully');
        }
        $this->validate($request, ['key' => 'required|unique:settings', 'label' => 'required|unique:settings']);
        $slug = str_slug($request->key);
        if($request->type == 'file' && $request->file) {
            if ($request->has('file')) {
                $image = $request->file('file');
                $ext =  $image->getClientOriginalExtension();
                $file_path ='/setting';
                $name = $slug.'.'.$ext;
                $path = $image->storeAs($file_path, $name, 'upload');
                $request['value'] = asset('storage/'.$path);
            }
        }
        Setting::create($request->all());
        return back()->with('success', 'Setting create successfully');
    }
}
