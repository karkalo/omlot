<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Partner;
use Image;

class PartnerController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::active()->get();
        return view('backend.partner.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $this->validate($request, ['name' => 'required', 'file' => 'required|image|mimes:jpg,png,jpeg,gif|max:2048']);


        $request['slug'] = str_slug($request->name);

        if ($request->has('file')) {
            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/partner';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['logo'] = $path;
            
        }
        $request['link'] = $request->link ? $request->link : url('/');
  
        Partner::create($request->all());
        return redirect()->route('partner.index')->with('success', 'Partner created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partner::find($id);
        return view('backend.partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //return $request->all();
        $partner = Partner::find($id);
        $this->validate($request, ['name' => 'required|unique:partners,name,'.$partner->id, 'images' => 'image|mimes:jpg,png,jpeg,gif']);
         $slug = str_slug( $request->name, "-" );


        if ($request->has('file')) {
            if(file_exists($partner->logo) ) {
                unlink($partner->logo);
            }

            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/partner';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['logo'] = $path;
           
        }
        $request['is_active'] = $request->is_active ? 1 : 0;
        $request['link'] = $request->link ? $request->link : url('/');
        //return $request->all();
        
        $partner->update($request->all());
        return redirect()->route('partner.index')->with('success', ' Partner updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Partner::find($id);
         if(file_exists($partner->logo) ) {
                unlink($partner->logo);
            }
        $ad->delete();
        return back()->with('success', 'Partner deleted successfully');
    }
}
