<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Comment;

class DashboardController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.dashboard');
    }


    public function allContact()
    {
        $contacts = Contact::get();
        return view('backend.contact.index', compact('contacts'));
    }

    public function getContact($id)
    {
        $contact = Contact::find($id);
        $contact->is_viewed = 1;
        $contact->save();
        return view('backend.contact.show', compact('contact'));
    }
    public function allComment()
    {
        $comments = Comment::get();
        return view('backend.comment.index', compact('comments'));
    }

    public function getComment($id)
    {
        $comment = Comment::find($id);
        $comment->is_active = 1;
        $comment->save();
        return view('backend.comment.show', compact('comment'));
    }
}
