<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Category;
use App\Http\Controllers\SeoController;

class PageController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Page $page, Category $category)
    {
        $this->middleware('auth');
        $this->page = $page;
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = $this->page->latest()->get();
        return view('backend.page.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->selectCategory();
        return view('backend.page.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required|unique:seos', 'file' => 'image|mimes:jpg,png,jpeg|max:2048','description' => 'max:240']);
        $request['slug'] = str_slug($request->title);
        if ($request->has('file')) {

            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/pages';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }

        $page = $this->page->create($request->all());
        $seo = SeoController::create($page->type, $page);
        return redirect()->route('page.index')->with('success', 'Page created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return redirect($page->slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->page->find($id);
        $categories = $this->category->selectCategory();
        return view('backend.page.edit', compact('page', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $page = $this->page->find($id);
      $this->validate($request, ['title' => 'required|unique:pages,title,'.$page->id, 'file' => 'image|mimes:jpg,png,jpeg|max:2048','description' => 'max:240']);

      $request['slug'] = str_slug($request->title);

       if ($request->has('file')) {
            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/pages';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        $request['is_active'] = $request->is_active ? 1 : 0;
        $request['is_menu'] = $request->is_menu ? 1 : 0;
        $request['is_featured'] = $request->is_featured ? 1 : 0;
        $page->update($request->all());
        //return $page;
        $seo = SeoController::update($page->type,$page);
        return redirect()->route('page.index')->with('success', 'Page updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = $this->page->find($id);
        $seo = SeoController::delete($page);
        $page->delete();
        return back()->with('success', 'Page deleted successfully');
    }
}
