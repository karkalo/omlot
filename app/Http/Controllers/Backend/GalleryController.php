<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Http\Controllers\SeoController;
use Image;

class GalleryController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Gallery $gallery)
    {
        $this->middleware('auth');
        $this->gallery = $gallery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $galleries = $this->gallery->latest()->get();
       return view('backend.gallery.index', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $this->validate($request, ['title' => 'required|unique:galleries', 'file' => 'required|mimes:jpg,png,jpeg|max:2048']);
        $request['slug'] = str_slug($request->title);

        if ($request->has('file')) {
            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/gallery';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        // Video from Youtube
        $request['link'] = $request->link ? $request->link : url('/');
        $gallery = $this->gallery->create($request->all());
        $seo = SeoController::create('gallery', $gallery);
        return redirect()->route('gallery.index')->with('success', 'Gallery created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = $this->gallery->find($id);
        return view('backend.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = $this->gallery->find($id);
        $this->validate($request, ['title' => 'required|unique:galleries,title,'.$gallery->id, 'file' => 'image|mimes:jpg,png,jpeg|max:2048']);
        $request['slug'] = str_slug($request->title);

        if ($request->has('file')) {

            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/gallery';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        $request['is_active'] = $request->is_active ? 1 : 0;
        $request['is_slider'] = $request->is_slider ? 1 : 0;
        $request['is_featured'] = $request->is_featured ? 1 : 0;
        $request['link'] = $request->link ? $request->link : url('/');

        $gallery->update($request->all());
        $seo = SeoController::update('gallery', $gallery);
        return redirect()->route('gallery.index')->with('success', 'Gallery updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = $this->gallery->find($id);
        $gallery->delete();
        return back()->with('success', 'Gallery deleted successfully');
    }

    public function uploadFile(Request $request)
    {
       $this->validate($request, ['file'=>'mimes:jpeg,gif,png']); 

        if ($request->hasFile('file')) {

            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_name = $image->getClientOriginalName();
            $slug = str_slug($file_name);
            $file_path ='/gallery';
            $name = $slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        $request['title'] = $file_name;
        $request['slug'] = $slug;
        $request['is_active'] = 1;
        $gallery =  Gallery::create($request->all());
        return response()->json($gallery);
    }

    public function uploadUrl(Request $request)
    {
        $this->validate($request, ['image_url'=>'required|url']); 
       
        $path = $request->image_url;
        $filename = basename($path);
        if (file_exists(public_path() . '/uploads/gallery/'.$filename)) {
                 $filename = time().$filename;
        }
        $image_path = '/uploads/gallery/'.$filename;
        $img = Image::make($path)->save(public_path().$image_path);
        $img->resize(200,200)->save(public_path().$thumbnail_path);
        $data['title'] = $filename;
        $data['image'] =  $image_path;
        $request['is_active'] = 1;
        $gallery = Gallery::create($data);
        return response ()->json ( $gallery );
    }
}
