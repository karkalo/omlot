<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Controllers\SeoController;

class CategoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Category $category)
    {
        $this->middleware('auth');
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->latest()->get();
        return view('backend.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required|unique:seos', 'file' => 'image|mimes:jpg,png,jpeg,gif|max:2048','description' => 'max:240']);

        $request['slug'] = str_slug($request->title);

        if ($request->has('file')) {

            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/categories';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }

        $category = $this->category->create($request->all());
        $seo = SeoController::create('category', $category);

        return redirect()->route('category.index')->with('success', 'Category created successfully');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
       return redirect($category->slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);
        return view('backend.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = $this->category->find($id);
        $this->validate($request, ['title' => 'required|unique:categories,title,'.$category->id, 'file' => 'image|mimes:jpg,png,jpeg,gif|max:2048','description' => 'max:240']);

        $request['slug'] = str_slug($request->title);
        if ($request->has('file')) {
            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/categories';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        $request['is_active'] = $request->is_active ? 1 : 0;
        $request['is_menu'] = $request->is_menu ? 1 : 0;
        $request['is_featured'] = $request->is_featured ? 1 : 0;
        $category->update($request->all());
        $seo = SeoController::update('category',$category);
        return redirect()->route('category.index')->with('success', 'Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->category->find($id);
        if(count($category->pages)) {
            return back()->withErrors('Sorry cannot remove category, this category is is use.');
        }
        $seo = SeoController::delete($category);
        $category->delete();
        return back()->with('success', 'Category deleted successfully');
    }
}
