<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user, Role $role)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->role = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->latest()->get();
        return view('backend.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->dropdown();
        return view('backend.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $this->validate($request, ['name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'passwords' => 'required|string|min:6|confirmed', 'role_id' => 'required']);

        $request['password'] = bcrypt($request->passwords);
        $this->user->create($request->all());
        return redirect()->route('user.index')->with('success', 'User created successfully');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);
        $roles = $this->role->dropdown();
        return view('backend.user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user->find($id);
        $rules = ['name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id, 'role_id' => 'required'];
        $this->validate($request, $rules);

        if($request->passwords || $request->passwords_confirmation) {

            $this->validate($request, ['passwords_confirmation' => 'required|string|min:6', 'passwords' => 'required|min:6:confirmed']);

            $request['password'] = bcrypt($request->passwords);
        }

        $request['is_active'] = $request->is_active ? 1 : 0;
        $user->update($request->all());
        return redirect()->route('user.index')->with('success', 'User updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->find($id);
        $user->delete();
        return back()->with('success', 'user deleted successfully');
    }
}
