<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Testimonial;

class TestimonialController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Testimonial $testimonial)
    {
        $this->middleware('auth');
        $this->testimonial = $testimonial;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = $this->testimonial->latest()->paginate(10);
        return view('backend.testimonials.index', compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:testimonials','position' => 'required', 'testimonial' => 'required', 'file' => 'required|image|mimes:jpg,png,jpeg|max:2048']);
        $request['slug'] = str_slug( $request->name, "-" );

        if ($request->has('file')) {
            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/testimonial';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['profile'] = $path;
        }
        
        $this->testimonial->create($request->all());
        return redirect()->route('testimonial.index')->with('success', 'Testimonial created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('backend.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {

        $this->validate($request, ['name' => 'required|unique:testimonials,name,'.$testimonial->id, 'file' => 'image|mimes:jpg,jpeg,png|max:3048']);
        $request['slug'] = str_slug( $request->name, "-" );
         if ($request->has('file')) {
            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/testimonial';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['profile'] = $path;
        }
        $request['is_active'] = $request->is_active ? 1 : 0;
        $testimonial->update($request->all());
        return redirect()->route('testimonial.index')->with('success', 'Testimonial updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy( Testimonial $testimonial)
    {
       $testimonial->delete();
       return back()->with('success', 'Testimonial deleted successfully');
    }
}
