<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Category;
use App\Http\Controllers\SeoController;

class PostController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Post $post, Category $category)
    {
        $this->middleware('auth');
        $this->post = $post;
        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->post->latest()->get();
        return view('backend.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->selectCategory();
        return view('backend.post.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required|unique:seos', 'file' => 'image|mimes:jpg,png,jpeg|max:2048','description' => 'max:240', 'category_id' => 'required']);

        $request['slug'] = str_slug($request->title);

        if ($request->has('file')) {

            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/posts';
            $name = $request->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        
        $request['user_id'] = $request->user()->id;

        $post = $this->post->create($request->all());
        $seo = SeoController::create('post', $post);
       
        return redirect()->route('post.index')->with('success', 'post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
         return redirect($post->slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post->find($id);
        $categories = $this->category->selectCategory();
        return view('backend.post.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $post = $this->post->find($id);
      $this->validate($request, ['title' => 'required|unique:posts,title,'.$post->id, 'file' => 'image|mimes:jpg,png,jpeg|max:2048','description' => 'max:240']);
      

       if ($request->has('file')) {

            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/posts';
            $name = $post->slug.'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        $request['is_active'] = $request->is_active ? 1 : 0;
        $request['is_featured'] = $request->is_featured ? 1 : 0;
        $post->update($request->all());
        $seo = SeoController::update('post',$post);
        return redirect()->route('post.index')->with('success', 'Post updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->post->find($id);
        if(count($post->comments)) {
            return back()->withErrors('Sorry cannot remove post, this post is is use.');
        }
        if(request()->user()->id != $post->user_id) {
            return back()->withErrors('Sorry cannot remove post, you arenot authorized person.');
        }
        $seo = SeoController::delete($post);
        $post->delete();
        return back()->with('success', 'Post deleted successfully');
    }
}
