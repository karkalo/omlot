<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seo;

class SeoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create($route = 'page', $object)
    {
        $data = array();
        $data['title'] = $object->title;
        $data['slug'] = str_slug($object->title);
        $data['description'] = $object->description;
        $data['keywords'] = $object->keyword;
        $data['route'] = $route;
        $data['url'] = url($data['slug']);
        $seo = Seo::create($data);
        $object->slug = $seo->slug;
        $object->save();
        return $seo;
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public static function update($route, $object)
    {
        $data['title'] = $object->title;
        $data['description'] = $object->description;
        $data['keywords'] = $object->keyword;
        $data['route'] = $route;

        if( $seo = Seo::whereSlug($object->slug)->first()) {
            $seo->update($data);
            return $seo;
        }
        self::create($route, $object);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public static function delete($object)
    {
        if( $seo = Seo::whereSlug($object->slug)->first()) {
            return $seo->delete();
        }
    }
}
