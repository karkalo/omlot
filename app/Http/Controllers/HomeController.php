<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partner;
use App\Models\Gallery;
use App\Models\Page;
use App\Models\Post;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Setting;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Seo;
use App\Models\Subscribe;
use App\Models\Testimonial;
use App\Models\Permission;
use App\Http\Controllers\SeoController;
use Route;
use Artisan;
use Mail;

class HomeController extends Controller
{
    /**
     * Show the application index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Gallery::active()->get();
        $about = Page::whereSlug('about')->first();
        $categories = Category::active()->menu()->latest()->limit(3)->get();
        $services = Page::active()->service()->limit(8)->get();
        $testimonials = Testimonial::active()->latest()->limit(3)->get();
        $blogs = Post::active()->latest()->limit(3)->get();
        $partners = Partner::active()->latest()->limit(4)->get();
        return view('home', compact('sliders','about', 'categories', 'testimonials', 'blogs', 'partners', 'services'));
    }

    public function sitemap()
    {
      $seos = Seo::active()->orderBy('updated_at', 'desc')->get();
      return response()->view('sitemap', compact('seos'))->header('Content-Type', 'application/xml');
    }

    public function storeComment(Request $request)
    {
        $this->validate($request, Comment::$rules);
        $request['ip_address'] = $request->ip();
        if ($request->has('file')) {
            $image = $request->file('file');
            $ext =  $image->getClientOriginalExtension();
            $file_path ='/comment';
            $name = date('Y-m-d h:i').'.'.$ext;
            $path = $image->storeAs($file_path, $name, 'upload');
            $request['image'] = $path;
        }
        $request['is_active'] = 0;
        $comment = Comment::create($request->all());
        $data = array('name' => 'Karkalo Pvt. Ltd.', 'email' => $request->email, 'comment' => $comment->comment);

        Mail::send('emails.message', $data, function($message) use ($data) {
            $message->to('karkalotech@gmail.com', 'Karkalo Pvt. Ltd.' )
                    ->subject($data['comment']);
            $message->from($data['email'], $data['comment']);
        });
        return back()->with('success', 'Thank you! Your request has been received. , we will respond you soon.');
    }

    public function storeContact(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|email', 'phone' => 'required', 'message' => 'required']);
        $request['ip_address'] = $request->ip();
        $contact = Contact::create($request->all());
        
        $data = array('name' => $contact->name, 'email' => $contact->email, 'message' => $contact->message);

        Mail::send('emails.message', $data, function($message) use ($data) {
            $message->to('karkalotech@gmail.com', 'Karkalo Pvt. Ltd.' )
                    ->subject($data['name']);
            $message->from($data['email'], $data['name']);
        });
        return back()->with('success', 'Thank you for contact us.');
    }

    public function storeNewsLetter(Request $request) 
    {
        $this->validate($request, ['email' => 'required|email|unique:subscribes']);
        $request['ip_address'] = $request->ip();
        Subscribe::create($request->all());
        return back()->with('success', 'Thank you for subscribe in our NewsLetter');

    }

    public function refresh()
    {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        $this->refreshSeo();
        // $this->refreshPermission();
        return redirect('/')->with('success', 'Thank you for refresh.');

    }

    public function refreshSeo() 
    {
        Seo::truncate();

        $title = [ 'Contact', 'Post', 'Portfolio'] ;
        $slug = ['contact', 'post', 'portfolio'];
        for ($i=0; $i < count($title); $i++) { 
            $data = [];
            $data['title'] = $title[$i];
            $data['slug'] = $slug[$i];
            $data['route'] = $slug[$i];
            $data['url'] = url($slug[$i]);
            Seo::create($data);
        }

        $services = Page::get();
        foreach ($services as $service) {
           SeoController::create($service->type, $service);
        }
        $categories = Category::get();
        foreach ($categories as $category) {
            SeoController::create('category', $category);
        }
        $posts = Post::get();
        foreach ($posts as $post) {
            SeoController::create('post', $post);
        }
        
    }

    /*Refresh Permission*/
    public function refreshPermission() 
    {
        Permission::truncate();
        foreach (Route::getRoutes()->getRoutes() as $route) {
            $action_object = $route->getAction();
            if (!empty($action_object['controller'])) {
                if(is_array($action_object['middleware'])){
                    if(in_array('auth',  $action_object['middleware'])) {
                        if(isset($action_object['as'])) {
                            $array  = explode('.', $action_object['as']);
                            Permission::create([
                           'object' => $array[0],
                           'action' => $array[1],
                           'name' => $action_object['as'],
                        ]);
                        }
                    }
                }
            }
        }
    }

    public function backup()
    {

        if(!auth()->user()) {
            return redirect()->route('index')->withErrors('Permission denied');
        }
        //ENTER THE RELEVANT INFO BELOW
        $db_name = env('DB_DATABASE', 'karkalo');
        $db_user = env('DB_USERNAME', 'root');
        $db_password = env('DB_PASSWORD', 'root');
        $db_host = env('DB_HOST', '127.0.0.1');
        $backup_path = public_path( date('Y-M-d').'.sql');

        //DO NOT EDIT BELOW THIS LINE
        //Export the database and output the status to the page
        $command = 'mysqldump --opt -h' .$db_host .' -u' .$db_user .' -p' .$db_password .' ' .$db_name .' > ' .$backup_path;

        //return $command;
        $output = array();
        $worked = '';

        exec($command, $output , $worked);

        //return $worked;

        switch($worked) {

            case 0:
                return response()->download($backup_path);
            break;
            case 1:
                    return redirect('/')->withErrors('Backup cannot complete... please try again later.');
            break;
            case 2:
                    return redirect('/')->withErrors('Backup cannot complete... please try again later.');
            break;
        }
    } 
}
