<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partner;
use App\Models\Page;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Setting;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Seo;
use App\Models\Post;
use App\Models\Advertisement;
use SEOMeta;
use OpenGraph;
use Auth;

class RouteController extends Controller
{

    /**
     * Show the application index.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request, $slug)
    {
        $page = Seo::whereSlug($slug)->first();
        if( $page ) {
            $redirect = $this->routePage($page); 
            $this->setSeo($page);
            $page->visit = $page->visit + 1;
            $page->save();
            return $redirect;
        }
        $url = Seo::whereUrl($request->url())->first();
        if( $url) {
            $redirect = $this->routePage($url); 
            $this->setSeo($url);
            return $redirect;
        }
        return view('welcome');
        abort(404);
    }

    public function routePage($page)
    { 
        $route = $page->route;
        $slug = $page->slug;

        switch ($route) {
            case 'about':
                return $this->about($slug);
                break;

            case 'contact':
                return $this->contact($slug);
                break;

            case 'category':
                return $this->category($slug);
                break;

            case 'post':
                return $this->post($slug);
                break;

            case 'page':
                return $this->page($slug);
                break;

            case 'service':
                return $this->service($slug);
                break;

            case 'portfolio':
                return $this->portfolio($slug);
                break;

            default:
                return redirect()->route('index');
                break;
        }
    }

    /**
     * Show the application about page.
     *
     * @return \Illuminate\Http\Response
     */

    public function about($slug) 
    {
        $pages = '';
        if($slug == 'about') {
            $pages = Page::about()->where('slug', '!=', $slug)->active()->get();
        }
        $about = Page::whereSlug('about')->first();
        if($about){
            OpenGraph::addImage($about->image);
            OpenGraph::addImage(['url' => $about->image, 'size' => 300]);
            OpenGraph::addImage($about->image, ['height' => 300, 'width' => 300]);
            return view('pages.about', compact('about', 'pages', 'teams'));
        }
        abort(404);
    }

    /**
     * Show the application contact page.
     *
     * @return \Illuminate\Http\Response
     */

    public function contact($slug) 
    {
        return view('pages.contact');
    }

     /**
     * Show the application contact page.
     *
     * @return \Illuminate\Http\Response
     */

    public function portfolio($slug) 
    {
        $categories = Category::active()->get();
        $portfolios = Page::active()->portfolio()->get();
        return view('pages.portfolio', compact('portfolios', 'categories'));
    }

    /**
     * Show the application about page.
     *
     * @return \Illuminate\Http\Response
     */

    public function post($slug) 
    {   
        $popular_posts = Post::active()->featured()->latest()->get();
        if($slug == 'post' ) {
             $posts = Post::active()->latest()->paginate(5);
            return view('pages.posts', compact('posts', 'popular_posts'));
        }
        $post = Post::whereSlug($slug)->first();
        $comments = $post->comments()->active()->get();
        $post->view = $post->view + 1;
        $post->save();
        if(Auth::check()) {
            $comments = $post->comments;
        }
        OpenGraph::addImage($post->image);
        OpenGraph::addImage(['url' => $post->image, 'size' => 300]);
        OpenGraph::addImage($post->image, ['height' => 300, 'width' => 300]);
        return view('pages.post', compact('post', 'popular_posts', 'comments'));
    }

    /**
     * Show the application about page.
     *
     * @return \Illuminate\Http\Response
     */

    public function page($slug) 
    {   
        $page = Page::whereSlug($slug)->first();
        if($page) {
            OpenGraph::addImage($page->image);
            OpenGraph::addImage(['url' => $page->image, 'size' => 300]);
            OpenGraph::addImage($page->image, ['height' => 300, 'width' => 300]);
            return view('pages.page', compact('page'));
        }
        abort(404);
    }

    public function service($slug) 
    {   
        $page = Page::whereSlug($slug)->first();
        if($page) {
            OpenGraph::addImage($page->image);
            OpenGraph::addImage(['url' => $page->image, 'size' => 300]);
            OpenGraph::addImage($page->image, ['height' => 300, 'width' => 300]);
            return view('pages.service', compact('page'));
        }
        abort(404);
    }

    /**
     * Show the application category page.
     *
     * @return \Illuminate\Http\Response
     */

    public function category($slug) 
    {
        if($slug == 'category') {
            return view('pages.categories');
        }
        $category = Category::whereSlug($slug)->first();
        if(!$category) {
            return back();
        }
        OpenGraph::addImage($category->image);
        OpenGraph::addImage(['url' => $category->image, 'size' => 300]);
        OpenGraph::addImage($category->image, ['height' => 300, 'width' => 300]);
        $pages = $category->pages()->service()->active()->get();
        $posts = $category->posts()->active()->latest()->get();
        return view('pages.category', compact('category', 'pages', 'posts'));
    }

    public function setSeo($post) 
    {
        SEOMeta::setTitle($post->title);
        SEOMeta::setDescription($post->description);
        SEOMeta::addMeta('article:published_time', $post->created_at->toW3CString(), 'property');
        SEOMeta::addMeta('article:section', $post->category, 'property');
        if($post->keyword) {
            $keywords = explode(",", $post->keyword);
            SEOMeta::addKeyword($keywords);
        }
    
        OpenGraph::setDescription($post->description);
        OpenGraph::setTitle($post->title);
        OpenGraph::setUrl(request()->fullUrl());
        OpenGraph::addProperty('type', $post->route);
        OpenGraph::addProperty('locale', 'en-us');
        OpenGraph::addProperty('locale:alternate', ['en-us']);
    }

}
