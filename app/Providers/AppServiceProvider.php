<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('permission', function ($permission) {
            return "<?php if ( Auth::check() && in_array($permission, Auth::user()->role->permissions->pluck('name')->toArray())): ?>";
        });

        Blade::directive('endpermission', function () {
            return "<?php endif; ?>";
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         
    }
}
