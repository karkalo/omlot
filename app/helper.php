<?php


	function pageType() {
		return ['page' => 'Page', 'about' => 'About', 'service' => 'Service', 'portfolio' => 'Portfolio'];
	}
	function getLayout() {
		return ['home' => 'Home', 'about' => 'About', 'service' => 'Service', 'faq' => 'FAQ', 'post' => 'Post', 'contact' => 'Contact', 'category' => 'Category', 'All' => 'all'];
	}

	function getContact() {
		return App\Models\Contact::unread()->get();
	}
	function getComment() {
		return App\Models\Comment::unread()->get();
	}

	function getPartner($limit = 5) {
		return App\Models\Partner::active()->limit($limit)->get();
	}

	function getMenu($layout) {
		$menu = App\Models\Menu::whereTitle($layout)->first();
		if($menu) {
			return $menu->children;
		}
	}

	function getSetting($key) {
		$setting = App\Models\Setting::where('key', $key)->first();
		if($setting) {
		 	return $setting->value;
		}
		return NULL;
	}
	function getLatestBlog($limit = 3) {
		return App\Models\Post::active()->latest()->limit($limit)->get();
	}
	function getCategory($limit = 5) {
		return App\Models\Category::active()->latest()->limit($limit)->get();
	}

	function getAdvertisement($limit = 2, $layout = 'home') {
		return App\Models\Advertisement::where('layout' , $layout)->active()->latest()->limit($limit)->get();
	}
