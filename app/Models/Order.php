<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    protected $fillable = [
        'worker_id', 'start_date', 'end_date', 'category_id', 'user_id'
    ];  
    public function customer()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function worker()
    {
        return $this->belongsTo('App\Models\Worker', 'worker_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
