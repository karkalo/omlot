<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'comment', 'ip_address', 'image', 'parent_id', 'is_active', 'is_featured', 'post_id', 'product_id', 'page_id'
    ];

    /**
     * The attributes that are rules for validation
     * 
     * @var array
     */
    public static $rules = ['name' => 'required', 'email' => 'required|email', 'phone' => 'required', 'comment' => 'required', 'image' => 'image|mimes:jpg,jpeg,png|max:2048'];



    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function scopeUnread($query)
    {
        return $query->where('is_active', 0);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeFeatured($query)
    {
        return $query->where('is_featured', 1);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeMain($query)
    {
        return $query->where('parent_id', NULL);
    }


    /**
     * Get the parent that owns the category.
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Comment', 'parent_id');
    }
     /**
     * Get the children category that related the category.
     */
    public function children()
    {
        return $this->hasOne('App\Models\Comment', 'parent_id');
    }

    public function type()
    {
       if($this->page_id) {
        return $this->page;
       }elseif($this->product_id) {
        return $this->product;
       }else{
        return $this->blog;
       }
        
    }

    /**
     * Get the parent that owns the category.
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

     /**
     * Get the parent that owns the category.
     */
    public function page()
    {
        return $this->belongsTo('App\Models\Page');
    }

        /**
     * Get the parent that owns the category.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function getImageAttribute($value)
    {
        if($value) {
            return asset('storage/'. $value);
        }

        return asset('no_image.jpg');
        
    }

}
