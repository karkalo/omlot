<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = ['question', 'answer','is_active'];

   

   public function getQuestionAttribue ($value)
   {
   		return $value . '?';
   }

   /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }
}
