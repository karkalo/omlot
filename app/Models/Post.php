<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'content', 'image', 'video', 'category_id', 'keyword','description', 'user_id', 'view', 'is_active', 'is_featured'
    ];


     /**
     * 
     * Foregin key assig as One to may relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function category()
    {
    	return $this->belongsTo('App\Models\Category');
    }


    /**
     * 
     * Foregin key assig as One to may relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * 
     * Foregin key assig as One to many relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }


    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }


    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeFeatured($query)
    {
        return $query->where('is_featured', 1);
    }
    
     /**
    * Query for return image attributes
    * @param query
    * @return Query
    */

    public function getImageAttribute($value)
    {
        if($value) {
            return asset('storage/'. $value);
        }

        return asset('/no_image.jpg');
        
    }

}
