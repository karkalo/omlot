<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'value', 'label', 'type'
    ];

    /**
     * The attributes that are rules for validation
     * 
     * @var array
     */
    protected $rules = ['key' => 'required|unique:settings'];
}
