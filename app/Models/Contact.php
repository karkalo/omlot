<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'subject', 'message', 'ip_address', 'parent_id', 'is_viewed', 'is_response'
    ];

    /**
     * The attributes that are rules for validation
     * 
     * @var array
     */
    public $rules = ['name' => 'required', 'email' => 'required|email', 'phone' => 'required', 'message' => 'required'];

    /**
     * Get the parent that owns the category.
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Comment', 'parent_id');
    }
     /**
     * Get the children category that related the category.
     */
    public function children()
    {
        return $this->hasOne('App\Models\Comment', 'parent_id');
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeUnread($query)
    {
        return $query->where('is_viewed', 0);
    }
    

}
