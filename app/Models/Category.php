<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug','content', 'image', 'keyword', 'description', 'is_active', 'is_menu', 'is_featured'
    ];


     /**
     * 
     * Foregin key assig as One to may relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function pages()
    {
    	return $this->hasMany('App\Models\Page');
    }

    /**
     * 
     * Foregin key assig as One to may relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }


    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeMenu($query)
    {
        return $query->where('is_menu', 1);
    }


     /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeFeatured($query)
    {
        return $query->where('is_featured', 1);
    }


    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function selectCategory()
    {
       return [NULL => 'Select Category'] + Self::active()->pluck('title', 'id')->toArray();
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */


    public function getImageAttribute($value)
    {
        if($value) {
            return asset('storage/'. $value);
        }

        return asset('no_image.jpg');
        
    }
    
}
