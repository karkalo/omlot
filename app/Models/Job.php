<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'title', 'description', 'slug', 'status', 'category_id'
    ];

    public function category(){

      return $this->belongsTo('App\Models\Category','category_id');
    }
}
