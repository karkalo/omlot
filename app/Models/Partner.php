<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [  'name','link','logo', 'is_active'];

     /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function getLogoAttribute($value)
    {
        if($value) {
            return asset('storage/'. $value);
        }

        return asset('logo.png');
        
    }
}
