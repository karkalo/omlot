<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'content', 'image', 'video', 'category_id','type', 'keyword','description', 'is_active', 'is_menu', 'is_featured'
    ];

     /**
     * 
     * Foregin key assig as One to may relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function category()
    {
    	return $this->belongsTo('App\Models\Category');
    }
     /**
     * 
     * Foregin key assig as One to many relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function metas()
    {
        return $this->hasMany('App\Models\Meta');
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeMenu($query)
    {
        return $query->where('is_menu', 1);
    }


    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeFeatured($query)
    {
        return $query->where('is_featured', 1);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeAbout($query)
    {
        return $query->whereType('About');
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeService($query)
    {
        return $query->whereType('Service');
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopePortfolio($query)
    {
        return $query->whereType('Portfolio');
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeCareer($query)
    {
        return $query->whereType('Career');
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopePage($query)
    {
        return $query->whereType('Page');
    }


    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeHome($query)
    {
        return $query->whereType('Home');
    }
    /**
     * 
     * Foregin key assig as One to many relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

     /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function getImageAttribute($value)
    {
        if($value) {
            return asset('storage/'. $value);
        }
    }

}
