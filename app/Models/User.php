<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','is_active', 'role_id', 'address', 'state', 'district', 'municipality', 'street', 'latitude', 'longitude', 'ip_address', 'type', 'status'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    

    /**
     * 
     * Foregin key assig as One to may relation
     * @return Illuminate\Database\Eloquent\Model\HasRelationship belongsTo;
     */

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
     * Relationship: role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function orders()
    { 
        return $this->hasMany('App\Models\Order');
    }
    
    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

}
