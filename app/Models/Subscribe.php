<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'ip_address',
    ];

        /**
     * The attributes that are rules for validation
     * 
     * @var array
     */
    protected $rules = ['email' => 'required|email'];

}
