<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    protected $fillable= [
      'hour_rate', 'working_hour_per_day', 'working_day_per_month', 'availability', 'user_id', 'status', 'slug', 'rate_type'
    ];

    public function user(){

        return $this->belongsTo('App\Models\User','user_id');
    }

    public function orders(){
        return $this->hasMany('App\Models\Order');
    }
}
