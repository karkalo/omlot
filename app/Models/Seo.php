<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'route', 'url', 'keywords', 'description', 'is_active', 'visit'
    ];

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    
}
