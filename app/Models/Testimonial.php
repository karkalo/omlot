<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'company', 'position', 'testimonial', 'profile', 'is_active'
    ];

     /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

     /**
    * Query for return profile attributes
    * @param query
    * @return Query
    */

    public function getProfileAttribute($value)
    {
        if($value) {
            return asset('storage/'. $value);
        }

        return asset('user.png');
        
    }
}
