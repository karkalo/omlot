<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'image', 'link', 'content', 'keyword', 'description', 'is_slider', 'is_active', 'is_featured'
    ];


    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeSlider($query)
    {
        return $query->where('is_slider', 1);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */

    public function scopeFeatured($query)
    {
        return $query->where('is_featured', 1);
    }

    /**
    * Query for return active attributes
    * @param query
    * @return Query
    */


    public function getImageAttribute($value)
    {
        if($value) {
            return asset('storage/'. $value);
        }

        return asset('no_image.jpg');
    }

    /**
     * relation to roles table.
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_gallery', 'gallery_id');
    }
}
