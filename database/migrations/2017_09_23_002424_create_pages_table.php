<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('type')->default('page');
            $table->longtext('content')->nullable();
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('keyword')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_menu')->default(0);
            $table->boolean('is_featured')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
