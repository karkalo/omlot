<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('hour_rate')->nullable();
            $table->decimal('working_hour_per_day')->nullable();
            $table->decimal('working_day_per_month')->nullable();
            $table->string('availability')->nullable();
            $table->integer('rate_type')->nullable();
            $table->integer('status')->nullable();
            $table->string('slug')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
