<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Worker::class, function (Faker $faker) {
    return [
        'user_id' => App\Models\User::first()->id,
        'hour_rate' => rand(1,100),
        'status' => rand(0,1),
        'slug' => $faker->slug(),
        'working_hour_per_day' => rand(1,12),
        'working_day_per_month' => rand(1,30),
        'availability' => $faker->word(),
        'rate_type' => rand(0,3),
    ];
});
