<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersSeeder::class,
            RolesSeeder::class,
            PermissionsSeeder::class,
            PermissionRoleTableSeeder::class,
            DemoSeeder::class,
            CategoryTableSeeder::class,
            JobTableSeeder::class,
            WorkerTableSeeder::class,
        ]);
    }
}
