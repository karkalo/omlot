<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::truncate();

        $admin = User::create([
            'name'     => 'Tim Xilwal',
            'email'    => 'dev.xilwal@gmail.com',
            'password' => bcrypt('lunatic3x'),
            'is_active' => 1,
            'role_id' => 1,
        ]);
    }
}
