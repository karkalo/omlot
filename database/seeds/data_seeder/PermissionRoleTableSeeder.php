<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $permissions = Permission::all();
      $role = Role::first();
      foreach ($permissions as $key => $permission) {
        DB::insert('insert into permission_role (role_id, permission_id) values (?, ?)', [$role->id, $permission->id]);

      }
    }
}
