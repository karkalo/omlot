<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;
use App\Models\Page;
use App\Models\Seo;
use App\Models\Menu;
use App\Http\Controllers\SeoController;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		Setting::truncate();
        Seo::truncate();
        Page::truncate();
        
    	$label = ['Company Name', 'Email Address', 'Phone Number', 'Mobile Number', 'Address', 'Facebook', 'Twitter', 'Google Plus', 'Linkedin', 'About', 'Working Hour', 'Map Longitude', 'Map Laltitude', 'Copy Right'];

    	$key = ['company_name', 'email', 'phone', 'mobile', 'address', 'facebook', 'twitter', 'google', 'linkedin', 'about', 'working_hour', 'longitude', 'latitude', 'copy_right'];

    	$value = ['Tim Xilwal', 'dev@xilwal.com', '9811960012', '9847502170', 'Kathmand, Nepal', 'https://facebook.com/bsilwal2', 'https://twitter.com/xilwal', 'https://plus.google.com/user/xilwal', 'https://linkedin.com/xilwal', 'We are a web developer.', 'Mon- Fri: 10:00 AM - 6:00 PM', '27.9901273', '87.3789269', 'copyright @2016, <a href="http://xilwal.com">Xilwal</a>. All Rights Reserved.'];

    	for ($i=0; $i < count($label); $i++) { 
    		$data = [];
    		$data['label'] = $label[$i];
    		$data['key'] = $key[$i];
    		$data['value'] = $value[$i];
    		Setting::create($data);
    	}


    	$title = ['About',  'Contact', 'Service', 'FAQ', 'Product', 'Gallery', 'Post', 'Category', 'Career', 'Portfolio'] ;
    	$slug = ['about', 'contact', 'service', 'faq', 'product', 'gallery', 'post', 'category', 'career', 'portfolio'];
        for ($i=0; $i < count($title); $i++) { 
            $data = [];
            $data['title'] = $title[$i];
            $data['slug'] = $slug[$i];
            $data['route'] = $slug[$i];
            $data['url'] = url($slug[$i]);
            Seo::create($data);
        }
        Page::create(['title' => $title[0], 'slug' => $slug[0], 'type' => $slug[0]]);
    }

}
