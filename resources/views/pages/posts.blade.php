@extends('layouts.front')
@section('content')
<!-- portfolio  open -->
<div class="about-us-top-part pt-0 mb-5">
    <div class="container">
        <h1 class="p-5">Recent posts</h1>
    </div>
</div>
<div class="portfolio">
    <div class="container">
        <!-- Section: Blog v.1 -->
        <section class="my-5">
            <!-- Grid row -->
            @foreach($posts as $key => $post)
                <div class="row">
                    <!-- Grid column -->
                    <div class="col-lg-5">
                        <!-- Featured image -->
                        <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
                            <img class="img-fluid" src="{{$post->image}}" alt="{{$post->title}}">
                            <a href="{{$post->slug}}">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                    </div>
                    <!-- Grid column -->
                    <div class="col-lg-7">
                        <!-- Category -->
                        
                        <!-- Post title -->
                        <h3 class="font-weight-bold mb-3"><strong>{{$post->title}}</strong></h3>
                        <a href="{{$post->category->slug}}" class="green-text">
                            <h6 class="font-weight-bold mb-3"><i class="fa fa-dashboard pr-2"></i>{{$post->category->title}}</h6>
                        </a>
                        <!-- Excerpt -->
                        <p>{{$post->description}}</p>
                        <!-- Post data -->
                        <p>by <a><strong>{{$post->user->name}}</strong></a>, {{date('Y/m/d', strtotime($post->updated_at))}}</p>
                        <!-- Read more button -->
                        <a class="btn btn-success btn-md" href="{{$post->slug}}">Read more</a>
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
                <hr class="my-5">
            @endforeach
        </section>
        <!-- Section: Blog v.1 -->
        <!-- portfolio close -->
        <nav aria-label="Pagination">
            {{$posts->render()}}
        </nav>
    </div>
    
</div>

@stop