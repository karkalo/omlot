@extends('layouts.front')
@section('content')

 <!-- About us and our team  open -->
<div class="about-us-top-part">
  <div class="container">
    <h1 class="p-5">{{$page->title}}</h1>
  </div>
</div>
 <div class="container">
        <p class="mt-5">{!! $page->content !!}</p>
 </div>

@stop