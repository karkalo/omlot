@extends('layouts.front')
@section('content')
<!-- portfolio  open -->
<div class="about-us-top-part">
	<div class="container">
		<h1 class="p-5">Some Of Our Work</h1>
	</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center py-3 float-right">
            <button class="btn btn-default filter-button" data-filter="all">All</button>
            @foreach($categories as $category)
                <button class="btn btn-default filter-button" data-filter="{{$category->slug}}">{{$category->title}}</button>
            @endforeach
        </div>
        @foreach($portfolios as $portfolio)
            <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-6 filter {{$portfolio->category ? $portfolio->category->slug: 'all'}}">
                <div class="ih-item square effect4">
                    <a href="{{$portfolio->slug}}">
                        <div class="img">
                            <img src="{{$portfolio->image}}" alt="{{$portfolio->title}}" title="{{$portfolio->title}}" class="img-fluid" style="height: 180px">
                        </div>
                        <div class="mask1"></div>
                        <div class="mask2"></div>
                        <div class="info float-left">
                            <h3>{{$portfolio->title}}</h3>
                            <p>{{$portfolio->description}}</p>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>

@stop