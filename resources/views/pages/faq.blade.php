@extends('layouts.front')
@section('content')
	<div class="container brdcrmb-sec">
        <div class="btn-group btn-breadcrumb">
            <a href="{{url('/')}}" class="btn btn-default btn-myway"><i class="fa fa-home"></i></a>
            <a href="{{ url('faq')}}" class="btn btn-default">FAQ</a>
        </div>
    </div><!--breadcrumb-->
   <section class="pages-sec-main">
        <div class="container pages-g">  
            <div class="col-md-12">
                <div class="blog-sec">
                    <div class="intro-header-sec">
                        <h4>Frequently Asked Question?</h4>
                        <hr class="title-hr">
                        <h5></h5>
                    </div>
	                @foreach($faqs as $faq)
	                <div class="service">
	    	        	<h3> {{$faq->question}}</h3>
	    	            <p style="text-align:justify">{!! ($faq->answer)!!}</p>
	                </div>
	                @endforeach
	            </div><!--service-detail-pg-->
	    </div>  
</section>
  @include('layouts.question')
@stop