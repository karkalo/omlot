@extends('layouts.front')
@section('content')
    <!-- About us and our team  open -->
    <div class="about-us-top-part">
      <div class="container">
        <h1 class="p-5">About Us</h1>
      </div>
    </div>

    <div class="container">
        <p class="mt-5">{!! $about->content !!}</p>
        <div class="about-mid">
            @foreach($pages as $key => $page)
                @if($key/2 == 0)
                    <div class="row my-5">
                        <div class="col-md-5"> 
                            <img src="{{$page->image}}" alt="{{$page->title}}" title="{{$page->title}}" class="img-fluid  waves-effect waves-light"> 
                        </div>
                        <div class="col-md-7">
                            <h2>{{$page->title}}</h2>
                            <p>{!! $page->content !!}</p>
                        </div>
                    </div>
                @else
                    <div class="row my-5">
                        <div class="col-md-7">
                            <h2>{{$page->title}}</h2>
                            <p>{!! $page->content !!}</p>
                        </div>
                    <div class="col-md-5"> 
                        <img src="{{$page->image}}" alt="{{$page->title}}" title="{{$page->title}}" class="img-fluid  waves-effect waves-light"> 
                    </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@stop