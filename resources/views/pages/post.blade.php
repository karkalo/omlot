@extends('layouts.front')
@section('content')
<!-- portfolio  Close  -->
<div class="about-us-top-part pt-0 mb-5">
    <div class="container">
        <h1 class="p-5">{{$post->title}}</h1>
    </div>
</div>
<div class="portfolio">
    <div class="container">
    <!-- Section: Blog v.4 -->
    <section class="my-5">
        <!-- Grid row -->
        <div class="row">
        <!-- Grid column -->
            <div class="col-md-12">
                <!-- Card -->
                <div class="card card-cascade wider reverse">
                    <!-- Card image -->
                    <div class="view view-cascade overlay">
                        <img class="card-img-top img-fluid" src="{{$post->image}}" alt="Sample image">
                        <a href="{{$post->slug}}">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                   </div>
                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center">
                        <!-- Title -->
                        <h2 class="font-weight-bold"><a>{{$post->title}}</a></h2>
                        <!-- Data -->
                        <p>Written by <a><strong>{{$post->user->name}}</strong></a>, {{date('Y/m/d', strtotime($post->updated_at))}}</p>
                        <!-- Social shares -->
                        <div class="social-counters">
                            <div class="sharethis-inline-share-buttons"></div>
                      </div>
                      <!-- Social shares -->
                    </div>
                    <!-- Card content -->
                  </div>
                <!-- Card -->
                <!-- Excerpt -->
                <div class="mt-5">
                    {!! $post->content !!}
                </div>
                <div class="sharethis-inline-reaction-buttons"></div>
            </div>
            <!-- Grid column -->
            <div class="fb-comments" data-href="{{url($post->slug)}}" data-numposts="5" width="500"></div>
        </div>
    </section>
    <!-- Section: Blog v.4 -->
    </div>
</div>
<!-- portfolio close -->
@stop