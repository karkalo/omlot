@extends('layouts.front')
@section('content')
<!-- portfolio  open -->
<div class="banner mb-3">
     <img src="{{$page->image}}" class="img img-fluid w-100">
</div>

<div class="services-singal-page">
    <div class="container">
        <div class="row services-bg">
            <div class="col-md-12 p-3">
              <h2 class="text-center pb-3">{{$page->title}} Service</h2>
              <div class="p-3">{!! $page->content !!}</div>
              <a href="{{$page->slug}}" class="my-5 btn btn-outline-danger float-right" data-toggle="modal" data-target="#getquotes">Request for Quote</a>
              <div class="modal fade" id="getquotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		          <div class="modal-dialog" role="document" style="width: 75%">
		            <div class="modal-content">
		              <div class="modal-header">
		                <h5 class="modal-title" id="exampleModalLabel">Request for Quotation</h5>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
		              </div>
		               <form method="POST" action="{{route('comment')}}" accept-charset="UTF-8" id="quote-form" enctype="multipart/form-data">
		                  <div class="modal-body">
		                        {{csrf_field()}}
		                        <h2 class="text-center">{{$page->title}}</h2>
		                        <div class="md-form mb-3">
		                           <input type="hidden" name="page_id" value="{{$page->id}}">
		                        </div>
		                        <div class="md-form mb-3">
		                            <input type="text" id="name" name="name" class="form-control" required=""> 
		                            <label for="name" class="">Name/Company</label>
		                        </div>
		                        <div class="md-form mb-3">
		                            <input type="text" id="email" name="email" class="form-control" required="">
		                            <label for="email" class="">E-mail Address</label>
		                        </div>
		                        <div class="md-form mb-3">
		                            <input type="text" id="phone" name="phone" class="form-control" required="">
		                            <label for="phone" class="">Contact Number </label>
		                        </div>
		                       
		                        <div class="md-form mb-3">
		                            <textarea type="text" id="message" name="comment" rows="2" class="form-control md-textarea"></textarea>
		                            <label for="message">Describe Your Requirements</label>
		                        </div>

		                        <div class=" md-form input-group mb-3">
		                          <div class="custom-file">
		                            <input type="file" class="custom-file-input" id="inputGroupFile02" name="file">
		                            <label class="custom-file-label" for="inputGroupFile02">Upload Attachment</label>
		                          </div>
		                        </div>
		                   
		                  </div>
		                  <div class="modal-footer center-on-small-only">
		                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		                    <button type="submit" class="btn btn-primary">Send Enquiry</button>
		                  </div>
		               </form>
		            </div>
		          </div>
		    </div>
            </div>
            <div class="col-md-12 mb-4">
            	<h2 class="text-center">You may also like</h2>
            </div>
            @foreach($page->category->pages()->service()->active()->get() as $pages)
	            <div class="col-md-4 mb-4">
	                <!-- Card -->
	                <div class="card hoverable h-100">
	                    <!-- Card image -->
	                    <div class="view overlay" >
	                        <img class="card-img-top" src="{{$pages->image}}" alt="Card image cap" style="height: 150px">
	                        <a href="{{$pages->slug}}">
	                          <div class="mask rgba-white-slight"></div>
	                        </a>
	                    </div>
	                    <!-- Card content -->
	                    <div class="card-body text-center">
	                        <!-- Title -->
	                        <h4 class="card-title">{{$pages->title}}</h4>
	                        <!-- Text -->
	                        <p class="card-text">{{$pages->description}}</p>
	                        <!-- Button -->
	                        <a href="{{$pages->slug}}" class="btn btn-sm  btn-success">Read More</a>
	                        <a href="{{$pages->slug}}" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#getquote-{{$pages->id}}">Get Quote</a>
	                    </div>
	                </div>
	                <!-- Card -->
	            </div>
	            <div class="modal fade" id="getquote-{{$pages->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	              <div class="modal-dialog" role="document" style="width: 75%">
	                <div class="modal-content">
	                  <div class="modal-header">
	                    <h5 class="modal-title" id="exampleModalLabel">Request for Quotation</h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                      <span aria-hidden="true">&times;</span>
	                    </button>
	                  </div>
	                    <form method="POST" action="{{route('comment')}}" accept-charset="UTF-8" id="quote-form" enctype="multipart/form-data">
	                      <div class="modal-body">
	                      	 <h2 class="text-center">{{$pages->title}}</h2>
	                            {{csrf_field()}}
	                            <div class="md-form mb-3">
	                               <input type="hidden" name="page_id" value="{{$pages->id}}">
	                            </div>
	                            <div class="md-form mb-3">
	                                <input type="text" id="name" name="name" class="form-control" required=""> 
	                                <label for="name" class="">Name/Company</label>
	                            </div>
	                            <div class="md-form mb-3">
	                                <input type="text" id="email" name="email" class="form-control" required="">
	                                <label for="email" class="">E-mail Address</label>
	                            </div>
	                            <div class="md-form mb-3">
	                                <input type="text" id="phone" name="phone" class="form-control" required="">
	                                <label for="phone" class="">Contact Number </label>
	                            </div>
	                           
	                            <div class="md-form mb-3">
	                                <textarea type="text" id="message" name="comment" rows="2" class="form-control md-textarea"></textarea>
	                                <label for="message">Describe Your Requirements</label>
	                            </div>

	                            <div class=" md-form input-group mb-3">
	                              <div class="custom-file">
	                                <input type="file" class="custom-file-input" id="inputGroupFile02" name="file">
	                                <label class="custom-file-label" for="inputGroupFile02">Upload Attachment</label>
	                              </div>
	                            </div>
	                       
	                      </div>
	                      <div class="modal-footer center-on-small-only">
	                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	                        <button type="submit" class="btn btn-primary">Send Enquiry</button>
	                      </div>
	                   </form>
	                </div>
	              </div>
	            </div>
	        @endforeach
	    </div>
     
    </div>
</div>
<!-- portfolio close -->
@stop