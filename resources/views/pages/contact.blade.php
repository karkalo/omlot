
@extends('layouts.front')
@section('content')
<!-- Conatct Form And info close -->
<div class="map mb-5 mt-3">
    <div class="map-sec">
        <div id="googleMap" style="width:100%; height:300px;"></div>
    </div><!--map-sec-->
    <br>    
</div>
<div class="contact-info">
    <div class="container">
        <section class="section">
            <div class="row mt-4">
                <!--Grid column-->
                <div class="col-md-7 col-lg-7 ">
                  	<form method="POST" action="{{route('contact')}}" accept-charset="UTF-8" id="contact-form">
                        {{csrf_field()}}
                        <!--Grid row-->
                        <div class="row">
                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    <input type="text" id="name" name="name" class="form-control">
                                    <label for="name" class="">Your name</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form">
                                    <input type="text" id="email" name="email" class="form-control">
                                    <label for="email" class="">Your email</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form">
                                    <input type="text" id="phone" name="phone" class="form-control">
                                    <label for="phone" class="">Phone</label>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="md-form">
                                    <input type="text" id="subject" name="subject" class="form-control">
                                    <label for="subject" class="">Subject</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form">
                                    <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                                    <label for="message">Your message</label>
                                </div>
                            </div>
                        </div>
                        <div class="center-on-small-only">
                            <a class="btn btn-primary" onclick="document.getElementById('contact-form').submit();">Send</a>
                        </div>
                        <div class="status"></div>
                  	</form>
                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-md-5  col-lg-5">
                    <div class="address-part">
                        <ul class="contact-icons">
                            <li><i class="fa fa-map-marker fa-2x"> OUR LOCATION</i>
                                <p>{{getSetting('address')}}<br>
                            </li>
                            <li><i class="fa fa-phone fa-2x"> CALL US ON</i>
                                <p>{{getSetting('phone')}}</p>
                                <p>{{getSetting('mobile')}}</p>
                            </li>

                            <li><i class="fa fa-envelope fa-2x"> SEND YOUR MESSAGE</i>
                                <p>{{getSetting('email')}}</p>
                            </li>
                        </ul>
                    </div>   
                </div>
                <!--Grid column-->
            </div>
        </section>
    </div>
</div>

@stop
@section('scripts')
    <script type="text/javascript">
        var myCenter=new google.maps.LatLng({{getSetting('latitude')}},{{getSetting('longitude')}});
             google.maps.event.addDomListener(window, 'load', initialize);

            function initialize()
            {
            var mapProp = {
              center:myCenter,
              zoom:20,
              mapTypeId:google.maps.MapTypeId.ROADMAP
              };
            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            var marker=new google.maps.Marker({
              position:myCenter,
              });
            marker.setMap(map);
            var infowindow = new google.maps.InfoWindow({
              content:"{{getSetting('company_name')}}"
              });
            infowindow.open(map,marker);
            }
    </script>
@stop
    