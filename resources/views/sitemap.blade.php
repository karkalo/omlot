<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($seos as $seo)
    <sitemap>
        <loc>{{$seo->url}}</loc>
        <lastmod>{{ $seo->updated_at->tz('UTC')->toAtomString() }}</lastmod>
    </sitemap>
    @endforeach
</sitemapindex>