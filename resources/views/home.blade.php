@extends('layouts.front')
@section('content')
    <div class="slider-section">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @for($i = 0; $i < count($sliders); $i ++ )
                <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{$i == 0 ? 'active' : ''}}"></li>
                @endfor
            </ol>
            <div class="carousel-inner">
                @foreach($sliders as $key => $slider)
                    <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                       {{--  @if($slider->description)
                            <div class=" intro-header-sec">
                                <h1>{{$slider->title}}</h1>
                                <hr class="title-hr">
                                <h5> {{$slider->description}}</h5>
                                <div class="pt-3">
                                    <a class="btn btn-info" href="{{($slider->link)}}">View More</a>
                                </div><!--btn-sec-1-->
                            </div>
                        @endif --}}
                        <a href="{{$slider->link}}">
                            <img src="{{$slider->image}}" class="d-block w-100 img-responsive" alt="{{$slider->title}}" title="{{$slider->title}}">
                        </a>
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- services section start -->
    <div class="services">
        <div class="container">
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-lg-4 col-md-12 col-sm-12 text-center">
                        <div class="card hoverable z-depth-3">
                            <div class="web-icon  z-depth-1">
                                <i class="fa fa-bar-chart-o" aria-hidden="true"></i>
                            </div>
                            <h4 class="text-center">{{$category->title}} </h4>
                            <div class="services-list">
                           <div class="list-group">
                            @foreach($category->pages()->service()->active()->limit(6)->get() as $page)
                              <a href="{{$page->slug}}" class="list-group-item list-group-item-action  waves-effect">
                                 {{$page->title}}
                              </a>
                            @endforeach
                                <a href="{{$category->slug}}" class="btn btn-outline-black btn-rounded waves-effect text-center">Read More</a>
                           </div>
                            </div>  
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Our Top Client open-->
    <div class="top-services">
        <div class="container">
            <div class="heading-section">
                <h3>OUR TOP SERVICES</h3>
                <img src="/img/title-line.png">
            </div>
            <div class="row services-top-space">
                @foreach($services as $service)
                <div class="col-lg-3 col-md-6 col-sm-12 pb-2">
                    <div class="card" style="min-height: 100%">
                        <div class="services-top-singal">
                            <!-- normal -->
                            <div class="ih-item square effect4">
                                <a href="{{$service->slug}}">
                                    <div class="img">
                                        <img src="{{$service->image}}" alt="{{$service->title}}" title="{{$service->title}}" class="img-fluid" style="height: 180px">
                                    </div>
                                    <div class="mask1"></div>
                                    <div class="mask2"></div>
                                    <div class="info float-left">
                                        <h3>{{$service->title}}</h3>
                                        <p>{{$service->description}}</p>
                                    </div>
                                </a>
                            </div>
                            <!-- end normal -->
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
       </div>
    </div>
    <!-- why choose us  open --> 
    <div class="why-choose-us">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="why-choose-us-left">
                        <div class="choose-caption text-center">
                            <h2 class="">Who we Are</h2>
                            <img src="/img/choose-line.png" alt="karkalo.com" title="karkalo.com">
                        </div>
                        <div class="choose-left">
                            <p style="color: #fff">{!!$about->content!!}</p>
                            <a href="{{$about->slug}}" class="btn waves-effect choose-btn">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="why-choose-us-right">
                        <div class="choose-caption text-center">
                            <h2 class="">Why Choose Us</h2>
                            <img src="/img/choose-line.png" alt="karkalo.com" title="karkalo.com">
                        </div>
                        <div class="choose-right">
                            <div class="media">
                                <i class="fa fa-asl-interpreting d-flex mr-3" aria-hidden="true"></i>
                                <div class="media-body">
                                    <h5 class="mt-0 font-weight-bold">Your business depends on your IT systems.</h5>
                                   Every day our team of experts provides an entirely positive, above and beyond experience to every client in the Karkalo Tech. We handle all aspects of your IT infrastructure including hardware and software management, vendor relationships for your digital advertisement, website management, and maintenance renewals, and any other related technology needs.
                                </div>
                            </div>
                        </div>
                        <div class="choose-right">
                            <div class="media">
                                <i class="fa fa-asl-interpreting d-flex mr-3" aria-hidden="true"></i>
                                <div class="media-body">
                                    <h5 class="mt-0 font-weight-bold">A Wide Spectrum Of Skills And Experience.</h5>
                                    From quick PC fixes to total server and software engineering – we’ve got it. And if there’s ever a problem we can’t solve, we know who to contact to get your business depends on your IT systems.
                                </div>
                            </div>
                        </div>
                        <div class="choose-right">
                            <div class="media">
                                <i class="fa fa-asl-interpreting d-flex mr-3" aria-hidden="true"></i>
                                <div class="media-body">
                                    <h5 class="mt-0 font-weight-bold">Committed to Quality.</h5>
                                    It takes teamwork and a solid commitment to good communication, excellence, and industry best practices to serve a company in an excellent manner. If we cannot succeed in an excellent manner because of value differences – we simply don’t pursue the opportunity.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- why choose us end --> 
    <!-- What Our Client Say open -->
    {{-- <div class="client-say">
        <div class="container">
            <div class="heading-section">
                <h2>TESTIMONIALS</h2>
                <img src="/img/title-line.png">
            </div> 
            <div class="row">
                @foreach($testimonials as $testimonial)
                <div class="col-lg-4 col-md-12 mb-4">
                    <!--Card-->
                    <div class="card testimonial-card hoverable">
                        <!--Avatar-->
                        <div class="card-up avatar  white"><img src="{{$testimonial->profile}}" 
                        alt="avatar white" class="rounded-circle testo-img ">
                        </div>
                        <div class="card-body">
                            <!--Name-->
                            <h4 class="card-title mt-1">{{$testimonial->name}}</h4>
                            <small>{{$testimonial->position}}, {{$testimonial->company}}</small>
                            <hr>
                            <!--Quotation-->
                            <p><i class="fa fa-quote-left"></i> {!! $testimonial->testimonial !!} </p>
                        </div>
                    </div>
                    <!--Card-->
                </div>
                @endforeach
            </div>
        </div>
    </div> --}}
    <!-- What Our Client Say close -->
    <!--  Latest News  OPEN -->
    <div class="latest-news">
        <div class="container">
            <div class="heading-section">
                <h2>LATEST NEWS</h2>
                <img src="/img/title-line.png">
            </div>
            <div class="row news-coll">
                <!-- Grid column -->
                @foreach($blogs as $blog)
                <div class="col-lg-4 col-md-12 mb-4">
                    <!--Card Narrower-->
                    <div class="card card-cascade narrower h-100">
                        <!--Card image-->
                        <div class="view view-cascade overlay">
                            <img src="{{$blog->image}}" class="card-img-top" alt="narrower" style="height: 265px;
    object-fit: cover;">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <!--/.Card image-->
                        <!--Card content-->
                        <div class="card-body card-body-cascade">
                            <h5 class="pink-text"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> <small>{{date('M, d, Y', strtotime($blog->updated_at))}}</small></h5>
                            <!--Title-->
                            <h4 class="card-title">{{$blog->title}}</h4>
                            <!--Text-->
                            <p class="card-text">{{$blog->description}}</p>
                            <a class="btn btn-unique" href="{{$blog->slug}}">Read More</a>
                        </div>
                        <!--/.Card content-->
                    </div>
                    <!--/.Card Narrower-->
                </div>
                @endforeach
               
            </div> 
            <!-- Grid column -->
        </div>
    </div>

    <!--  Latest News  CLOSE -->
    <!-- Our Top Clients open -->
 {{--    <div class="top-clients">
        <div class="container">
            <div class="heading-section">
                <h2>OUR TOP CLIENTES</h2>
                <img src="/img/title-line.png">
            </div>
            
            <div class="row">
                @foreach($partners as $partner)
                    <div class="col-md-2 col-sm-2 mt-2">
                        <a href="{{$partner->link}}" target="_blank">
                            <img src="{{$partner->logo}}" alt="{{$partner->link}}"  title="{{$partner->name}}" class="  hoverable z-depth-1 img w-100 h-75">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div> --}}
    <!-- Our Top Clients close  -->
@endsection
