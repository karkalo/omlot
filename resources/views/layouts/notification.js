document.addEventListener('DOMContentLoaded', function () {
      if (Notification.permission !== "granted")
        Notification.requestPermission();
    });
    notifyMe();

    function notifyMe() {
      if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.'); 
        return;
      }

      if (Notification.permission !== "granted")
        Notification.requestPermission();
      else {
        var notification = new Notification('Welcome to Acs Group', {
          icon: "{{asset('logo.jpg')}}",
          body: "We Launched soon! Contact Us for more Details",
        });

        notification.onclick = function () {
          window.open("{{url('contact')}}");      
        };
      }
    }