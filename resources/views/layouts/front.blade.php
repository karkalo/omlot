<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Seo section -->
    {!! SEO::generate()!!}
    <!-- Seo Section Ends -->
    <meta name="google-site-verification" content="kCd-XOVubcFovMjRwfDJzUzWYveKv7lErUYx5UeshDU" />
    <link rel="icon" href="{{asset('favicon.ico')}}" sizes="32x32" />
    <link rel="icon" href="{{asset('favicon.ico')}}" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="{{asset('favicon.ico')}}" />
    <meta name="msapplication-TileImage" content="{{asset('favicon.ico')}}" />
    <link rel="apple-touch-icon" href="{{asset('logo.jpg')}}" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Css Link Section -->
    <link href="{{asset('favicon.ico')}}" rel="icon" type="image/png" >
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/ihover.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120908040-1"></script>
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b239e0e2337c30011a5b76f&product=inline-share-buttons' async='async'></script>
    <script>
        window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-120908040-1');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WDK9J8P');
    </script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <div id="app">
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=UA-120908040-1" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
            FB.init({
              appId      : '204116023544434',
              xfbml      : true,
              version    : 'v2.11'
            });
            FB.AppEvents.logPageView();
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=204116023544434';";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>


        <!-- Your customer chat code -->
        <div class="fb-customerchat"
          attribution=setup_tool
          page_id="872495786271312"
          theme_color="#67b868"
          logged_in_greeting="Hi! How can we help you?"
          logged_out_greeting="Hi! How can we help you?">
        </div>
        <!-- Start Logo Nav Tag -->
        <div class="logo-nav-section fixed-top">
            <div class="container">                                   
                <nav class="navbar navbar-expand-lg ">
                    <a class="navbar-brand" href="/">
                        <img src="/img/logo.png" alt="karkalo.com" title="karkalo.com"/>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav flash ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/about">About us</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle"
                                   href="#" id="navbarDropdownMenuLink" 
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Services
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    @foreach(App\Models\Category::active()->menu()->latest()->get() as $cat)
                                        <a class="dropdown-item" href="{{$cat->slug}}">{{$cat->title }}</a>
                                        {{-- <div class="dropdown-divider"></div> --}}
                                   @endforeach
                                </div>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="/portfolio">Our work</a>
                            </li>
                            
                            <li class="nav-item">
                              <a class="nav-link" href="/post">Blogs</a>
                            </li>
                            
                            <li class="nav-item">
                              <a class="nav-link" href="/contact">Contact us</a>
                            </li>
                            <button type="button" class="btn btn-default btn-rounded"  data-toggle="modal" data-target="#quote">Request For Quote</button>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <main class="pb-3">
            @yield('content')
        </main>
         <!-- Footer -->
        <footer class="page-footer font-small mdb-color pt-5 ">
                <!-- Footer Links -->
                <div class="container text-center text-md-left">
                    <!-- Footer links -->
                    <div class="row text-center text-md-left  pb-3">
                        <!-- Grid column -->
                        <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                            <h6 class="text-uppercase mb-4 font-weight-bold"><img src="/img/karkalo-footer-logo.png" alt="karkalo.com" title="karkalo.com"></h6>
                            <p>{{getSetting('about')}}</p>
                            <h1 style="font-size: 6px; color: #45526e">{{SEO::getTitle()}}</h1>
                        </div>
                        <!-- Grid column -->
                        <hr class="w-100 clearfix d-md-none">
                        <!-- Grid column -->
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                            <h6 class="text-uppercase mb-4 font-weight-bold">Quick Link</h6>
                            <p><a href="/">Home</a></p>
                            @foreach(App\Models\Page::page()->active()->menu()->get() as $page)
                            <p><a href="{{$page->slug}}">{{$page->title}}</a></p>
                            @endforeach
                        </div>
                        <!-- Grid column -->
                        <hr class="w-100 clearfix d-md-none">
                        <!-- Grid column -->
                        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                            <h6 class="text-uppercase mb-4 font-weight-bold">Our Services</h6>
                           @foreach(App\Models\Category::active()->menu()->get() as $cat)
                               <p><a href="{{$cat->slug}}">{{$cat->title}}</a></p>
                           @endforeach
                        </div>
                        <!-- Grid column -->
                        <hr class="w-100 clearfix d-md-none">
                        <!-- Grid column -->
                        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                            <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
                            <p><i class="fa fa-home mr-3"></i>{{getSetting('address')}}</p>
                            <p><i class="fa fa-envelope mr-3"></i>{{getSetting('email')}}</p>
                            <p><i class="fa fa-phone mr-3"></i>{{getSetting('phone')}}</p>
                            <p><i class="fa fa-mobile mr-3"></i>{{getSetting('mobile')}}</p>
                        </div>
                          <!-- Grid column -->
                    </div>
                    <!-- Footer links -->
                    <hr>
                    <!-- Grid row -->
                    <div class="row d-flex align-items-center ">
                    <!-- Grid column -->
                        <div class="col-md-8 col-lg-8">
                            <!--Copyright-->
                            <p class="text-center text-md-left">© 2018 Copyright: <a href="https://karkalo.com/"><strong> www.karkalo.com</strong></a></p>

                        </div>
                        <!-- Grid column -->
                        <!-- Grid column -->
                        <div class="col-md-4 col-lg-4 ml-lg-0">
                            <!-- Social buttons -->
                            <div class="text-center text-md-right">
                                <ul class="list-unstyled list-inline">
                                  <div class="sharethis-inline-follow-buttons"></div>
                                </ul>
                            </div>
                        </div>
                        <!-- Grid column -->
                    </div>
                    <!-- Grid row -->
                </div>
                <!-- Footer Links -->
        </footer>
            <!-- Footer -->
            <!-- Modal -->
            <div class="modal fade" id="quote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document" style="width: 75%">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Request for Quotation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    <form method="POST" action="{{route('comment')}}" accept-charset="UTF-8" id="quote-form" enctype="multipart/form-data">
                      <div class="modal-body">
                            {{csrf_field()}}
                            <div class="md-form mb-3">
                               
                                <select class="form-control" name="page_id" id="service">
                                    <option disabled="" value="" selected="">Select Our Service</option>
                                    @foreach(App\Models\Category::active()->menu()->limit(3)->get() as $category)
                                        <optgroup label="{{$category->title}}">
                                            @foreach( $category->pages()->service()->get() as $service)
                                                <option value="{{($service->id)}}">{{$service->title}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                                {{-- <label for="service" class="">Select Service.</label> --}}
                            </div>
                            <div class="md-form mb-3">
                                <input type="text" id="name" name="name" class="form-control" required=""> 
                                <label for="name" class="">Name/Company</label>
                            </div>
                            <div class="md-form mb-3">
                                <input type="text" id="email" name="email" class="form-control" required="">
                                <label for="email" class="">E-mail Address</label>
                            </div>
                            <div class="md-form mb-3">
                                <input type="text" id="phone" name="phone" class="form-control" required="">
                                <label for="phone" class="">Contact Number </label>
                            </div>
                           
                            <div class="md-form mb-3">
                                <textarea type="text" id="message" name="comment" rows="2" class="form-control md-textarea"></textarea>
                                <label for="message">Describe Your Requirements</label>
                            </div>

                            <div class=" md-form input-group mb-3">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile02" name="file">
                                <label class="custom-file-label" for="inputGroupFile02">Upload Attachment</label>
                              </div>
                            </div>

                       
                       
                      </div>
                      <div class="modal-footer center-on-small-only">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send Enquiry</button>
                      </div>
                   </form>
                </div>
              </div>
            </div>
    </div>
    <!-- SCRIPTS -->
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <!--Required libraries-->
    <script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
    <!--Execute flickerplate--> 
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{asset('admins/js/bootstrap-notify.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2DFy7X7FkiS51S97PT8gbqbpqb3VB2c0"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            @if(isset($errors) && count($errors->all()))
                $.notify({
                    icon: 'pe-7s-close-circle',
                    message: "{{$errors->first()}}"
                },{
                    type: 'danger',
                    timer: 4000
                });
            @endif
            @if ($message = Session::get('success'))

                $.notify({
                    icon: 'pe-7s-like2',
                    message: "{{$message}}"

                },{
                    type: 'success',
                    timer: 4000
                });

            @endif

        });   
        
    </script> 
    @yield('scripts')

</body>
</html>
