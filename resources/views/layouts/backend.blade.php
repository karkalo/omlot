<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	   <meta charset="utf-8" />
	   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	  <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">
	   <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
     <title>{{ config('app.name', 'Omlots Pvt. Ltd.') }}</title>

     <link rel="icon" type="image/png" href="{{asset('favicon.ico')}}">
     <!-- Bootstrap core CSS     -->
     <link href="{{asset('admins/css/jquery.dataTables.min.css')}}" rel="stylesheet" />
     <link href="{{asset('admins/css/bootstrap.min.css')}}" rel="stylesheet" />
     <!-- Animation library for notifications   -->
     <link href="{{asset('admins/css/animate.min.css')}}" rel="stylesheet"/>
     <!--  Light Bootstrap Table core CSS    -->
     <link href="{{asset('admins/css/light-bootstrap-dashboard.css?v=1.4.0')}}" rel="stylesheet"/>
     <!--  CSS for Demo Purpose, don't include it in your project     -->
      <link href="{{asset('admins/css/demo.css')}}" rel="stylesheet" />
     <!--     Fonts and icons     -->
     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
     <link href="{{asset('admins/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
     <!-- Global site tag (gtag.js) - Google Analytics -->
     <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120908040-1"></script>
     <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-120908040-1');
     </script>
     <!-- Google Tag Manager -->
     <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WDK9J8P');
     </script>
     <!-- End Google Tag Manager -->
     <script>
        (function(w,d,s,g,js,fs){
          g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
          js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
          js.src='https://apis.google.com/js/platform.js';
          fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
        }(window,document,'script'));
     </script>
     <script>
        gapi.analytics.ready(function() {
          /**
           * Authorize the user immediately if the user has already granted access.
           * If no access has been created, render an authorize button inside the
           * element with the ID "embed-api-auth-container".
           */
          gapi.analytics.auth.authorize({
            container: 'embed-api-auth-container',
            clientid: '383113958286-jf21fo3d4ba4kbkut10ri5uqrvv91afp.apps.googleusercontent.com'
          });
          /**
           * Create a new ViewSelector instance to be rendered inside of an
           * element with the id "view-selector-container".
           */
          var viewSelector = new gapi.analytics.ViewSelector({
            container: 'view-selector-container'
          });

          // Render the view selector to the page.
          viewSelector.execute();
          /**
           * Create a new DataChart instance with the given query parameters
           * and Google chart options. It will be rendered inside an element
           * with the id "chart-container".
           */
          var dataChart = new gapi.analytics.googleCharts.DataChart({
            query: {
              metrics: 'ga:sessions',
              dimensions: 'ga:date',
              'start-date': '30daysAgo',
              'end-date': 'yesterday'
            },
            chart: {
              container: 'chart-container',
              type: 'LINE',
              options: {
                width: '100%'
              }
            }
          });
          /**
           * Render the dataChart on the page whenever a new view is selected.
           */
          viewSelector.on('change', function(ids) {
            dataChart.set({query: {ids: ids}}).execute();
          });

        });
     </script>
</head>
<body>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WDK9J8P" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    {{-- Facebook SDK --}}
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '204116023544434',
          xfbml      : true,
          version    : 'v2.11'
        });

        FB.AppEvents.logPageView();

      };
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=204116023544434';";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    {{-- Header Starts --}}

    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="{{asset('admins/img/sidebar-4.jpg')}}">

            <!--

                Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
                Tip 2: you can also add an image using data-image tag
            -->

        	<div class="sidebar-wrapper">
                <div class="logo">
                    <a href="{{route('backend.dashboard')}}" class="simple-text"> <img src="{{getSetting('admin-logo') ? getSetting('admin-logo') : asset('/logo.jpg')}}" title="{{ config('app.name', 'Karkalo Pvt. Ltd.') }}" alt="Karkalo Pvt. Ltd." height="50" width="200"></a>
                </div>

                <ul class="nav">
                    <li class="{{Route::currentRouteName() == 'backend.dashboard' ? 'active': ''}}">
                        <a href="{{route('backend.dashboard')}}">
                            <i class="pe-7s-graph"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                   
                    @permission('category.index')
                    <li class="{{Request::is('*/category*') ? 'active': ''}}">
                        <a href="{{route('category.index')}}">
                            <i class="pe-7s-folder"></i>
                            <p>Category</p>
                        </a>
                    </li>
                    @endpermission

                    @permission('job.index')
                    <li class="{{Request::is('*/job*') ? 'active': ''}}">
                        <a href="{{route('job.index')}}">
                            <i class="pe-7s-folder"></i>
                            <p>Job</p>
                        </a>
                    </li>
                    @endpermission

                    @permission('worker.index')
                    <li class="{{Request::is('*/worker*') ? 'active': ''}}">
                        <a href="{{route('worker.index')}}">
                            <i class="pe-7s-folder"></i>
                            <p>Worker</p>
                        </a>
                    </li>
                    @endpermission

                    @permission('order.index')
                    <li class="{{Request::is('*/order*') ? 'active': ''}}">
                        <a href="{{route('order.index')}}">
                            <i class="pe-7s-folder"></i>
                            <p>Orders</p>
                        </a>
                    </li>
                    @endpermission

                    @permission('page.index')
                    <li class="{{Request::is('*/page*') ? 'active': ''}}">
                        <a href="{{route('page.index')}}">
                            <i class="pe-7s-global"></i>
                            <p>Page</p>
                        </a>
                    </li>
                    @endpermission
                   
                    @permission('post.index')
                    <li class="{{Request::is('*/post*') ? 'active': ''}}">
                        <a href="{{route('post.index')}}">
                            <i class="pe-7s-note"></i>
                            <p>Post</p>
                        </a>
                    </li>
                    @endpermission
                   

                    @permission('faq.index')
                    <li class="{{Request::is('*/faq*') ? 'active': ''}}">
                        <a href="{{route('faq.index')}}">
                            <i class="pe-7s-chat"></i>
                            <p>FAQ</p>
                        </a>
                    </li>
                    @endpermission
                    
                    @permission('testimonial.index')
                    <li class="{{Request::is('*/testimonial*') ? 'active': ''}}">
                        <a href="{{route('testimonial.index')}}">
                            <i class="pe-7s-id"></i>
                            <p>Testimonial</p>
                        </a>
                    </li>
                    @endpermission

                    @permission('partner.index')
                    <li class="{{Request::is('*/partner*') ? 'active': ''}}">
                        <a href="{{route('partner.index')}}">
                            <i class="pe-7s-users"></i>
                            <p>Partner</p>
                        </a>
                    </li>
                    @endpermission

                    @permission('gallery.index')
                    <li class="{{Request::is('*/gallery*') ? 'active': ''}}">
                        <a href="{{route('gallery.index')}}">
                            <i class="pe-7s-photo-gallery"></i>
                            <p>Gallery</p>
                        </a>
                    </li>
                    @endpermission

                    @permission('user.index')
                    <li class="{{Request::is('*/user*') ? 'active': ''}}">
                        <a href="{{route('user.index')}}">
                            <i class="pe-7s-user"></i>
                            <p>User</p>
                        </a>
                    </li>
                    @endpermission
                    @permission('role.index')
                     <li class="{{Request::is('*/role*') ? 'active': ''}}">
                        <a href="{{route('role.index')}}">
                            <i class="pe-7s-note2"></i>
                            <p>Role</p>
                        </a>
                    </li>
                    @endpermission
                    @permission('setting.index')
                    <li class="{{Request::is('*/setting*') ? 'active': ''}}">
                        <a href="{{route('setting.index')}}">
                            <i class="pe-7s-tools"></i>
                            <p>Setting</p>
                        </a>
                    </li>
                    <li class="{{Request::is('*/backup*') ? 'active': ''}}">
                        <a href="{{url('backup')}}">
                            <i class="pe-7s-tools"></i>
                            <p>Backup</p>
                        </a>
                    </li>
                    @endpermission
                    
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{route('backend.dashboard')}}">Dashboard</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-dashboard"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                  <a href="{{route('contact.index')}}" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-globe"></i>
                                        <b class="caret"></b>
                                        <span class="notification">{{getContact()->count()}}</span>
                                  </a>
                                  
                                  <ul class="dropdown-menu">
                                    @if(getContact()->count())
                                    @foreach(getContact() as $contact)
                                    <li><a href="{{route('contact.show', $contact->id)}}">{{str_limit($contact->message)}}</a></li>
                                    @endforeach
                                    <li class="divider"></li>
                                     @endif
                                    <li><a href="{{route('contact.index')}}">View All Message</a></li>
                                  </ul>
                                 
                            </li>
                            <li class="dropdown">
                                  <a href="{{route('comment.index')}}" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-comment"></i>
                                        <b class="caret"></b>
                                        <span class="notification">{{getComment()->count()}}</span>
                                  </a>
                                 
                                  <ul class="dropdown-menu">
                                    @if(getComment()->count())
                                    @foreach(getComment() as $comment)
                                    <li><a href="{{route('comment.show', $comment->id)}}">{{str_limit($comment->type()->title)}}</a></li>
                                    @endforeach
                                    <li class="divider"></li>
                                    @endif
                                    <li><a href="{{route('comment.index')}}">View All Comments</a></li>
                                  </ul>
                                  
                            </li>
                            <li>
                               <a href="">
                                    <i class="fa fa-search"></i>
                                </a>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li>
                               <a href="{{url('/')}}" target="_blank">
                                   <i class="fa fa-home"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                         <i class="fa fa-user"></i>
                                        <b class="caret"></b>
                                  </a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-user"></i>{{ Auth::user()->name }}</a></li>
                                    <li><a href="{{ route('user.edit', Auth::user()->id)}}"><i class="fa fa-pencil-square-o"></i> Edit Profile</a></li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i>
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                  </ul>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="content">
                <div class="container-fluid">

                    @yield('content')
                    
                </div>
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                   Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.xilwal.com">Tim Xilwal</a>, made with love.
                    </p>
                </div>
            </footer>
        </div>
    </div>
    

    <!--   Core JS Files   -->
    <script src="{{asset('admins/js/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('admins/js/bootstrap.min.js')}}" type="text/javascript"></script>
	{{-- < Charts Plugin 
	<script src="{{asset('admins/js/chartist.min.js')}}"></script> --}}

    <!--  Notifications Plugin    -->
    <script src="{{asset('admins/js/bootstrap-notify.js')}}"></script>
    <script src="{{asset('admins/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('admins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admins/js/jquery.dataTables.min.js')}}"></script>
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{asset('admins/js/light-bootstrap-dashboard.js')}}"></script>
    <script src="{{asset('admins/js/main.js')}}" type="text/javascript"></script>
	{{-- Light Bootstrap Table DEMO methods, don't include it in your project!
	<script src="{{asset('admins/js/demo.js')}}"></script> --}}

	<script type="text/javascript">
    	$(document).ready(function(){
        	//demo.initChartist();
            $('.table-hover').dataTable();
            @if(count($errors->all()))

            	$.notify({
                	icon: 'pe-7s-close-circle',
                	message: "{{$errors->first()}}"

                },{
                    type: 'danger',
                    timer: 4000
                });
            @endif
            @if ($message = Session::get('success'))

                $.notify({
                    icon: 'pe-7s-like2',
                    message: "{{$message}}"
                },{
                    type: 'success',
                    timer: 4000
                });

            @endif
             tinymce.init({          
                selector: 'textarea.editor',  
                plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern',

                });
    	});
	</script>
    @yield('js')
</body>
</html>
