
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="p:domain_verify" content="509ce3761b6ac3177d3685a189e299ec"/>
    <!-- Seo section -->
    {!! SEO::generate()!!}
  <style>
    body { text-align: center; padding: 50px; }
    img {width: 100% ; height:auto; img-align: center;}
    h1 { font-size: 50px; }
    body { font: 20px Helvetica, sans-serif; color: #333; }
    article { display: block; text-align: left; width: 650px; margin: 0 auto; }
    a { color: #dc8100; text-decoration: none; }
    a:hover { color: #333; text-decoration: none; }
  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120908040-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120908040-1');
  </script>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-WDK9J8P');</script>
  <!-- End Google Tag Manager -->
</head>
<body>
<!-- End Google Tag Manager (noscript) -->
{{-- Facebook SDK --}}
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '204116023544434',
      xfbml      : true,
      version    : 'v2.11'
    });

    FB.AppEvents.logPageView();

  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=204116023544434';";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
{{-- <!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution="setup_tool"
  page_id="872495786271312">
</div> --}}
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WDK9J8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<article>
<img src="logo.jpg" alt="Karkalo.com">
    <h1>Coming soon!</h1>
    <div>
        <h2>Our website is on progress. If you need, you can always <a href="mailto:info@karkalo.com">contact us</a>, otherwise we&rsquo;ll be back online shortly!</h2>
        <h3>&mdash; Karkalo Team</h3>
    </div>
</article>
</body>
</html>
