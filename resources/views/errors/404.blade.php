@extends('layouts.front')
@section('content')

<section class="page-sec">
    <div class="intro-header-sec text-center">
        <h1>404 ERROR</h1>
        <hr align="center" style="width:120px;">
        <h5>Page Not Found.</h5>
        <p><a href="{{url('/')}}" class="btn-danger btn">Home</a></p>
    </div>
</section>

@stop