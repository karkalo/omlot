@extends('layouts.backend')
@section('content')
	<section class="page-sec">
    <div class="intro-header-sec text-center">
        <h1>403 ERROR</h1>
        <hr class="title-hr">
        <h5>You dont have required permission.</h5>
        <p><a href="{{url('/login')}}" class="btn-danger btn">Home</a></p>
    </div>
</section>
@stop