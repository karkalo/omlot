@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">

                {{-- @if ($message = Session::get('success'))
	                <div class="alert alert-success">
				        <button type="button" aria-hidden="true" class="close">×</button>
				        	<span>{{$message}}</span>
			    	</div>
			    @endif --}}
                  	<h4 class="title">Testimonials Table with Action  <a href="{{route('testimonial.create')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a></h4>
                    <p class="testimonials">Here is a list for this all testimonials</p>
                   
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-striped">
					    <thead>
					        <tr>
					            <th class="text-center">#</th>
					            <th>Organization</th>
					            <th>Name</th>
					            <th>Position</th>
					            <th>Testimonial</th>
					            <th>Profile</th>
					            <th>Status</th>
					            <th>Actions</th>
					        </tr>
					    </thead>
					    <tbody>
					    	@foreach($testimonials as $key => $testimonial)
					        <tr>
					            <td class="text-center">{{++$key}}</td>
					            <td>{{ $testimonial->company }}</td>
					            <td>{{ $testimonial->name }}</td>
					            <td>{{ $testimonial->position }}</th>
					            <th>{!! str_limit($testimonial->testimonial, 100) !!}</th>
					            <th><img src="{{ $testimonial->profile }}" class="img-thumbnail" height="40" width="50"></th>
					            <td>{{$testimonial->is_active ? 'Active' : 'Deactive'}}</td>
					            
					            <td class="td-actions">
					                <a href="{{route('testimonial.show', $testimonial->id)}}" rel="tooltip" title="View testimonials" class="btn btn-info btn-simple btn-xs">
					                    <i class="fa fa-eye"></i>
					                </a>
					                <a href="{{route('testimonial.edit', $testimonial->id)}}" rel="tooltip" title="Edit testimonials" class="btn btn-success btn-simple btn-xs">
					                    <i class="fa fa-edit"></i>
					                </a>
					                <a data-toggle="modal" data-target="#deleteForm{{$testimonial->id}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
					                    <i class="fa fa-times"></i>
					                </a>
									<!-- Modal -->
									<div class="modal fade" id="deleteForm{{$testimonial->id}}" role="dialog">
										<div class="modal-dialog">
										<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h3 class="modal-title">Delete</h3>
												</div>
												{{Form::open(['route' => ['testimonial.destroy', $testimonial->id],'method' => 'DELETE', 'id' => 'delete'] )}}
												<div class="modal-body">
													
													<strong>Are you sure want to Delete this Testimonials?</strong>
													
												</div>
												<div class="modal-footer">
													<button class="btn btn-small btn-fulltime">Yes</button>
													<button type="button" class="btn btn-small btn-danger" data-dismiss="modal">No</button>
													
												</div>
												{{Form::close()}}
											</div>
										</div>
									</div>
					            </td>
					        </tr>
					       @endforeach
					    </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
@stop