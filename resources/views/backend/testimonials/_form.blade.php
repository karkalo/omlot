<div class="row">
    <div class="col-md-12">
         <label>Organization Name <font color="red">*</font></label>
         {{Form::text('company', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Organization Name ...'])}}
    </div>
</div>
<div class="row">
   
  	 <div class="col-md-6">
      <div class="form-group">
         <label>Name <font color="red">*</font></label>
         {{Form::text('name', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Person Name ...'])}}
      </div>
    </div>
     <div class="col-md-6">
      <div class="form-group">
         <label>Position <font color="red">*</font></label>
         {{Form::text('position', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Title/Position..'])}}
      </div>
    </div>
</div>

<div class="row">
  	<div class="col-md-12">
  		<div class="form-group">
  			 <label>Testimonial <font color="red">*</font> </label>
  			 {{Form::textarea('testimonial', null, ['class' => 'form-control', 'required' => true])}}
  		</div>
  	</div>
</div>
 <div class="row">
  	<div class="col-md-6">
  		<div class="form-group">
  			 <label>Profile: <font color="red">*</font></label>
  			 {{Form::file('file',  [ 'required' => isset($testimonial) ? false : true])}}
  		</div>

      @if(isset($testimonial->profile))
          <img class="img-circle" src="{{$testimonial->profile}}" alt="{{$testimonial->slug}}" height="80" width="70">
      @endif
  	</div>
  	<div class="col-md-2">
  		<div class="form-group">
  			 <label for="active">Active </label>
  			 {{Form::checkbox('is_active', 1, isset($testimonial) ? ($testimonial->is_active ? true : false) : true, ['id' => 'active'])}}
  		</div>
  	</div>
</div>