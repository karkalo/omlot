<div class="row">
   
  	 <div class="col-md-6">
      <div class="form-group">
         <label>Name <font color="red">*</font></label>
         {{Form::text('name', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Name ...'])}}
      </div>
    </div>
     <div class="col-md-6">
      <div class="form-group">
         <label>Website </label>
         {{Form::url('link', null, ['class' => 'form-control', 'placeholder' => 'Website..'])}}
      </div>
    </div>
</div>

 <div class="row">
  	<div class="col-md-6">
  		<div class="form-group">
  			 <label>Logo: <font color="red">*</font></label>
  			 {{Form::file('file',  [ 'required' => isset($partner) ? false : true])}}
  		</div>

      @if(isset($partner->logo))
          <img class="img-responsive img-rounded" src="{{$partner->logo}}" alt="{{$partner->name}}" height="60" width="70">
      @endif
  	</div>
  	<div class="col-md-2">
  		<div class="form-group">
  			 <label for="active">Active </label>
  			 {{Form::checkbox('is_active', 1, isset($partner) ? ($partner->is_active ? true : false) : true, ['id' => 'active'])}}
  		</div>
  	</div>
</div>