@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                  	<h4 class="title">Setting Table with Action  
                  		@permission('setting.store')
                  			<a data-toggle="modal" data-target="#addNew" rel="tooltip" title="Remove" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a>
                  		@endpermission
                  </h4>
                    <p class="role">Here is a list for this all setting</p>
                   <!-- Modal -->
					<div class="modal fade" id="addNew" role="dialog">
						<div class="modal-dialog">
						<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h3 class="modal-title">Add New Setting</h3>
								</div>
								{{Form::open(['route' => ['setting.store'],'method' => 'POST', 'enctype' => 'multipart/form-data', 'novalidate' => true] )}}
								<div class="modal-body">
									<div class="form-group">
										<label>Setting Name</label>
										{{Form::text('label', null, ['class' =>'form-control', 'required' => true, 'placeholder' => 'Enter Setting Name (eg. Meta Keyword..'])}}
									</div>
									<div class="form-group">
										<label>Setting Key</label>
										{{Form::text('key', null, ['class' =>'form-control', 'required' => true, 'placeholder' => 'Enter Setting Key (eg. meta_keyword..'])}}
									</div>
									<div class="form-group setting-type">
										<label>Setting Type:</label>
										<br>
										{{Form::label('text')}}
										{{Form::radio('type', 'text', true)}}
										{{Form::label('textarea')}}
										{{Form::radio('type', 'textarea')}}
										{{Form::label('file')}}
										{{Form::radio('type', 'file')}}
									</div>
									<div class="form-group">
										<label>Setting Value</label>
										<div class="setting-input"> 
											{{Form::text('value', null, ['class' =>'form-control', 'placeholder' => 'Enter Setting Value (eg. Adversitment Agency, Keyword, ..', 'id' => 'setting-text'])}}
											{{Form::file('file', ['id' => 'setting-file'])}}
											{{Form::textarea('value', null, ['class' => 'form-control', 'id' => 'setting-textarea'])}}
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button class="btn btn-small btn-success">Create</button>
									<button type="button" class="btn btn-small btn-danger" data-dismiss="modal">Cancel</button>
								</div>
								{{Form::close()}}
							</div>
						</div>
					</div>
                </div>
                <div class="content">
                	@if(count($settings))
                	{{Form::open(['route' => ['setting.store'],'method' => 'POST', 'enctype' => 'multipart/form-data'])}}
                		<div class="row">
                			@foreach($settings as $setting)
                			<div class="col-md-6">
                				<label>{{$setting->label}}</label>
                				@if($setting->type == 'text')
                					<input type="hidden" name="type" value="text">
                					<input type="text" name="{{$setting->key}}" value="{{$setting->value}}" class="form-control">
                				@endif
                				@if($setting->type == 'textarea')
                					<textarea class="form-control" name="{{$setting->key}}">{{$setting->value}}</textarea>
                					<input type="hidden" value="textarea" name="type">
                				@endif
                				@if($setting->type == 'file')
                					<input type="hidden" value="{{$setting->value}}" name="{{$setting->key}}">
                					<input type="file" name="{{$setting->key}}-file" class="form-control">
                					<input type="hidden" value="file" name="type">
                					<img src="{{$setting->value}}" class="img-responsive" style="height:80px">
                				@endif
                			</div>
                			@endforeach
                		</div>
                		<br>
                		 <button type="submit" class="btn btn-info btn-fill pull-right" name="update">Update</button>
                    <div class="clearfix"></div>
                    {{Form::close()}}
                   	@endif
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
	<script type="text/javascript">
		$(document).ready( function () {
			var text = $('#setting-text');
			var textarea = $('#setting-textarea');
			var file = $('#setting-file');
			text.show();
			textarea.hide();
			file.hide();	
			$(".setting-type input[name='type']").on('click', function(){
				var type = $('input[name="type"]:checked').val();
				text.val('');
				textarea.val('');
				file.val('');
				if(type == 'text') {
					text.show();
					textarea.hide();
					file.hide();	
				}
				if(type == 'textarea') {
					text.hide();
					textarea.show();
					file.hide();	
				}
				if(type == 'file') {
					text.hide();
					textarea.hide();
					file.show();	
				}
			});
		});
	</script>
@stop