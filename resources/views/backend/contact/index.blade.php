@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                  	<h4 class="title">Contact Table with Action.</h4>
                    <p class="user">Here is a list for this all message</p>
                   
                </div>
                <div class="content table-responsive">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Subject</th>
							
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Subject</th>
							
							<th>Status</th>
							<th>Action</th>
						</tr>
					</tfoot>
					<tbody>
					@foreach($contacts as $contact)
						<tr>
							<td>{{$contact->id}}</td>
							<td>{{$contact->name}}</td>
							<td>{!! $contact->email !!}</td>
							<td>{{$contact->phone}}</td>
							<td>{{$contact->subject}}</td>
							<td>{{$contact->is_viewed === 1 ? 'Seen' : 'Unseen'}}</td>
							<td class="td-actions">
				                <a href="{{route('contact.show', $contact->id)}}" rel="tooltip" title="View Mesage" class="btn btn-info btn-simple btn-xs">
				                    <i class="fa fa-eye"></i>
				                </a>
				    
					        </td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
