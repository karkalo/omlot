@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                  	<h4 class="title">Contact Table with Action.</h4>
                    <p class="user">Here is a list for this all message</p>
                   
                </div>
                <div class="content table-responsive">
					<table class="table">
						<tr>
							<th>Name</th>
							<th>{{$contact->name}}</th>
						</tr>
						<tr>
							<th>Email</th>
							<th>{{$contact->email}}</th>
						</tr>
						<tr>
							<th>Phone</th>
							<th>{{$contact->phone}}</th>
						</tr>
						<tr>
							<th>Date</th>
							<th>{{$contact->created_at}}</th>
						</tr>
						<tr>
							<th>Message</th>
							<th>{{$contact->message}}</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
