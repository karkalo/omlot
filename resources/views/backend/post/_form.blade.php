<div class="row">
    <div class="col-md-12">
  		<div class="form-group">
  			 <label>Post Title <font color="red">*</font></label>
  			 {{Form::text('title', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Post Title..'])}}
  		</div>
  	</div>
</div>
<div class="row">
  	<div class="col-md-6">
  		<div class="form-group">
  			  <label>Keyword <small style="text-transform: lowercase;">(with commas .. eg('Agency in Nepal, we sell your product,)</small> </label>
  			 {{Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Keywords with commas'])}}
  		</div>
  	</div>

    <div class="col-md-6">
        <div class="form-group">
            <label>Category<font color="red">*</font></label>
            {{Form::select('category_id', $categories, null, ['class' => 'form-control', 'required' => true])}}
        </div>
    </div>
</div>
<div class="row">
  	<div class="col-md-12">
  		<div class="form-group">
  			 <label>Description <small style="text-transform: lowercase;">(max : 240 words, this is a meta description for seo)</small> </label>
  			 {{Form::textarea('description', null, ['class' => 'form-control', 'maxlength' => 240, 'size' => '3x5', 'placeholder' => 'Meta Description for SEO'])}}
  		</div>
  	</div>
</div>
<div class="row">
  	<div class="col-md-12">
  		<div class="form-group">
  			 <label>Content </label>
  			 {{Form::textarea('content', null, ['class' => 'form-control editor', 'size' => '3x5', 'placeholder' => 'post Content'])}}
  		</div>
  	</div>
 </div>
 <div class="row">
  	<div class="col-md-3">
  		<div class="form-group">
  			 <label>Image </label>
  			 {{Form::file('file')}}
  		</div>

      @if(isset($post->image))
          <img class="img-circle" src="{{$post->image}}" alt="{{$post->slug}}" height="80" width="70">
      @endif
  	</div>
    <div class="col-md-3">
      <div class="form-group">
         <label>Video Url </label>
         {{Form::text('video', null, ['class' => 'form-control', 'placeholder' => 'Youtube Video Url'])}}
      </div>

      @if(isset($post->video))
          <iframe width="100" height="100" src="{{$post->video}}"></iframe> 
      @endif
    </div>
  	<div class="col-md-2">
  		<div class="form-group">
  			 <label for="active">Active </label>
  			 {{Form::checkbox('is_active', 1, isset($post) ? ($post->is_active ? true : false) : true, ['id' => 'active'])}}
  		</div>
  	</div>
  	<div class="col-md-2">
  		<div class="form-group">
  			 <label for="featured">Featured </label>
  			 {{Form::checkbox('is_featured', 1, isset($post) ? ($post->is_featured ? true : false) : false, ['id' => 'featured'])}}
  		</div>
  	</div>
</div>