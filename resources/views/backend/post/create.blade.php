@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
            	
                <h4 class="title">Add New Post</h4>
            </div>
            <div class="content">
                {{Form::open(['route' => ['post.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data'])}}
                    @include('backend.post._form')
                    <button type="submit" class="btn btn-info btn-fill pull-right">Create</button>
                    <div class="clearfix"></div>
               	{{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop