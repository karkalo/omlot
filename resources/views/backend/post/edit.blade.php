@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
            	
                <h4 class="title">Edit Post {{$post->title}}</h4>
            </div>
            <div class="content">
                {{Form::model($post, ['route' => ['post.update', $post->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data'])}}
                    @include('backend.post._form')
                    <button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
                    <div class="clearfix"></div>
               	{{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop