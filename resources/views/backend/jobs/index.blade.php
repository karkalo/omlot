@extends('layouts.backend')
@section('content')
  <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">

               {{--  @if ($message = Session::get('success'))
                  <div class="alert alert-success">
                <button type="button" aria-hidden="true" class="close">×</button>
                  <span>{{$message}}</span>
            </div>
          @endif --}}
                    <h4 class="title">Job Table with action   
                  <a href="{{route('job.create')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a>
               </h4>
                    <p class="category">Here is a list for this all job</p>
                   
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-striped table-sm">
              <thead>
                  <tr>
                      <th class="text-center">#</th>
                      <td>Title</td>
                      <td>Description</td>
                      <td>status</td>
                      <td>slug</td>
                      <td>Category ID</td>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($jobs as $key => $job)
                  <tr>
                      <td class="text-center">{{++$key}}</td>
                      <td>{{ $job->title }}</td>
                      <td>{{ $job->description }}</td>
                      <td>{{ $job->status}}</td>
                      <td>{{ $job->slug }}</td>
                      <td>{{ $job->category? $job->category->title : '' }}</td>  
                      <td class="td-actions">
                          <a href="{{route('job.show', $job->id)}}" rel="tooltip" title="View Category" class="btn btn-info btn-simple btn-xs">
                            <i class="fa fa-eye"></i>
                          </a>
                        
                            <a href="{{route('job.edit', $job->id)}}" rel="tooltip" title="Edit Job" class="btn btn-success btn-simple btn-xs">
                            <i class="fa fa-edit"></i>
                          </a>
             
                          <a data-toggle="modal" data-target="#deleteForm{{$job->id}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                              <i class="fa fa-times"></i>
                          </a>
                         
                  <!-- Modal -->
                  <div class="modal fade" id="deleteForm{{$job->id}}" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h3 class="modal-title">Delete</h3>
                        </div>
                        {{Form::open(['route' => ['job.destroy', $job->id],'method' => 'DELETE', 'id' => 'delete'] )}}
                        <div class="modal-body">
                          
                          <strong>Are you sure want to Delete this Job?</strong>
                          
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-small btn-fulltime">Yes</button>
                          <button type="button" class="btn btn-small btn-danger" data-dismiss="modal">No</button>
                          
                        </div>
                        {{Form::close()}}
                      </div>
                    </div>
                  </div>
                      </td>
                  </tr>
                 @endforeach
              </tbody>
          </table>
                </div>
            </div>
        </div>
    </div>
@stop