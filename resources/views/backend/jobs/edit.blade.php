@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
              {{-- @if(count($errors))
              <div class="alert alert-danger">
              <button type="button" aria-hidden="true" class="close">×</button>
              @foreach($errors as $error)
                <span>{{$error}}</span>
              @endforeach
          </div>
          @endif --}}
                <h4 class="title">Edit Job</h4>
            </div>
            <div class="content">
                {{Form::model($job, ['route' => ['job.update', $job->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data'])}}
                    @include('backend.jobs._form')
                    <button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
                    <div class="clearfix"></div>
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop


