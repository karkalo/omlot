@extends('layouts.backend')

@section('content')
  <div class="row">
    job show
    <div class="col-12">
      <div class="form-group">
        <label for="title">Title</label>
        <input name="title" type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title" value="{{ $job->title }}">
        <small id="titleHelp" class="form-text text-muted">title of job.</small>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">status</label>
        <select class="form-control" name="status" value={{ $job->status }}>
          <option>Default select</option>
          <option value="0">Active</option>
          <option value="1">Inactive</option>
        </select>
      </div>
      <div class="form-group">
        <label for="slug">Slug</label>
        <input name="slug" type="text" value="{{ $job->slug }}" class="form-control" id="slug" aria-describedby="slugHelp" placeholder="Enter slug">
      </div>
      <div class="form-group">
        <label for="parent">Category</label>
        <input name="category_id" type="text" value="{{ $job->category_id }}" class="form-control" id="part" aria-describedby="parentHelp" placeholder="Enter parent category">
      </div>
      <div class="form-group">
        <label for="exampleDescription">Description</label>
        <textarea name="description" value="{{ $job->description }}" class="form-control" rows="3" id="exampleDescription"></textarea>
      </div>
    </div>
      
  </div>

@endsection

