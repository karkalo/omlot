<div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <label>Title <font color="red">*</font></label>
         {{Form::text('title', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Job Title..'])}}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
          <label>Status</label>
          {{ Form::select('status', [''=> 'Select Status', '1' => 'Active', '0' => 'Inactive'],null, ['id' => 'status', 'class' => 'form-control']) }}
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="form-group">
         <label>Description</label>
         {{Form::textarea('description', null, ['class' => 'form-control', 'maxlength' => 240, 'size' => '3x5', 'plcaeholder' => 'Job description'])}}
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <label>Slug <font color="red">*</font></label>
         {{Form::text('slug', null, ['class' => 'form-control', 'required' => 'false', 'placeholder' => 'Slug..'])}}
      </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Category</label>
            {{Form::select('category_id', $categories , isset($job->category) ? $job->category->id: null, ['class' => 'form-control'])}}
        </div>
    </div>
</div>
