<div class="row">
    <div class="col-md-12">
      <div class="form-group">
         <label>Title <font color="red">*</font></label>
         {{Form::text('title', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Gallery Title..'])}}
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
      <div class="form-group">
          <label>Keyword <small style="text-transform: lowercase;">(with commas .. eg('Agency in Nepal, we sell your product,)</small></label>
         {{Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Keywords with commas'])}}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
          <label>Link <small style="text-transform: lowercase;">(Url link to redirect for this gallery)</small></label>
         {{Form::url('link', null, ['class' => 'form-control', 'placeholder' => 'Enter redirect link (http://example.com)'])}}
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="form-group">
         <label>Description <small style="text-transform: lowercase;">(max : 240 words, this is a meta description for seo)</small> <small style="text-transform: lowercase;">(max : 240 words, this is a meta description for seo)</small></label>
         {{Form::textarea('description', null, ['class' => 'form-control', 'maxlength' => 240, 'size' => '3x5', 'plcaeholder' => 'Meta Description for SEO'])}}
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="form-group">
         <label>Content </label>
         {{Form::textarea('content', null, ['class' => 'form-control editor'])}}
      </div>
    </div>
 </div>
 <div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <label>Image </label>
         {{Form::file('file')}}
      </div>

      @if(isset($gallery->image))
          <img class="img-responsive img-rounded" src="{{$gallery->image}}" alt="{{$gallery->slug}}" height="60" width="80">
      @endif
    </div>
    <div class="col-md-2">
      <div class="form-group">
         <label for="active">Active </label>
         {{Form::checkbox('is_active', 1, isset($gallery) ? ($gallery->is_active ? true : false) : true, ['id' => 'active'])}}
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
         <label for="menu">Slider </label>
         {{Form::checkbox('is_slider', 1, isset($gallery) ? ($gallery->is_slider ? true : false) : false, ['id' => 'menu'])}}
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
         <label for="featured">Featured </label>
         {{Form::checkbox('is_featured', 1, isset($gallery) ? ($gallery->is_featured ? true : false) : false, ['id' => 'featured'])}}
      </div>
    </div>
</div>