<div class="row">
    <div class="col-md-12">
  		<div class="form-group">
  			 <label>Question: <font color="red">*</font></label>
  			 {{Form::text('question', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Faq Ouestion..'])}}
  		</div>
  	</div>
</div>
<div class="row">
  	 <div class="col-md-12">
      <div class="form-group">
         <label>Answer: <font color="red">*</font></label>
         {{Form::textarea('answer', null, ['class' => 'form-control editor', 'required' => true, 'placeholder' => 'Answer ...'])}}
      </div>
    </div>
</div>
<div class="row">
  <div class="col-md-2">
      <div class="form-group">
         <label for="active">Active </label>
         {{Form::checkbox('is_active', 1, isset($faq) ? ($faq->is_active ? true : false) : true, ['id' => 'active'])}}
      </div>
</div>
</div>