@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
            	
                <h4 class="title">Add New Faq</h4>
                {{-- @if(count($errors))
                <div class="alert alert-danger">
                    <button type="button" aria-hidden="true" class="close">×</button>
                    @foreach($errors as $error)
                        <span>{{$error}}</span>
                    @endforeach
                </div>
                @endif --}}
            </div>
            <div class="content">
                {{Form::open(['route' => ['faq.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'novalidate' => true])}}
                    @include('backend.faq._form')
                    <button type="submit" class="btn btn-info btn-fill pull-right">Create</button>
                    <div class="clearfix"></div>
               	{{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop