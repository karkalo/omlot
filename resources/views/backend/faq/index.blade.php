@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">

                {{-- @if ($message = Session::get('success'))
	                <div class="alert alert-success">
				        <button type="button" aria-hidden="true" class="close">×</button>
				        	<span>{{$message}}</span>
			    	</div>
			    @endif --}}
                  	<h4 class="title">User Table with Action  <a href="{{route('faq.create')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a></h4>
                    <p class="user">Here is a list for this all user</p>
                   
                </div>
                <div class="content table-responsive">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Questions</th>
							
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Questions</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</tfoot>
					<tbody>
					@foreach($faqs as $faq)
						<tr>
							<td>{{$faq->id}}</td>
							<td>{{$faq->question}}</td>
							<td>{{$faq->is_active === 1 ? 'Active' : 'Deactive'}}</td>
							<td class="td-actions">
				            
				                <a href="{{route('faq.edit', $faq->id)}}" rel="tooltip" title="Edit faq" class="btn btn-success btn-simple btn-xs">
				                    <i class="fa fa-edit"></i>
				                </a>
				                <a data-toggle="modal" data-target="#deleteForm{{$faq->id}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
				                    <i class="fa fa-times"></i>
				                </a>
								<!-- Modal -->
								<div class="modal fade" id="deleteForm{{$faq->id}}" role="dialog">
									<div class="modal-dialog">
									<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h3 class="modal-title">Delete</h3>
											</div>
											{{Form::open(['route' => ['faq.destroy', $faq->id],'method' => 'DELETE', 'id' => 'delete'] )}}
											<div class="modal-body">
												
												<strong>Are you sure want to Delete this faq?</strong>
												
											</div>
											<div class="modal-footer">
												<button class="btn btn-small btn-fulltime">Yes</button>
												<button type="button" class="btn btn-small btn-danger" data-dismiss="modal">No</button>
												
											</div>
											{{Form::close()}}
										</div>
									</div>
								</div>
					        </td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
