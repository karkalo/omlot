@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
            	{{-- @if(count($errors))
            	<div class="alert alert-danger">
			        <button type="button" aria-hidden="true" class="close">×</button>
			        @foreach($errors as $error)
			        	<span>{{$error}}</span>
			        @endforeach
		    	</div>
		    	@endif --}}
                <h4 class="title">Edit Page</h4>
            </div>
            <div class="content">
                {{Form::model($page, ['route' => ['page.update', $page->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data'])}}
                    @include('backend.page._form')
                    <button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
                    <div class="clearfix"></div>
               	{{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop