@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">

                  	<h4 class="title">Manage Page Table with Action  <a href="{{route('page.create')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a></h4>
                    <p class="page">Here is a list for this all Page.</p>
                   
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-striped">
					    <thead>
					        <tr>
					            <th class="text-center">#</th>
					            <th>Title</th>
					            <th>Description</th>
					            <th>Type</th>
					            <th>Image</th>
					            <th>Status</th>
					            <th>Actions</th>
					        </tr>
					    </thead>
					    <tbody>
					    	@foreach($pages as $key => $page)
					        <tr>
					            <td class="text-center">{{++$key}}</td>
					            <td>{{$page->title}}</td>
					            <td>{{$page->description}}</th>
					            <td>{{$page->type}}</td>
					            <th><img src="{{$page->image}}"></th>
					            <td>{{$page->is_active ? 'Active' : 'Deactive'}}</td>
					            <td class="td-actions">
					                <a href="{{route('page.show', $page->id)}}" rel="tooltip" title="View Page" class="btn btn-info btn-simple btn-xs">
					                    <i class="fa fa-eye"></i>
					                </a>
					                <a href="{{route('page.edit', $page->id)}}" rel="tooltip" title="Edit Page" class="btn btn-success btn-simple btn-xs">
					                    <i class="fa fa-edit"></i>
					                </a>
					                <a data-toggle="modal" data-target="#deleteForm{{$page->id}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
					                    <i class="fa fa-times"></i>
					                </a>
									<!-- Modal -->
									<div class="modal fade" id="deleteForm{{$page->id}}" role="dialog">
										<div class="modal-dialog">
										<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h3 class="modal-title">Delete</h3>
												</div>
												{{Form::open(['route' => ['page.destroy', $page->id],'method' => 'DELETE', 'id' => 'delete'] )}}
												<div class="modal-body">
													
													<strong>Are you sure want to Delete this Page "{{$page->title}}"?</strong>
													
												</div>
												<div class="modal-footer">
													<button class="btn btn-small btn-fulltime">Yes</button>
													<button type="button" class="btn btn-small btn-danger" data-dismiss="modal">No</button>
													
												</div>
												{{Form::close()}}
											</div>
										</div>
									</div>
					            </td>
					        </tr>
					       @endforeach
					    </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
@stop