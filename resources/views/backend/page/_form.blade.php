<div class="row">
    <div class="col-md-12">
  		<div class="form-group">
  			 <label>Page Title <font color="red">*</font></label>
  			 {{Form::text('title', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Page Title..'])}}
  		</div>
  	</div>
</div>
<div class="row">
  	<div class="col-md-6">
  		<div class="form-group">
  			  <label>Keyword <small style="text-transform: lowercase;">(with commas .. eg('Agency in Nepal, we sell your product,)</small> </label>
  			 {{Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Keywords with commas'])}}
  		</div>
  	</div>

    <div class="col-md-3">
        <div class="form-group">
            <label>Category</label>
            {{Form::select('category_id', $categories, null, ['class' => 'form-control'])}}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Type</label>
            {{Form::select('type', pageType(), null, ['class' => 'form-control'])}}
        </div>
    </div>
</div>
<div class="row">
  	<div class="col-md-12">
  		<div class="form-group">
  			 <label>Description <small style="text-transform: lowercase;">(max : 240 words, this is a meta description for seo)</small> </label>
  			 {{Form::textarea('description', null, ['class' => 'form-control', 'maxlength' => 240, 'size' => '3x5', 'placeholder' => 'Meta Description for SEO'])}}
  		</div>
  	</div>
</div>
<div class="row">
  	<div class="col-md-12">
  		<div class="form-group">
  			 <label>Content </label>
  			 {{Form::textarea('content', null, ['class' => 'form-control editor', 'size' => '3x5', 'placeholder' => 'Page Content'])}}
  		</div>
  	</div>
 </div>
 <div class="row">
  	<div class="col-md-3">
  		<div class="form-group">
  			 <label>Image </label>
  			 {{Form::file('file')}}
  		</div>

      @if(isset($page->image))
          <img class="img-responsive img-rounded" src="{{$page->image}}" alt="{{$page->slug}}" height="60" width="80">
      @endif
  	</div>
    <div class="col-md-3">
      <div class="form-group">
         <label>Video Url <small style="text-transform: lowercase;">(Video Url : (embed code url only))</small></label>
         {{Form::text('video', null, ['class' => 'form-control', 'placeholder' => 'Youtube Video Url'])}}
      </div>

      @if(isset($page->video))
          <iframe width="100" height="100" src="{{$page->video}}"></iframe> 
      @endif
    </div>
  	<div class="col-md-2">
  		<div class="form-group">
  			 <label for="active">Active </label>
  			 {{Form::checkbox('is_active', 1, isset($page) ? ($page->is_active ? true : false) : true, ['id' => 'active'])}}
  		</div>
  	</div>
  	<div class="col-md-2">
  		<div class="form-group">
  			 <label for="menu">Menu </label>
  			 {{Form::checkbox('is_menu', 1, isset($page) ? ($page->is_menu ? true : false) : false, ['id' => 'menu'])}}
  		</div>
  	</div>
  	<div class="col-md-2">
  		<div class="form-group">
  			 <label for="featured">Featured </label>
  			 {{Form::checkbox('is_featured', 1, isset($page) ? ($page->is_featured ? true : false) : false, ['id' => 'featured'])}}
  		</div>
  	</div>
</div>