@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">

               {{--  @if ($message = Session::get('success'))
	                <div class="alert alert-success">
				        <button type="button" aria-hidden="true" class="close">×</button>
				        	<span>{{$message}}</span>
			    	</div>
			    @endif --}}
                  	<h4 class="title">Category Table with Action    
                	<a href="{{route('category.create')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a>
               </h4>
                    <p class="category">Here is a list for this all category</p>
                   
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-striped table-sm">
					    <thead>
					        <tr>
					            <th class="text-center">#</th>
					            <th>Title</th>
					            <th>Image</th>
					            <th>Description</th>
					            <th>Status</th>
					            <th>Actions</th>
					        </tr>
					    </thead>
					    <tbody>
					    	@foreach($categories as $key => $category)
					        <tr>
					            <td class="text-center">{{++$key}}</td>
					            <td>{{$category->title}}</td>
					            <th><img src="{{$category->image}}"></th>
					            <td>{{$category->description}}</th>
					            <td>{{$category->is_active ? 'Active' : 'Deactive'}}</td>
					            <td class="td-actions">
				                	<a href="{{route('category.show', $category->id)}}" rel="tooltip" title="View Category" class="btn btn-info btn-simple btn-xs">
				                    <i class="fa fa-eye"></i>
				                	</a>
				                
				                  	<a href="{{route('category.edit', $category->id)}}" rel="tooltip" title="Edit Category" class="btn btn-success btn-simple btn-xs">
				                    <i class="fa fa-edit"></i>
				                	</a>
					   
					                <a data-toggle="modal" data-target="#deleteForm{{$category->id}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
					                    <i class="fa fa-times"></i>
					                </a>
					               
									<!-- Modal -->
									<div class="modal fade" id="deleteForm{{$category->id}}" role="dialog">
										<div class="modal-dialog">
										<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h3 class="modal-title">Delete</h3>
												</div>
												{{Form::open(['route' => ['category.destroy', $category->id],'method' => 'DELETE', 'id' => 'delete'] )}}
												<div class="modal-body">
													
													<strong>Are you sure want to Delete this Category?</strong>
													
												</div>
												<div class="modal-footer">
													<button class="btn btn-small btn-fulltime">Yes</button>
													<button type="button" class="btn btn-small btn-danger" data-dismiss="modal">No</button>
													
												</div>
												{{Form::close()}}
											</div>
										</div>
									</div>
					            </td>
					        </tr>
					       @endforeach
					    </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
@stop