@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
            	{{-- @if(count($errors))
            	<div class="alert alert-danger">
			        <button type="button" aria-hidden="true" class="close">×</button>
			        @foreach($errors as $error)
			        	<span>{{$error}}</span>
			        @endforeach
		    	</div>
		    	@endif --}}
                <h4 class="title">Add New Category</h4>
            </div>
            <div class="content">
                {{Form::open(['route' => ['category.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data'])}}
                    @include('backend.category._form')
                    <button type="submit" class="btn btn-info btn-fill pull-right">Create</button>
                    <div class="clearfix"></div>
               	{{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop