@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
              {{-- @if(count($errors))
              <div class="alert alert-danger">
              <button type="button" aria-hidden="true" class="close">×</button>
              @foreach($errors as $error)
                <span>{{$error}}</span>
              @endforeach
          </div>
          @endif --}}
                <h4 class="title">Edit User</h4>
            </div>
            <div class="content">
                {{Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data'])}}
                   <div class="row">
                       <div class="col-md-6">
                        <div class="form-group">
                           <label>Name <font color="red">*</font></label>
                           {{Form::text('name', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Name ...'])}}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                           <label>Email <font color="red">*</font></label>
                           {{Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email ...', 'required' => true])}}
                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                           <label>Password </label>
                           {{Form::password('passwords', ['class' => 'form-control', 'placeholder' => '*******'])}}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                           <label>Confirm Password </label>
                           {{Form::password('passwords_confirmation', ['class' => 'form-control', 'placeholder' => '*******'])}}
                        </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                           <label for="active">Role </label>
                           {{Form::select('role_id', $roles, null, ['class' => 'form-control', 'required' => true])}}
                        </div>
                      </div>
                      <div class="col-md-offset-1 col-md-2">
                        <div class="form-group">
                           <label for="active">Active </label>
                           {{Form::checkbox('is_active', 1, isset($user) ? ($user->is_active ? true : false) : true, ['id' => 'active'])}}
                        </div>
                      </div>
                  </div>
                    <button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
                    <div class="clearfix"></div>
                {{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop