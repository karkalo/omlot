<div class="row">
  	 <div class="col-md-6">
      <div class="form-group">
         <label>Name <font color="red">*</font></label>
         {{Form::text('name', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Name ...'])}}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
         <label>Email <font color="red">*</font></label>
         {{Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email ...', 'required' => true])}}
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <label>Password <font color="red">*</font></label>
         {{Form::password('passwords', ['class' => 'form-control', 'required' => 'true', 'placeholder' => '*******', 'required' => true])}}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
         <label>Confirm Password <font color="red">*</font></label>
         {{Form::password('passwords_confirmation', ['class' => 'form-control', 'required' => 'true', 'placeholder' => '*******', 'required' => true])}}
      </div>
    </div>
</div>
<div class="row">
  <div class="col-md-6">
      <div class="form-group">
         <label for="active">Role </label>
         {{Form::select('role_id', $roles, null, ['class' => 'form-control', 'required' => true])}}
      </div>
    </div>
  	<div class="col-md-offset-1 col-md-2">
  		<div class="form-group">
  			 <label for="active">Active </label>
  			 {{Form::checkbox('is_active', 1, isset($user) ? ($user->is_active ? true : false) : true, ['id' => 'active'])}}
  		</div>
  	</div>
</div>