@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                  	<h4 class="title">Comment Table with Action.</h4>
                    <p class="user">Here is a list for this all quotations</p>
                   
                </div>
                <div class="content table-responsive">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Subject</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Subject</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</tfoot>
					<tbody>
					@foreach($comments as $comment)
						<tr>
							<td>{{$comment->id}}</td>
							<td>{{$comment->name}}</td>
							<td>{!! $comment->email !!}</td>
							<td>{{$comment->phone}}</td>
							<td>{{ucfirst($comment->type()->title)}}</td>
							<td>{{$comment->is_active === 1 ? 'Seen' : 'Unseen'}}</td>
							<td class="td-actions">
				                <a href="{{route('comment.show', $comment->id)}}" rel="tooltip" title="View Mesage" class="btn btn-info btn-simple btn-xs">
				                    <i class="fa fa-eye"></i>
				                </a>
				    
					        </td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
