@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                  	<h4 class="title">Comment Table with Action. <a href="{{route('comment.index')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-eye"></i> Show all</a></h4>
                    <p class="user">Here is a list for this all quotations</p>
                   
                </div>
                <div class="content table-responsive">
					<table class="table">
						<tr>
							<th>Name</th>
							<th>{{$comment->name}}</th>
						</tr>
						<tr>
							<th>Email</th>
							<th>{{$comment->email}}</th>
						</tr>
						<tr>
							<th>Phone</th>
							<th>{{$comment->phone}}</th>
						</tr>
						<tr>
							<th>Date</th>
							<th>{{$comment->created_at}}</th>
						</tr>
						<tr>
							<th>{{$comment->type()->category->title}}</th>
							<th>{{$comment->type()->title}}</th>
						</tr>
						@if($comment->image)
							<tr>
								<th>Attachments</th>
								<th><img src="{{$comment->image}}" class="img img-fluid"></th>
							</tr>
						@endif
						<tr>
							<th>Requirements</th>
							<th>{{$comment->comment}}</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
