@extends('layouts.backend')
@section('content')
	<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
          
                  	<h4 class="title">Manage Role Table with Action  <a href="{{route('role.create')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a></h4>
                    <p class="role">Here is a list for this all role</p>
                   
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-striped">
					    <thead>
					        <tr>
					            <th class="text-center">#</th>
					            <th>Name</th>
					            <th>Permission</th>
					            <th>Actions</th>
					        </tr>
					    </thead>
					    <tbody>
					    	@foreach($roles as $key => $role)
					        <tr>
					            <td class="text-center">{{++$key}}</td>
					            <td>{{$role->name}}</td>
					            <td><span class="badge">{{count($role->permissions)}}</span></th>
					            <td class="td-actions">
					               
					                <a href="{{route('role.edit', $role->id)}}" rel="tooltip" title="Edit Role" class="btn btn-success btn-simple btn-xs">
					                    <i class="fa fa-edit"></i>
					                </a>
					                <a data-toggle="modal" data-target="#deleteForm{{$role->id}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
					                    <i class="fa fa-times"></i>
					                </a>
									<!-- Modal -->
									<div class="modal fade" id="deleteForm{{$role->id}}" role="dialog">
										<div class="modal-dialog">
										<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h3 class="modal-title">Delete</h3>
												</div>
												{{Form::open(['route' => ['role.destroy', $role->id],'method' => 'DELETE', 'id' => 'delete'] )}}
												<div class="modal-body">
													
													<strong>Are you sure want to Delete this Role?</strong>
													
												</div>
												<div class="modal-footer">
													<button class="btn btn-small btn-fulltime">Yes</button>
													<button type="button" class="btn btn-small btn-danger" data-dismiss="modal">No</button>
													
												</div>
												{{Form::close()}}
											</div>
										</div>
									</div>
					            </td>
					        </tr>
					       @endforeach
					    </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
@stop