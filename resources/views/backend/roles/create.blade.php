@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Add New Role</h4>
            </div>
            <div class="content">
                {{Form::open(['route' => ['role.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data'])}}
                    <div class="row">
                         <div class="col-md-12">
                          <div class="form-group">
                             <label>Name <font color="red">*</font></label>
                             {{Form::text('name', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Name ...'])}}
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                             <label>Permission <font color="red">*</font></label>
                             <div class="row">
                                  @foreach($permissions as $object => $controller)
                                      <div class="col-md-3">
                                          <h5>{{ ucfirst($object) }}</h5>
                                          @foreach($controller as $permission)
                                          <div class="checkboxes in-row ">
                                              <input id="{{$permission->id}}" type="checkbox" value="{{ $permission->id }}" name="permission_ids[]" {{ !in_array($permission->id, old('permission_ids', [])) ?: 'checked="true"' }} ><label for="{{$permission->id}}">{{ $permission->action }}</label>
                                          </div>
                                          @endforeach
                                      </div>
                                  @endforeach
                              </div>
                          </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info btn-fill pull-right">Create</button>
                    <div class="clearfix"></div>
               	{{Form::close()}}
            </div>
        </div>
    </div>

</div>

@stop