@extends('layouts.backend')

@section('content')
  <div class="row">
    <div class="col-12">  
      <div class="form-group">
        <label for="user">user</label>
        <input name="user_id" value="{{ $worker->user_id }}" type="text" class="form-control" id="user" aria-describedby="userHelp" placeholder="Enter user">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">status</label>
        <select class="form-control" name="status" value="{{ $worker->status }}">
          <option>Default select</option>
          <option value="0">Active</option>
          <option value="1">Inactive</option>
        </select>
      </div>
      <div class="form-group">
        <label for="slug">Slug</label>
        <input name="slug" value="{{ $worker->slug }}" type="text" class="form-control" id="slug" aria-describedby="slugHelp" placeholder="Enter slug" >
      </div>
      <div class="form-group">
        <label for="parent">Hour Rate</label>
        <input name="hour_rate" value="{{ $worker->hour_rate }}" type="text" class="form-control" id="part" aria-describedby="parentHelp" placeholder="Enter hour rate">
      </div>
      <div class="form-group">
        <label for="parent">Working Hour per Day</label>
        <input name="working_hour_per_day" value="{{ $worker->working_hour_per_day }}" type="text" class="form-control" id="part" aria-describedby="parentHelp" placeholder="Enter hour per day">
      </div>
      <div class="form-group">
        <label for="parent">Working Day per Month</label>
        <input name="working_day_per_month" value="{{ $worker->working_day_per_month }}" type="text" class="form-control" id="part" aria-describedby="parentHelp" placeholder="Enter day per month">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Rate Type</label>
        <select class="form-control" name="rate_type" value="{{ $worker->rate_type }}">
          <option>Default select</option>
          <option value="0">Hourly</option>
          <option value="1">Daily</option>
          <option value="1">Weekly</option>
          <option value="1">Monthly</option>
        </select>
      </div>
      <div class="form-group">
        <label for="parent">Availability</label>
        <input name="availability" type="text" class="form-control" id="part" aria-describedby="parentHelp" placeholder="Availability" value="{{ $worker->availability }}" >
      </div>
    </div>
  </div>
@endsection