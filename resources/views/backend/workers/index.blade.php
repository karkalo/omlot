@extends('layouts.backend')
@section('content')
  <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">

               {{--  @if ($message = Session::get('success'))
                  <div class="alert alert-success">
                <button type="button" aria-hidden="true" class="close">×</button>
                  <span>{{$message}}</span>
            </div>
          @endif --}}
                    <h4 class="title">Worker Table with action   
                  <a href="{{route('worker.create')}}" class="btn-success btn-sm btn pull-right"><i class="fa fa-plus"></i> Add New</a>
               </h4>
                    <p class="category">Here is a list for this all workers</p>
                   
                </div>
                <div class="content table-responsive">
                    <table class="table table-hover table-striped table-sm">
              <thead>
                  <tr>
                      <th class="text-center">#</th>
                      <td>User Id</td>
                      <td>Hour Rate</td>
                      <td>Working Hour per Day</td>
                      <td>Working Day per Month</td>
                      <td>Availability</td>
                      <td>Rate Type</td>
                      <td>Status</td>
                      <td>Slug</td>
                      <td>Action</td>
                  </tr>
              </thead>
              <tbody>
                @foreach($workers as $key => $worker)
                  <tr>
                      <td class="text-center">{{++$key}}</td>
                      <td>{{ $worker->user->email }}</td>
                      <td>{{ $worker->hour_rate }}</td>
                      <td>{{ $worker->working_hour_per_day}}</td>
                      <td>{{ $worker->working_day_per_month }}</td>
                      <td>{{ $worker->availability}}</td>
                      <td>{{ $worker->rate_type }}</td>
                      <td>{{ $worker->status }}</td>
                      <td>{{ $worker->slug }}</td>
                      <td class="td-actions">
                          <a href="{{route('worker.show', $worker->id)}}" rel="tooltip" title="View Category" class="btn btn-info btn-simple btn-xs">
                            <i class="fa fa-eye"></i>
                          </a>
                        
                            <a href="{{route('worker.edit', $worker->id)}}" rel="tooltip" title="Edit worker" class="btn btn-success btn-simple btn-xs">
                            <i class="fa fa-edit"></i>
                          </a>
             
                          <a data-toggle="modal" data-target="#deleteForm{{$worker->id}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                              <i class="fa fa-times"></i>
                          </a>
                         
                  <!-- Modal -->
                  <div class="modal fade" id="deleteForm{{$worker->id}}" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h3 class="modal-title">Delete</h3>
                        </div>
                        {{Form::open(['route' => ['worker.destroy', $worker->id],'method' => 'DELETE', 'id' => 'delete'] )}}
                        <div class="modal-body">
                          
                          <strong>Are you sure want to Delete this Worker?</strong>
                          
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-small btn-fulltime">Yes</button>
                          <button type="button" class="btn btn-small btn-danger" data-dismiss="modal">No</button>
                          
                        </div>
                        {{Form::close()}}
                      </div>
                    </div>
                  </div>
                      </td>
                  </tr>
                 @endforeach
              </tbody>
          </table>
                </div>
            </div>
        </div>
    </div>
@stop

@extends('layouts.backend')

@section('content')
  <div class="row">
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="card w-100 m-2">
      <div class="card-header">
        <label>Worker index</label>
      </div>
      <div class="card-body">
        <div class="row">
            <a href="{{url('/workers/create')}}" class="btn btn-primary float-right">Create New</a>
                      
        </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>User Id</td>
                    <td>Hour Rate</td>
                    <td>Working Hour per Day</td>
                    <td>Working Day per Month</td>
                    <td>Availability</td>
                    <td>Rate Type</td>
                    <td>Status</td>
                    <td>Slug</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
            @foreach($workers as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->user->email }}</td>
                    <td>{{ $value->hour_rate }}</td>
                    <td>{{ $value->working_hour_per_day}}</td>
                    <td>{{ $value->working_day_per_month }}</td>
                    <td>{{ $value->availability}}</td>
                    <td>{{ $value->rate_type }}</td>
                    <td>{{ $value->status }}</td>
                    <td>{{ $value->slug }}</td>
                    <td>
                        <a class="btn btn-small btn-success" href="{{ URL::to('/workers/' . $value->id) }}">Show</a>
                        <a class="btn btn-small btn-info" href="{{ URL::to('/workers/' . $value->id . '/edit') }}">Edit</a>
                        <form method="POST" action="/workers/{{ $value->id }}" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger btn-small" value="Delete">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $workers->links() }}
      </div>
    </div>

  </div>

@endsection