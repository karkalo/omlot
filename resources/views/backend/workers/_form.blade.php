<div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <label>User <font color="red">*</font></label>
        {{Form::select('user_id', $users , isset($worker) ? $worker->user_id: null, ['class' => 'form-control'])}}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
          <label>Status</label>
          {{ Form::select('status', [''=> 'Select Status', '1' => 'Active', '0' => 'Inactive'],null, ['id' => 'status', 'class' => 'form-control']) }}
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
      <div class="form-group">
         <label>Slug <font color="red">*</font></label>
         {{Form::text('slug', null, ['class' => 'form-control', 'required' => 'false', 'placeholder' => 'Slug..'])}}
      </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
           <label>Hour Rate <font color="red">*</font></label>
           {{Form::text('hour_rate', null, ['class' => 'form-control', 'required' => 'false', 'placeholder' => 'Hour rate..'])}}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
      <div class="form-group">
         <label>Working Hour per Day <font color="red">*</font></label>
         {{Form::text('working_hour_per_day', null, ['class' => 'form-control', 'required' => 'false', 'placeholder' => 'Working Hour per Day..'])}}
      </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
           <label>Working Day per Month <font color="red">*</font></label>
           {{Form::text('working_day_per_month', null, ['class' => 'form-control', 'required' => 'false', 'placeholder' => 'Working Day per Month..'])}}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Rate Type <font color="red">*</font></label>
            {{ Form::select('rate_type', [''=> 'Select Status', '0' => 'Hourly', '1' => 'Daily', '2'=>'Weekly', '3'=> 'Monthly'],null, ['id' => 'status', 'class' => 'form-control']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
      <div class="form-group">
         <label>Availability <font color="red">*</font></label>
         {{Form::text('availability', null, ['class' => 'form-control', 'required' => 'false', 'placeholder' => 'Availability..'])}}
      </div>
    </div>
</div>
