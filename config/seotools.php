<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => "Karkalo Pvt. Ltd. #1 Prefab Works, Advertising and digital company of Nepal.", // set false to total remove
            'description'  => 'Karkalo Pvt. Ltd. offering total solutions in Prefabrication homes, Design & Decore, Advertisements and branding fields. With its expertise in solutions and services, Karkalo offers value-added services in key areas of digital Services.', 
            // set false to total remove
            'separator'    => ' - ',
            'keywords'     => [ 'Karkalo','Prefab home', 'Karkalo Pvt. Ltd Nepal', 'Led video screen', 'Acrylic 3D Letter', 'Trusses works', 'Prefab construction in nepal', 'Job in Nepal', 'Web Developer' ,'CCTV Camera', 'Web  Development in Nepal', 'T-shirt Printing', 'Flex Print'],
            'canonical'    => null, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
           'title'        => "Karkalo Pvt. Ltd. #1 Prefab Works, Advertising and digital company of Nepal.", // set ftotal remove
            'description'  => 'Karkalo Pvt. Ltd. offering total solutions in Prefabrication homes, Design & Decore, Advertisements and branding fields. With its expertise in solutions and services, Karkalo offers value-added services in key areas of digital Services.', 
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => 'website',
            'site_name'   => false,
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          'card'        => 'Karkalo Pvt. Ltd. offering total solutions in Prefabrication homes, Design & Decore, Advertisements and branding fields. With its expertise in solutions and services, Karkalo offers value-added services in key areas of digital Services.', 
          'site'        => '@KarkaloTech',
        ],
    ],
];
