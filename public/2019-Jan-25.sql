-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: omlot
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advertisements`
--

DROP TABLE IF EXISTS `advertisements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `click` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisements`
--

LOCK TABLES `advertisements` WRITE;
/*!40000 ALTER TABLE `advertisements` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertisements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `is_response` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_slider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_featured` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'About','https://karkalo.com/about',NULL,NULL,1,'2019-01-25 05:56:45','2019-01-25 05:56:45'),(2,'Contact','https://karkalo.com/contact',NULL,NULL,1,'2019-01-25 05:56:45','2019-01-25 05:56:45'),(3,'Service','https://karkalo.com/service',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(4,'FAQ','https://karkalo.com/faq',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(5,'Product','https://karkalo.com/product',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(6,'Gallery','https://karkalo.com/gallery',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(7,'Post','https://karkalo.com/post',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(8,'Category','https://karkalo.com/category',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(9,'Career','https://karkalo.com/career',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(10,'Portfolio','https://karkalo.com/portfolio',NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metas`
--

LOCK TABLES `metas` WRITE;
/*!40000 ALTER TABLE `metas` DISABLE KEYS */;
/*!40000 ALTER TABLE `metas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_06_13_060835_create_permissions_table',1),(4,'2017_06_13_060857_create_roles_table',1),(5,'2017_06_13_061359_create_permission_role_table',1),(6,'2017_06_13_061359_create_product_gallery_table',1),(7,'2017_07_31_101302_create_testimonials_table',1),(8,'2017_09_04_130628_create_faqs_table',1),(9,'2017_09_10_115427_create_partners_table',1),(10,'2017_09_23_001946_create_categories_table',1),(11,'2017_09_23_002424_create_pages_table',1),(12,'2017_09_23_003309_create_metas_table',1),(13,'2017_09_23_004611_create_contacts_table',1),(14,'2017_09_23_005728_create_teams_table',1),(15,'2017_09_23_131130_create_products_table',1),(16,'2017_09_23_132058_create_galleries_table',1),(17,'2017_09_23_133244_create_comments_table',1),(18,'2017_09_23_135222_create_settings_table',1),(19,'2017_09_23_135351_create_subscribes_table',1),(20,'2017_11_07_112459_create_seos_table',1),(21,'2017_11_22_044306_create_posts_table',1),(22,'2017_11_25_144241_create_menus_table',1),(23,'2017_12_16_184732_create_advertisements_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'page',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'About','about','about',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,'2019-01-25 05:56:46','2019-01-25 05:56:46');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,NULL,NULL),(1,2,NULL,NULL),(1,3,NULL,NULL),(1,4,NULL,NULL),(1,5,NULL,NULL),(1,6,NULL,NULL),(1,7,NULL,NULL),(1,8,NULL,NULL),(1,9,NULL,NULL),(1,10,NULL,NULL),(1,11,NULL,NULL),(1,12,NULL,NULL),(1,13,NULL,NULL),(1,14,NULL,NULL),(1,15,NULL,NULL),(1,16,NULL,NULL),(1,17,NULL,NULL),(1,18,NULL,NULL),(1,19,NULL,NULL),(1,20,NULL,NULL),(1,21,NULL,NULL),(1,22,NULL,NULL),(1,23,NULL,NULL),(1,24,NULL,NULL),(1,25,NULL,NULL),(1,26,NULL,NULL),(1,27,NULL,NULL),(1,28,NULL,NULL),(1,29,NULL,NULL),(1,30,NULL,NULL),(1,31,NULL,NULL),(1,32,NULL,NULL),(1,33,NULL,NULL),(1,34,NULL,NULL),(1,35,NULL,NULL),(1,36,NULL,NULL),(1,37,NULL,NULL),(1,38,NULL,NULL),(1,39,NULL,NULL),(1,40,NULL,NULL),(1,41,NULL,NULL),(1,42,NULL,NULL),(1,43,NULL,NULL),(1,44,NULL,NULL),(1,45,NULL,NULL),(1,46,NULL,NULL),(1,47,NULL,NULL),(1,48,NULL,NULL),(1,49,NULL,NULL),(1,50,NULL,NULL),(1,51,NULL,NULL),(1,52,NULL,NULL),(1,53,NULL,NULL),(1,54,NULL,NULL),(1,55,NULL,NULL),(1,56,NULL,NULL),(1,57,NULL,NULL),(1,58,NULL,NULL),(1,59,NULL,NULL),(1,60,NULL,NULL),(1,61,NULL,NULL),(1,62,NULL,NULL),(1,63,NULL,NULL),(1,64,NULL,NULL),(1,65,NULL,NULL),(1,66,NULL,NULL),(1,67,NULL,NULL),(1,68,NULL,NULL),(1,69,NULL,NULL),(1,70,NULL,NULL),(1,71,NULL,NULL),(1,72,NULL,NULL),(1,73,NULL,NULL),(1,74,NULL,NULL),(1,75,NULL,NULL),(1,76,NULL,NULL),(1,77,NULL,NULL),(1,78,NULL,NULL),(1,79,NULL,NULL),(1,80,NULL,NULL),(1,81,NULL,NULL),(1,82,NULL,NULL),(1,83,NULL,NULL),(1,84,NULL,NULL),(1,85,NULL,NULL),(1,86,NULL,NULL),(1,87,NULL,NULL),(1,88,NULL,NULL),(1,89,NULL,NULL),(1,90,NULL,NULL),(1,91,NULL,NULL),(1,92,NULL,NULL),(1,93,NULL,NULL),(1,94,NULL,NULL),(1,95,NULL,NULL),(1,96,NULL,NULL),(1,97,NULL,NULL),(1,98,NULL,NULL),(1,99,NULL,NULL),(1,100,NULL,NULL),(1,101,NULL,NULL),(1,102,NULL,NULL),(1,103,NULL,NULL),(1,104,NULL,NULL),(1,105,NULL,NULL);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `object` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'backend','dashboard','backend.dashboard','2019-01-25 05:56:39','2019-01-25 05:56:39'),(2,'category','index','category.index','2019-01-25 05:56:39','2019-01-25 05:56:39'),(3,'category','create','category.create','2019-01-25 05:56:39','2019-01-25 05:56:39'),(4,'category','store','category.store','2019-01-25 05:56:39','2019-01-25 05:56:39'),(5,'category','show','category.show','2019-01-25 05:56:39','2019-01-25 05:56:39'),(6,'category','edit','category.edit','2019-01-25 05:56:39','2019-01-25 05:56:39'),(7,'category','update','category.update','2019-01-25 05:56:39','2019-01-25 05:56:39'),(8,'category','destroy','category.destroy','2019-01-25 05:56:39','2019-01-25 05:56:39'),(9,'page','index','page.index','2019-01-25 05:56:39','2019-01-25 05:56:39'),(10,'page','create','page.create','2019-01-25 05:56:39','2019-01-25 05:56:39'),(11,'page','store','page.store','2019-01-25 05:56:39','2019-01-25 05:56:39'),(12,'page','show','page.show','2019-01-25 05:56:39','2019-01-25 05:56:39'),(13,'page','edit','page.edit','2019-01-25 05:56:39','2019-01-25 05:56:39'),(14,'page','update','page.update','2019-01-25 05:56:39','2019-01-25 05:56:39'),(15,'page','destroy','page.destroy','2019-01-25 05:56:39','2019-01-25 05:56:39'),(16,'post','index','post.index','2019-01-25 05:56:39','2019-01-25 05:56:39'),(17,'post','create','post.create','2019-01-25 05:56:39','2019-01-25 05:56:39'),(18,'post','store','post.store','2019-01-25 05:56:39','2019-01-25 05:56:39'),(19,'post','show','post.show','2019-01-25 05:56:39','2019-01-25 05:56:39'),(20,'post','edit','post.edit','2019-01-25 05:56:39','2019-01-25 05:56:39'),(21,'post','update','post.update','2019-01-25 05:56:39','2019-01-25 05:56:39'),(22,'post','destroy','post.destroy','2019-01-25 05:56:39','2019-01-25 05:56:39'),(23,'product','index','product.index','2019-01-25 05:56:39','2019-01-25 05:56:39'),(24,'product','create','product.create','2019-01-25 05:56:39','2019-01-25 05:56:39'),(25,'product','store','product.store','2019-01-25 05:56:39','2019-01-25 05:56:39'),(26,'product','show','product.show','2019-01-25 05:56:40','2019-01-25 05:56:40'),(27,'product','edit','product.edit','2019-01-25 05:56:40','2019-01-25 05:56:40'),(28,'product','update','product.update','2019-01-25 05:56:40','2019-01-25 05:56:40'),(29,'product','destroy','product.destroy','2019-01-25 05:56:40','2019-01-25 05:56:40'),(30,'metas','index','metas.index','2019-01-25 05:56:40','2019-01-25 05:56:40'),(31,'metas','create','metas.create','2019-01-25 05:56:40','2019-01-25 05:56:40'),(32,'metas','store','metas.store','2019-01-25 05:56:40','2019-01-25 05:56:40'),(33,'metas','show','metas.show','2019-01-25 05:56:40','2019-01-25 05:56:40'),(34,'metas','edit','metas.edit','2019-01-25 05:56:40','2019-01-25 05:56:40'),(35,'metas','update','metas.update','2019-01-25 05:56:40','2019-01-25 05:56:40'),(36,'metas','destroy','metas.destroy','2019-01-25 05:56:40','2019-01-25 05:56:40'),(37,'teams','index','teams.index','2019-01-25 05:56:40','2019-01-25 05:56:40'),(38,'teams','create','teams.create','2019-01-25 05:56:40','2019-01-25 05:56:40'),(39,'teams','store','teams.store','2019-01-25 05:56:40','2019-01-25 05:56:40'),(40,'teams','show','teams.show','2019-01-25 05:56:40','2019-01-25 05:56:40'),(41,'teams','edit','teams.edit','2019-01-25 05:56:40','2019-01-25 05:56:40'),(42,'teams','update','teams.update','2019-01-25 05:56:40','2019-01-25 05:56:40'),(43,'teams','destroy','teams.destroy','2019-01-25 05:56:40','2019-01-25 05:56:40'),(44,'gallery','index','gallery.index','2019-01-25 05:56:40','2019-01-25 05:56:40'),(45,'gallery','create','gallery.create','2019-01-25 05:56:40','2019-01-25 05:56:40'),(46,'gallery','store','gallery.store','2019-01-25 05:56:40','2019-01-25 05:56:40'),(47,'gallery','show','gallery.show','2019-01-25 05:56:41','2019-01-25 05:56:41'),(48,'gallery','edit','gallery.edit','2019-01-25 05:56:41','2019-01-25 05:56:41'),(49,'gallery','update','gallery.update','2019-01-25 05:56:41','2019-01-25 05:56:41'),(50,'gallery','destroy','gallery.destroy','2019-01-25 05:56:41','2019-01-25 05:56:41'),(51,'menu','index','menu.index','2019-01-25 05:56:41','2019-01-25 05:56:41'),(52,'menu','create','menu.create','2019-01-25 05:56:41','2019-01-25 05:56:41'),(53,'menu','store','menu.store','2019-01-25 05:56:41','2019-01-25 05:56:41'),(54,'menu','show','menu.show','2019-01-25 05:56:41','2019-01-25 05:56:41'),(55,'menu','edit','menu.edit','2019-01-25 05:56:41','2019-01-25 05:56:41'),(56,'menu','update','menu.update','2019-01-25 05:56:41','2019-01-25 05:56:41'),(57,'menu','destroy','menu.destroy','2019-01-25 05:56:41','2019-01-25 05:56:41'),(58,'user','index','user.index','2019-01-25 05:56:41','2019-01-25 05:56:41'),(59,'user','create','user.create','2019-01-25 05:56:41','2019-01-25 05:56:41'),(60,'user','store','user.store','2019-01-25 05:56:41','2019-01-25 05:56:41'),(61,'user','show','user.show','2019-01-25 05:56:41','2019-01-25 05:56:41'),(62,'user','edit','user.edit','2019-01-25 05:56:41','2019-01-25 05:56:41'),(63,'user','update','user.update','2019-01-25 05:56:41','2019-01-25 05:56:41'),(64,'user','destroy','user.destroy','2019-01-25 05:56:41','2019-01-25 05:56:41'),(65,'role','index','role.index','2019-01-25 05:56:41','2019-01-25 05:56:41'),(66,'role','create','role.create','2019-01-25 05:56:41','2019-01-25 05:56:41'),(67,'role','store','role.store','2019-01-25 05:56:41','2019-01-25 05:56:41'),(68,'role','show','role.show','2019-01-25 05:56:41','2019-01-25 05:56:41'),(69,'role','edit','role.edit','2019-01-25 05:56:42','2019-01-25 05:56:42'),(70,'role','update','role.update','2019-01-25 05:56:42','2019-01-25 05:56:42'),(71,'role','destroy','role.destroy','2019-01-25 05:56:42','2019-01-25 05:56:42'),(72,'faq','index','faq.index','2019-01-25 05:56:42','2019-01-25 05:56:42'),(73,'faq','create','faq.create','2019-01-25 05:56:42','2019-01-25 05:56:42'),(74,'faq','store','faq.store','2019-01-25 05:56:42','2019-01-25 05:56:42'),(75,'faq','show','faq.show','2019-01-25 05:56:42','2019-01-25 05:56:42'),(76,'faq','edit','faq.edit','2019-01-25 05:56:42','2019-01-25 05:56:42'),(77,'faq','update','faq.update','2019-01-25 05:56:42','2019-01-25 05:56:42'),(78,'faq','destroy','faq.destroy','2019-01-25 05:56:42','2019-01-25 05:56:42'),(79,'partner','index','partner.index','2019-01-25 05:56:42','2019-01-25 05:56:42'),(80,'partner','create','partner.create','2019-01-25 05:56:42','2019-01-25 05:56:42'),(81,'partner','store','partner.store','2019-01-25 05:56:42','2019-01-25 05:56:42'),(82,'partner','show','partner.show','2019-01-25 05:56:42','2019-01-25 05:56:42'),(83,'partner','edit','partner.edit','2019-01-25 05:56:42','2019-01-25 05:56:42'),(84,'partner','update','partner.update','2019-01-25 05:56:42','2019-01-25 05:56:42'),(85,'partner','destroy','partner.destroy','2019-01-25 05:56:42','2019-01-25 05:56:42'),(86,'testimonial','index','testimonial.index','2019-01-25 05:56:42','2019-01-25 05:56:42'),(87,'testimonial','create','testimonial.create','2019-01-25 05:56:42','2019-01-25 05:56:42'),(88,'testimonial','store','testimonial.store','2019-01-25 05:56:42','2019-01-25 05:56:42'),(89,'testimonial','show','testimonial.show','2019-01-25 05:56:42','2019-01-25 05:56:42'),(90,'testimonial','edit','testimonial.edit','2019-01-25 05:56:42','2019-01-25 05:56:42'),(91,'testimonial','update','testimonial.update','2019-01-25 05:56:43','2019-01-25 05:56:43'),(92,'testimonial','destroy','testimonial.destroy','2019-01-25 05:56:43','2019-01-25 05:56:43'),(93,'advertisement','index','advertisement.index','2019-01-25 05:56:43','2019-01-25 05:56:43'),(94,'advertisement','create','advertisement.create','2019-01-25 05:56:43','2019-01-25 05:56:43'),(95,'advertisement','store','advertisement.store','2019-01-25 05:56:43','2019-01-25 05:56:43'),(96,'advertisement','show','advertisement.show','2019-01-25 05:56:43','2019-01-25 05:56:43'),(97,'advertisement','edit','advertisement.edit','2019-01-25 05:56:43','2019-01-25 05:56:43'),(98,'advertisement','update','advertisement.update','2019-01-25 05:56:43','2019-01-25 05:56:43'),(99,'advertisement','destroy','advertisement.destroy','2019-01-25 05:56:43','2019-01-25 05:56:43'),(100,'contact','index','contact.index','2019-01-25 05:56:43','2019-01-25 05:56:43'),(101,'contact','show','contact.show','2019-01-25 05:56:43','2019-01-25 05:56:43'),(102,'comment','index','comment.index','2019-01-25 05:56:43','2019-01-25 05:56:43'),(103,'comment','show','comment.show','2019-01-25 05:56:43','2019-01-25 05:56:43'),(104,'setting','index','setting.index','2019-01-25 05:56:43','2019-01-25 05:56:43'),(105,'setting','store','setting.store','2019-01-25 05:56:43','2019-01-25 05:56:43');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_gallery`
--

DROP TABLE IF EXISTS `product_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_gallery` (
  `product_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_gallery`
--

LOCK TABLES `product_gallery` WRITE;
/*!40000 ALTER TABLE `product_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `deal_price` double(8,2) DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `view` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Superadmin','2019-01-25 05:56:43','2019-01-25 05:56:43');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seos`
--

DROP TABLE IF EXISTS `seos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `visit` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seos`
--

LOCK TABLES `seos` WRITE;
/*!40000 ALTER TABLE `seos` DISABLE KEYS */;
INSERT INTO `seos` VALUES (1,'About','about','about','https://karkalo.com/about',NULL,NULL,1,1,'2019-01-25 05:56:45','2019-01-25 06:42:16'),(2,'Contact','contact','contact','https://karkalo.com/contact',NULL,NULL,1,1,'2019-01-25 05:56:45','2019-01-25 06:42:06'),(3,'Service','service','service','https://karkalo.com/service',NULL,NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(4,'FAQ','faq','faq','https://karkalo.com/faq',NULL,NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(5,'Product','product','product','https://karkalo.com/product',NULL,NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(6,'Gallery','gallery','gallery','https://karkalo.com/gallery',NULL,NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(7,'Post','post','post','https://karkalo.com/post',NULL,NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(8,'Category','category','category','https://karkalo.com/category',NULL,NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(9,'Career','career','career','https://karkalo.com/career',NULL,NULL,NULL,1,'2019-01-25 05:56:46','2019-01-25 05:56:46'),(10,'Portfolio','portfolio','portfolio','https://karkalo.com/portfolio',NULL,NULL,1,1,'2019-01-25 05:56:46','2019-01-25 06:42:09');
/*!40000 ALTER TABLE `seos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'company_name','Tim Xilwal','Company Name','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(2,'email','dev@xilwal.com','Email Address','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(3,'phone','9811960012','Phone Number','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(4,'mobile','9847502170','Mobile Number','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(5,'address','Kathmand, Nepal','Address','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(6,'facebook','https://facebook.com/bsilwal2','Facebook','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(7,'twitter','https://twitter.com/xilwal','Twitter','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(8,'google','https://plus.google.com/user/xilwal','Google Plus','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(9,'linkedin','https://linkedin.com/xilwal','Linkedin','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(10,'about','We are a web developer.','About','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(11,'working_hour','Mon- Fri: 10:00 AM - 6:00 PM','Working Hour','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(12,'longitude','27.9901273','Map Longitude','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(13,'latitude','87.3789269','Map Laltitude','text','2019-01-25 05:56:45','2019-01-25 05:56:45'),(14,'copy_right','copyright @2016, <a href=\"http://xilwal.com\">Xilwal</a>. All Rights Reserved.','Copy Right','text','2019-01-25 05:56:45','2019-01-25 05:56:45');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribes`
--

DROP TABLE IF EXISTS `subscribes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribes`
--

LOCK TABLES `subscribes` WRITE;
/*!40000 ALTER TABLE `subscribes` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial` text COLLATE utf8mb4_unicode_ci,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Tim Xilwal','dev.xilwal@gmail.com','$2y$10$qBJMJxe.Qzb4RnSASCRta.UNOwNTbLon.9yqg/WdNXh.uYEngwEBq',1,1,'K0TehxjL3d1PIYrLZ3r67N86mbk7iwpH23WjadWwHTExbKuO26iYkzipWlIS','2019-01-25 05:56:44','2019-01-25 05:56:44');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-25 19:52:59
