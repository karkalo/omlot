-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: xilwalvikash90917.ipagemysql.com    Database: karkalo
-- ------------------------------------------------------
-- Server version	5.6.37-82.2-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advertisements`
--

DROP TABLE IF EXISTS `advertisements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `click` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisements`
--

LOCK TABLES `advertisements` WRITE;
/*!40000 ALTER TABLE `advertisements` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertisements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Advertising & Marketing','advertising-marketing','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Karkalo Pvt. Ltd. Limited is one of the leading companies among Creates new promotional ideas, designs, print, radio, television, and internet advertisements, book advertisement space and time, provide other such services that help a client in entering and succeeding in a chosen market. The company utilizes the latest technology blended with a mechanism, software, and electronics to design an exciting and excellent range of electronic display systems and electronic display boards for various commercial requirements. Our range of LED signs and display boards including a token display system, status display board, 3D acrylic led letters and metal letters are high in performance and quality and serve an excellent media for indoor and outdoor advertising. Attractive in looks, these offer long visibility, easily communicate with customers and help businesses such as shops, offices and other commercial entities to attract customers and generate more business. Available at competitive prices, our display boards can also be effectively used for applications such as road signals, security equipment, and information display systems as in foggy and smoked conditions; these have good visibility from long distances.</p>\r\n</body>\r\n</html>','categories/advertising-marketing.jpg','Digital Advertisement','Karkalo Pvt. Ltd. Limited is one of the leading companies among Creates new promotional ideas, designs, print, radio, television, and internet advertisements, book advertisement space and time, provide other such services that help a client',1,1,1,'2018-06-12 11:25:21','2018-08-08 02:58:17'),(2,'Information Technology','information-technology','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #555555; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">We offer Web and Technology services in Nepal.Web Technology in Nepal. Web Development in Nepal, E-commerce sites in Nepal, website in nepal.</span></p>\r\n</body>\r\n</html>','categories/information-technology.jpg','Information Technology Web Technology in Nepal. Web Development in Nepal, E-commerce sites in Nepal, website in nepal.','We offer Web and Technology services in Nepal. Web Technology in Nepal. Web Development in Nepal, E-commerce sites in Nepal, website in nepal.',1,1,1,'2018-06-22 09:59:06','2018-08-08 02:56:50'),(3,'Designing and Printing','designing-and-printing','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">Karkalo Tech provides&nbsp;all kinds of &nbsp; Graphic design and printing services for every business needs.&nbsp;</span><span style=\"color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">&nbsp;We also offer</span><span style=\"box-sizing: border-box; font-weight: bold; color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">&nbsp;business cards, letterhead, Company Profiles, Logo design, T-shirt print,&nbsp; Cup Print, Magazine, Books, Prospectus, Brochures and Flyers, Bulletins, Restaurant Menu, Invitation Cards, Visiting Cards, Certificates, Wedding Cards, PVC ID Cards, envelopes&nbsp; &amp; Pree-Ink Stamp and self-ink Stamps in Nepal.</span><span style=\"color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">&nbsp;We maintain relationships with companies that produce continuous forms, foil stamping, and embossing, die to cut and many other services.</span></p>\r\n</body>\r\n</html>','categories/designing-and-printing.jpg','printing','Karkalo Tech provides all kinds of   Graphic design and printing services for every business needs.  We also offer business cards, letterhead, Company Profiles, Logo design, T-shirt print,  Cup Print in Nepal.',1,1,1,'2018-06-22 09:59:20','2018-08-08 02:46:04');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,NULL,NULL,'karkalo','thapaparbat9@gmail.com','9818384837','dai malai yo tshirt','27.34.16.111','comment/2018-08-08 09:24.jpg',NULL,1,NULL,'2018-08-09 01:24:04','2018-08-09 16:15:14',14),(2,NULL,NULL,'Bikash Silwal','xilwalvikash@gmail.com','22345678','sdfghjkl','27.34.109.202',NULL,NULL,NULL,NULL,'2018-08-09 15:59:12','2018-08-09 15:59:12',7),(3,NULL,NULL,'sdfhj','info@karkalo.com','1234567','sdfghjk','27.34.109.202',NULL,NULL,1,NULL,'2018-08-09 16:21:51','2018-08-09 16:22:03',6);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `is_response` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'dfgwerthj','asdc@g.c','6565543424354','asdfghjk','wertyuil;lkjhgc','27.34.109.236',NULL,1,0,'2018-08-12 15:48:32','2018-08-13 01:21:26');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_slider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_featured` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Test-10','test-10','gallery/test.png','http://localhost:8000','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>',NULL,NULL,'0','1','0','2018-06-12 11:26:00','2018-06-12 11:26:01'),(2,'35151356_1873951802626562_3791355279300362240_n (1).png','35151356-1873951802626562-3791355279300362240-n-1png','gallery/35151356-1873951802626562-3791355279300362240-n-1png.png',NULL,NULL,NULL,NULL,'0','0','0','2018-06-12 12:49:50','2018-06-12 12:49:50'),(3,'acrylic tools','acrylic-tools','gallery/acrylic-tools.jpg','https://www.karkalo.com','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>',NULL,NULL,'0','0','0','2018-06-12 12:50:38','2018-08-09 15:28:47'),(4,'Karkalo start seo','karkalo-start-seo','gallery/karkalo-start-seo.jpg','https://www.karkalo.com/search-engine-optimizationseo','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>','Karkalo start seo','Karkalo start seo','1','1','0','2018-06-12 12:52:56','2018-08-09 02:37:36'),(5,'Connect your business to digital','connect-your-business-to-digital','gallery/connect-your-business-to-digital.jpg','https://www.karkalo.com/information-technology','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>','Web pages','Web pages','1','1','1','2018-06-12 12:53:45','2018-08-14 01:13:35'),(6,'karkalo-google.jpg','karkalo-googlejpg','gallery/karkalo-googlejpg.jpg',NULL,NULL,NULL,NULL,'0','1','0','2018-06-12 12:54:14','2018-06-12 12:54:14'),(7,'Printing and Designing','printing-and-designing','gallery/printing-and-designing.png','https://www.karkalo.com/designing-and-printing','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>','Printing and Designing','Printing and Designing','1','1','1','2018-06-18 16:03:56','2018-08-14 00:30:40'),(8,'Acrylic 3D Letter','acrylic-3d-letter','gallery/acrylic-3d-letter.jpg','https://www.karkalo.com/acrylic-letter-board','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>','Acrylic 3D Letter','Acrylic 3D Letter','0','1','1','2018-06-18 16:05:47','2018-08-14 01:16:04'),(9,'Led video screen board in nepal','led-video-screen-board-in-nepal','gallery/led-video-screen-board-in-nepal.jpg','https://www.karkalo.com/led-video-screen','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>','Led scrolling board in nepal, Led screen board','Led scrolling board in nepal, Led screen board','0','0','0','2018-06-18 16:06:12','2018-08-14 01:15:10');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Header','http://localhost:8000',NULL,NULL,1,'2018-06-25 10:14:55','2018-06-25 11:51:53'),(2,'Home','http://localhost:8000',2,1,1,'2018-06-25 10:15:15','2018-06-25 10:22:48'),(3,'About Us','http://localhost:8000/about',2,1,1,'2018-06-25 10:22:25','2018-06-25 10:23:08'),(4,'Services','https://localhost:8000/service',3,1,1,'2018-06-25 10:23:08','2018-06-25 10:31:35'),(5,'Web and Technology','http://localhost:8000/web-development',2,4,1,'2018-06-25 10:25:55','2018-06-25 10:29:36'),(6,'Graphic Design and Printing','http://localhost:8000/graphic-design-and-printing',2,4,1,'2018-06-25 10:26:09','2018-06-25 10:30:59'),(7,'Digital Advertisement','http://localhost:8000/digital-advertisement',1,4,1,'2018-06-25 10:30:59','2018-06-25 10:30:59'),(8,'Portfolio','http://localhost:8000/portfolio',4,1,1,'2018-06-25 10:31:35','2018-06-25 11:50:35'),(9,'Contact Us','https://localhost:8000/contact',1,1,1,'2018-06-25 11:51:01','2018-06-25 11:51:01'),(10,'Footer','http://localhost:8000',1,NULL,1,'2018-06-25 11:51:53','2018-06-25 11:51:53'),(11,'Quick Link','http://localhost:8000',1,10,1,'2018-06-25 11:52:21','2018-06-25 11:52:21'),(12,'Home','http://localhost:8000',2,11,1,'2018-06-25 11:52:40','2018-06-25 11:53:30'),(13,'About','http://localhost:8000/about',3,11,1,'2018-06-25 11:52:59','2018-06-25 11:54:23'),(14,'Portfolio','http://localhost:8000/portfolio',4,11,1,'2018-06-25 11:53:30','2018-06-25 11:54:47'),(15,'Privacy Policy','http://localhost:8000/privacy-policy',1,11,1,'2018-06-25 11:54:23','2018-06-25 11:54:23'),(16,'Terms & Conditions','http://localhost:8000/terms-and-conditions',1,11,1,'2018-06-25 11:54:47','2018-06-25 11:54:47');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metas`
--

LOCK TABLES `metas` WRITE;
/*!40000 ALTER TABLE `metas` DISABLE KEYS */;
/*!40000 ALTER TABLE `metas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_06_13_060835_create_permissions_table',1),(4,'2017_06_13_060857_create_roles_table',1),(5,'2017_06_13_061359_create_permission_role_table',1),(6,'2017_07_31_101302_create_testimonials_table',1),(7,'2017_09_04_130628_create_faqs_table',1),(8,'2017_09_10_115427_create_partners_table',1),(9,'2017_09_23_001946_create_categories_table',1),(10,'2017_09_23_002424_create_pages_table',1),(11,'2017_09_23_003309_create_metas_table',1),(12,'2017_09_23_004611_create_contacts_table',1),(13,'2017_09_23_005728_create_teams_table',1),(14,'2017_09_23_131130_create_products_table',1),(15,'2017_09_23_132058_create_galleries_table',1),(16,'2017_09_23_133244_create_comments_table',1),(17,'2017_09_23_135222_create_settings_table',1),(18,'2017_09_23_135351_create_subscribes_table',1),(19,'2017_11_07_112459_create_seos_table',1),(20,'2017_11_22_044306_create_posts_table',1),(21,'2017_11_25_144241_create_menus_table',1),(22,'2017_12_16_184732_create_advertisements_table',1),(23,'2017_06_13_061359_create_product_gallery_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'page',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'About','about','about','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; font-family: MyriadPro; font-size: 16px; padding-top: 15px; padding-bottom: 20px; color: #333333;\">Karkalo Tech is the professional organization providing high-end solutions and services in the domains of business. Karkalo Tech&nbsp;is founded and led by a group of young professionals, with a long-time vision and a very high level of commitment to the client&rsquo;s satisfaction. Karkalo Tech&nbsp;aims to be a world leader in the field of technology systems integration and related business. We are committed to excellence in quality, cost effectiveness, and customer satisfaction.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; font-family: MyriadPro; font-size: 16px; padding-top: 15px; padding-bottom: 20px; color: #333333;\">We unquenchable partial for quality has led us to get prestigious clients and keeping our growth at a very high rate. We offering total solutions in Web and Information Technology, Graphics Design &amp; Printing Services, and Digital Advertisements fields. With its expertise in solutions and services, Karkalo offers value-added services in key areas of Systems and Services.</p>\r\n</body>\r\n</html>','pages/about.png',NULL,'web and technology in Nepal, graphics designing in Nepal, all kinds of printing services, digital advertisements such as led display board, led video screen, 3d letter, acrylic raising baord','We provide web and technology in Nepal, graphics designing in Nepal, all kinds of printing services, digital advertisements such as led display board, led video screen, 3d letter, acrylic raising board in Kathmandu, Nepal.',NULL,1,1,1,'2018-06-11 16:21:23','2018-08-09 14:08:49'),(2,'Our Vision','our-vision','about','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #333333; font-family: MyriadPro; font-size: 16px; text-align: justify;\">&nbsp;At Karkalo, we aim to provide innovative product solutions and know that our customers place their trust in us. We in turn make a commitment to uphold that trust and cater to all the creative needs of our clients. Our values of innovation and integrity are underpinned by our belief that customers always come first. As a company, we create an ambience that is impressive and inspiring to all and take pride in our expertise, capability, reliability and quality of work. The objective of management is to provide these services in a manner which conforms to contractual and regulatory requirements. This unquenchable thirst for quality has led us to getting prestigious clients and keeping our growth at a very high rate.&nbsp;</span></p>\r\n</body>\r\n</html>','pages/our-vision.png',NULL,NULL,NULL,NULL,1,0,0,'2018-06-22 13:53:34','2018-08-09 14:30:52'),(3,'Our Mission','our-mission','about','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #333333; font-family: MyriadPro; font-size: 16px; text-align: justify;\">Karkalo provides you with a deep partnership and commitment you need to realize your business and technology goals. It starts with making your goals our goals and ends with an inventive and flexible solution for your most pressing challenges. At Karkalo every project is important, and this attitude is adhered to at all levels in the company. As a client, you will appreciate the dedication of effort that goes into making your dream project a reality.</span></p>\r\n</body>\r\n</html>','pages/our-mission.png',NULL,NULL,'Karkalo provides you with a deep partnership and commitment you need to realize your business and technology goals.',NULL,1,0,0,'2018-06-22 13:58:08','2018-08-09 14:11:10'),(4,'Privacy Policy','privacy-policy','page','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Privacy Policy</p>\r\n</body>\r\n</html>','pages/privacy-policy.jpeg',NULL,'Privacy Policy','Privacy Policy',NULL,1,1,1,'2018-06-25 11:53:48','2018-08-09 14:11:10'),(5,'Terms and conditions','terms-and-conditions','page','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Terms and conditions</p>\r\n</body>\r\n</html>','pages/terms-and-conditions.jpeg',NULL,'Terms and conditions','Terms and conditions',NULL,1,1,1,'2018-06-25 11:53:58','2018-08-09 14:11:10'),(6,'Web Development','web-development','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Karkalo Tech developes responsive standard websites enhancing your digital experience. We are dedicated to create powerful, effective and engaging websites able to serve your best purpose. We use ultra-clean and standard-based markup code to produce websites with prodigious search engine results.</p>\r\n<p>We are competent with extensive experience on E-commerce, Job-protal, News-portal, Enterprise management system and many more. Professional website creation is our job while giving you the stability on online platform is our responsibility. Based on discovery sessions and regular discussions with your team,&nbsp;our expertise advocate best technology for your website to satisfy your needs ensuring your web presense as well as guide you through the process of&nbsp; enhancing the administrative interface. For front-end technology we use HTML, JavaScript and CSS along with JavaScript Frameworks such as VueJS, ReactJS, NodeJS, and AngularJS to&nbsp;simplify and provide more agility and for back-end technology we use PHP, Python, Ruby, and Java.</p>\r\n</body>\r\n</html>','pages/web-development.png',NULL,'Web Development in Nepal,create website,website services in Kathmandu, job-portal site,e-commerce site, enterprise management  system,news  portal website, Web development company in Nepal','We are competent with extensive experience on E-commerce, Job-portal, News-portal, Enterprise management system and many more with standards- based markup code for the creation of powerful, effective and engaging websites.',2,1,1,1,'2018-06-29 07:12:43','2018-08-13 21:39:12'),(7,'Web Designing','web-designing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Are you searching for best web design and development in Nepal?&nbsp;</p>\r\n<p>The main objective of web designing is transforming guests into customers by developing feelings of delight, trust, and confidence while they navigate through your website. Effective website design is concerned about the visualization of an organization and core of&nbsp; web based advertising activities. We put balanced&nbsp;approach to content, design and development to your website which are completely Search Engine Optimized. So why not give us a chance to make your insight consolidate your goals?</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>','pages/web-designing.png',NULL,'Web Designing  company in Nepal, website creation, improve website  graphics,','We focus on making our work simple yet ingenious which is aesthetically appealing, highly responsive and functional with cohesive web designs to match your unique personality.',2,1,1,1,'2018-06-29 07:13:41','2018-08-13 21:45:41'),(8,'Mobile Application','mobile-application','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>The new upgraded approach for web development in Nepal is Mobile Application yet it needs more quality correspondence towards the customer. Disclose your application concepts to us and we shall create the best versatile application possible.</p>\r\n</body>\r\n</html>','pages/mobile-application.png',NULL,'Mobile Application development in Nepal,mobile application company in Kathmandu, develop mobile application,','Reach your customers through mobile phones. Karkalo Tech will provide you with felicitous Mobile Application to meet your application requirements.',2,1,1,1,'2018-06-29 07:14:40','2018-08-13 21:50:06'),(9,'Web Hosting','web-hosting','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>We are rendering web hosting services in Nepal with general support of user friendly control panel with&nbsp;the assistance of most recent web innovations. Karkalo gives dependable, secured and best hosting services&nbsp;in Nepal at least cost. Our web hosting administrations in Nepal guarantees your sites are working fast with all of your information secured with standard regular backups. We offer Linux and also windows web&nbsp;hosting services in Nepal. Get in touch with us for best web hosting services in Nepal .&nbsp;</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>','pages/web-hosting.png',NULL,'Web Hosting in Nepal,domain register in Nepal,how to host website,','Karkalo gives dependable, secured and best hosting services in Nepal at affordable price. Contact us for  superlative Web Hosting services.',2,1,1,1,'2018-06-29 07:16:14','2018-08-13 21:55:19'),(10,'Domain Register','domain-register','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>A domain name is the embodiment of your organization\'s online personality. It features the company\'s type and the mission. We offer .com, .net, .organization domain registration, where you can register&nbsp;your domain with us. Before choosing domain, picking proper accessible domain name that suits your business is a standout amongst the most vital things to consider. We provide guidelines to enroll domain name with .np extension as a cost-free exercise.</p>\r\n</body>\r\n</html>','pages/domain-register.jpg',NULL,'Domain Register in nepal, how to register domain,','Karkalo Tech provide domain services with traditional domains such as .com .edu .org .net .mil .gov  .int and new domain names along with geographical, second level domain and sub domains.',2,1,1,1,'2018-06-29 07:19:51','2018-08-13 21:58:03'),(11,'Search Engine Optimization(SEO)','search-engine-optimizationseo','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Search engine optimization is a set of rules and good practices to get traffic and online visibility of a website. SEO can help your optimize your website for better search engines ranks.The majority of search engines users&nbsp; will probably tap on one of the best proposals in the first page, so to&nbsp;exploit this and pick up guests to your site or clients to your online store you have to be in the best position. Good SEO hones client experience and ease&nbsp;of use of a site. Search engines are the foundation of user trust and having your online presence on the top while user is looking for your product and services, it increases the website&rsquo;s trust.Given two websites are selling the same product, the website whose search engine is optimized is more likely to connect to&nbsp;more customers and make more deals online.</p>\r\n<p>We at Karkalo Tech understand the mechanism of search, we ensure organic, natural and earned web results for your website\'s high page ranking. You could be the biggest shop in the town, but if you don\'t show up in local searches online, you\'re missing out on a significant amount of clients and sales. We provide Global and National SEO, Ecommerce SEO, Enterprise SEO, Local SEO, Content Marketing, Off-site SEO, On-site SEO and Google Recovery&nbsp;Services. Remember us in order to take advantages of felicitous&nbsp;SEO services in Kathmandu as well as all over Nepal.</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>','pages/search-engine-optimizationseo.jpg',NULL,'Seo services  in Kathmandu, search engine optimization services in Nepal, increase my website page rank, improve SEO,  increase google page rank','We are fully fledged to improve and increase  your website page ranking with felicitous Search Engine Optimization(SEO) as per your needs and targeted areas.',2,1,1,1,'2018-06-29 07:20:46','2018-08-13 22:09:16'),(12,'Business Card Printing','business-card-printing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h3 style=\"padding: 0px; margin: 14px 0px 0px; box-sizing: border-box; font-size: 18px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: 300; line-height: 1.1;\">Give a Brand Recognition to Your Business</h3>\r\n<p style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-family: Roboto, sans-serif; color: #666666; line-height: 19px; text-align: justify;\">Business cards are the ignition toggle that initiates the conversation between your projections and your brand. This most basic yet effective tool for business use works ideally for exchanging contact information as well. If you are in search of business card printing, Karkalo Tech is here to aid you in&nbsp; all perspectives. We offer many services for businesses and individuals. Our online content is specifically designed for our potential customers, so that ordering is quite fast, easy and affordable.</p>\r\n</body>\r\n</html>','pages/business-card-printing.jpg',NULL,'Business Card Design,Business Card Printing','Business cards printing services on high-quality paper at karkalo.com. Now Print 500 Business cards in 500 Rs only. Contact us for Business cards printing.',3,1,1,1,'2018-06-29 07:22:50','2018-08-13 22:18:04'),(13,'Logo Design','logo-design','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Logo is the branding of your company.&nbsp; We understand the aesthetics and focus on the message and impact of your logo to earmark your market requirements. We offer tailored, impressive, relevant, highly complex to simple, minimilist logo designs.</p>\r\n<p>After much research and conceptualizing, our branding group will make distinctive plan headings and ideas and present them to you in excellent marking introductions so you can perceive how the outlines connect in reality and help you imagine how each outline would&nbsp; have the correct effect.</p>\r\n<p>Submit your requirements, review the innovative design concepts exclusively for you from our team and finalize your logo.</p>\r\n</body>\r\n</html>','pages/logo-design.jpg',NULL,'Logo Design,Logo Design in Nepal,custom logo design services in nepal,business logo design in Nepal,cheap and best logo design services in Nepal','We are catering logo design services form small to big organizations and companies in Nepal. Consideration upon your branding needs our team will offer tailored logo designs as well as enhance your custom needs.',3,1,1,1,'2018-06-29 07:24:36','2018-08-13 22:20:30'),(14,'T-shirt Printing','t-shirt-printing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">We print for:</strong></p>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Academic Instutions</strong>: Sports and uniforms. </p>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Cooperate &amp; Elections</strong>: Offices, Clerks, Cooperate Functions, and Police Departments.</p>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Personal</strong>: Families, Individual, couple and Gifts.</p>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Events</strong>: Festivals, Reunions, Concerts, Birthdays, Holidays, and&nbsp; volunteering.</p>\r\n</body>\r\n</html>','pages/t-shirt-printing.jpg',NULL,'Affordable T-shirt Printing, Best T-shirt Printing in Nepal, Best font for T-shirt Printing, Cheap T-shirt Printing','Our operation quality custom T-shirt printing in Nepal. Mass garments printing Nepal pros. Exciting client benefit.',3,1,1,1,'2018-06-29 07:27:07','2018-08-13 22:27:21'),(15,'Brochure Printing & Flyers','brochure-printing-flyers','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Karkalo offers premium flyer and leaflet administrations for nearby brands. Our company\'s driven way to deal with printing has given our customers scope of mind boggling visual outcomes.It\'s fundamental that your interchanges emerge from the group. Utilizing our creative way to deal with advanced printing with top notch plans, materials and designs&nbsp; that will help take your image story to another level. Regardless of whether you need to advance another menu at your bar or bistro, connect with your customers to enhance your connections or basically need to redesign your special system, contact Karkalo in Kathmandu today for amazing pamphlet printing.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Draw in with your clients, individuals and customers with striking symbolism of your leaflets and flyers. Karkalo utilizes imaginative advanced printing techniques and top notch materials to catch consideration and convey a significant contact with your gathering of people. Not exclusively do we create flyers and booklets in customary organizations yet we additionally offer complex developments for direct mail. With crease outs or covered mailing sleeves for your special material you will have the capacity to accomplish more positive recognitions and more noteworthy review of your image. Get in touch with us today to discover precisely how our pamphlet printing administrations can enable your image to accomplish new levels of client commitment.</p>\r\n</body>\r\n</html>','pages/brochure-printing-flyers.jpg',NULL,'Flyer and Brochure services in Nepal,Brochure and Flyers maker,Brochure and Flyers Printing,Brochure and Flyers in Budget,Customized Brochure and Flyers Design,','Affordable  Flyers and Brochure Services in Nepal.',3,1,1,1,'2018-06-29 07:30:03','2018-08-13 22:51:35'),(16,'ID Card Printing','id-card-printing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>With security being at such a high need nowadays, organizations and associations like schools, private companies, and corporations have taken additional endeavors to give a protected and secure condition for representatives and the general population inside their structures.&nbsp;</p>\r\n<p>A clear and cost-productive way of organizations enhancing their security endeavors is requiring ID identifications. Having a decently desingned ID card is essential in the working environment since IDs should be a simple method to distinguish people and whether they\'re permitted to be insure territories in the working environment. A basic ID prerequisite may appear to be little, however it can keep terrible circumstances from occurring in any case.&nbsp;So as to create first rate ID cards, there are a couple of fundamentals that should be set up with a specific security necessities each organization has for their identifications.</p>\r\n</body>\r\n</html>','pages/id-card-printing.jpg',NULL,'ID Card Printing, ID Card Printing in Nepal, Affordable ID Card Printing, Custom ID Card Printing, Business ID Card Printing, Company ID Card Printing','Our ID card printing administration is quick and practical. Karkalo presents its national ID card printing arrangement.Outsource your association\'s plastic card printing with IdentiSys. Get in touch with us.',3,1,1,1,'2018-06-29 07:34:52','2018-08-13 23:05:28'),(17,'Interior Design and Decor','interior-design-and-decor','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>The design of the work environment impacts innovation, employee engagement, and performance. Our services enhance office optimization, design and business operations. We create arrangements that progressively affect any workplace. From idea to finishing, we embrace inside tasks in each perspective: repairs, design, painting, flooring &amp; furnishing, furniture, and relocation. </p>\r\n</body>\r\n</html>','pages/interior-design-and-decor.jpg',NULL,'Interior Design and Decor in Nepal,best interior design in Nepal,best interior designs in Nepal,Interior Design and Decor','Interior Design and Decor is the art of  upgrading the inside of a workspace or a building to create more pleasing condition for the people using the space incorporating space planning, site inspections,and execution of designs.',3,1,1,1,'2018-06-29 07:38:58','2018-08-13 23:11:13'),(18,'LED Scrolling Board','led-scrolling-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>These displays are microprocessor-based Moving Message Display, made up of light emitting diode [LEDs] arranged in an array. The moving message or data can be fed &amp; set with the help of keyboard on the corded remote. The system can also be customized to remote operate through GPRS system, MMS, etc.</p>\r\n<p><strong>It can be of any color:-</strong></p>\r\n<ul>\r\n<li>Single color</li>\r\n<li>Tricolor</li>\r\n<li>Full-color</li>\r\n<li>Feature Led moving display boards</li>\r\n</ul>\r\n<p><strong>Product details </strong></p>\r\n<ul>\r\n<li>Display shape Square, Rectangle</li>\r\n<li>Display Size 12&rdquo;/6&rdquo;</li>\r\n<li>Lighting Color Red, Blue, Green, White, Single Colors</li>\r\n<li>Operating Voltage 5V DC</li>\r\n</ul>\r\n<p><strong>P10 Moving Led Message Sign&rsquo;s Key Feature</strong></p>\r\n<ul>\r\n<li>&nbsp;Available at different colors and size</li>\r\n<li>Available indoor and outdoor display</li>\r\n<li>Support Temperature sensor</li>\r\n<li>Long viewing distance and wide viewing angle</li>\r\n<li>Low power consumption and low maintenance cost</li>\r\n<li>Full language support</li>\r\n<li>Real-time clock and calendar</li>\r\n<li>Auto power on/off function</li>\r\n<li>Multiple fonts and moving effects</li>\r\n<li>Support at most 200 programs each program support 16 partitions simultaneous play each image-text 200 messages. (quantity in storage space under the premise of memory)</li>\r\n<li>Led moving sign can be widely used in shopping mall, retails store, Caf&eacute;, hotel, school, college, railway station, college, station, highway, building and other commercial sectors.</li>\r\n</ul>\r\n</body>\r\n</html>','pages/led-scrolling-board.jpg',NULL,'LED Scrolling Board in Nepal,','LED Scrolling Board is used to display moving message using LED that comes in various shapes and size,colors and fonts. Karkalo tech provides services at very reasonable cost for digital advertisement.',1,1,1,1,'2018-06-29 07:41:30','2018-08-09 14:11:10'),(19,'LED Video Screen','led-video-screen','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>LED Video Screen&nbsp; is increasing progressively&nbsp; because of its capacity to convey seamless pictures, versatility to any size or shape, and fantastic ideal qualities that influence LED video screen&nbsp; look&nbsp; extraordinary from any angle. We offer LED items in a few setups adaptable to your application. Select your choices beneath and see accessible models based on viewing distance upto 60 metres and pixel pitch upto 20mm.</p>\r\n<p>APPLICATIONS</p>\r\n<ul>\r\n<li>Indoor</li>\r\n<li>Outdoor</li>\r\n<li>Fixed&nbsp;Rental</li>\r\n<li>Staging</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>FEATURES</p>\r\n<ul>\r\n<li>Ultra Lightweight</li>\r\n<li>Redundant Video</li>\r\n<li>Ultra HD</li>\r\n<li>Low power</li>\r\n</ul>\r\n</body>\r\n</html>','pages/led-video-screen.jpg',NULL,'LED Video Screen in Nepal, LED advertisement board in Nepal,video scrolling board in Nepal','The concept for digital advertisement in LED Video Screen is increasing progressively in Nepal. We provide Video screen with best quality that can be viewed from any angle .',1,1,1,1,'2018-06-29 07:43:11','2018-08-13 21:23:14'),(20,'Acrylic Letter Board','acrylic-letter-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>We are offering a wide cluster of 3D LED Letter Board that is accessible in different examples to take care of the particular demand of customers. These items are produced utilizing quality materials.&nbsp;</p>\r\n<p>Features:</p>\r\n<ul>\r\n<li>Smooth surface finish</li>\r\n<li>Unique designs</li>\r\n<li>Eye-catching appearance</li>\r\n<li>Easy to use</li>\r\n<li>High strength</li>\r\n<li>Lightweight</li>\r\n</ul>\r\n</body>\r\n</html>','pages/acrylic-letter-board.jpg',NULL,'Acrylic Letter Board in Nepal,stylish letter for advertisement in nepal,designs for company name,','Karkalo provides Acrylic Letter Board with multiple colours and fonts with premium quality.You can also provide detailed specifications by visiting us in Kathmandu,Nepal.',1,1,1,1,'2018-06-29 07:45:52','2018-08-13 21:24:01'),(21,'Metal Letter Board','metal-letter-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>By keeping ourselves up to date with the advancement occurring in this industry domain, we have been immersed in presenting of Stainless Metal Letter Board. Particularly planned and built up, these letters are outlined by making utilization of optimum-class basic material alongside modernized methods coupled with the market set standards. Besides, these letters are cost effective.</p>\r\n<p>Features:</p>\r\n<ul>\r\n<li>Fine finish</li>\r\n<li>Excellent strength</li>\r\n<li>Alluring designs</li>\r\n</ul>\r\n</body>\r\n</html>','pages/metal-letter-board.jpg',NULL,'Metal Letter Board in Nepal,metal letter in nepal,metal letter for advertising,metal letters for company name,stylish letter for organization name','We understand your advertisement needs of your company. Karkalo provides Metal Letter Board with sleek and eye catching finish with alluring designs.',1,1,1,1,'2018-06-29 07:47:03','2018-08-13 21:27:40'),(22,'Slim Light Box','slim-light-box','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>From the recent years, we are occupied with assembling and providing a profuse collection of LED Slim light boxes. Available in varied details, these light boxes are created from quality endorsed raw materials that have been sourced from trusted and legitimate sellers of the business. Every last thing offered by us is thoroughly checked and analyzed on set industry standards.</p>\r\n<p>Features:</p>\r\n<ul>\r\n<li>Modern designed</li>\r\n<li>Unique patterns</li>\r\n<li>Durable finish</li>\r\n<li>Full visibility</li>\r\n<li>Quick and easy assembly</li>\r\n</ul>\r\n</body>\r\n</html>','pages/slim-light-box.jpg',NULL,'Slim Light Box in Nepal,light box in Nepal,advertisement light box in Kathmandu','We offer Slim Light Box with wide range of dimensions for light box for digital advertisement in Nepal.',1,1,1,1,'2018-06-29 07:48:31','2018-08-13 21:30:57'),(23,'Light Box and Flex Board','light-box-and-flex-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>We are one of the reputed organization involved in providing Flex Print, Flex Board Services to our clients. These are available in different attractive designs. We provide these service at the most affordable price.</p>\r\n</body>\r\n</html>','pages/light-box-and-flex-board.jpg',NULL,'Light and Flex Board','We offer Light Box and flex board for advertisement with wide range of dimensions that are durable and do not wear off in harsh environmental conditions of Nepal with attention grabbing output at standard market rates.',1,1,1,1,'2018-06-29 07:53:29','2018-08-13 21:33:46'),(24,'Job Portal Sites - Applyjob','job-portal-sites-applyjob','portfolio','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Job Portal Sites - Applyjob.com.np</p>\r\n</body>\r\n</html>','pages/job-portal-sites-applyjob.png',NULL,'Job Portal Sites - Applyjob.com.np','Job Portal Sites - Applyjob.com.np',2,1,0,0,'2018-06-29 08:09:20','2018-08-09 14:11:10'),(25,'Cementry Management System - (Western Charity Foundation)','cementry-management-system-western-charity-foundation','portfolio','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Cementry Management System - (Western Charity Foundation)</p>\r\n</body>\r\n</html>','pages/cementry-management-system-western-charity-foundation.png',NULL,'Cementry Management System - (Western Charity Foundation)','Cementry Management System - (Western Charity Foundation) \r\nhttp://www.westerncharitablefoundation.com/',NULL,1,0,0,'2018-06-29 08:15:00','2018-08-09 16:25:05'),(26,'Introduction','introduction','page','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>Your business depends on your IT systems.</strong></p>\r\n<ul>\r\n<li>Every day our team of experts provide an entirely positive, above and beyond experience to every client in the karkalo tech.We handle all aspects of your IT infrastructure including hardware and software management, vendor relationships for your digital advertisement, website management, and maintenance renewals, and any other related technology needs. We focus on your IT so you can focus on your business.</li>\r\n</ul>\r\n<p><strong>A Wide Spectrum Of Skills And Experience.</strong></p>\r\n<ul>\r\n<li>From quick PC fixes to total server and software engineering &ndash; we&rsquo;ve got it. And if there&rsquo;s ever a problem we can&rsquo;t solve, we know who to contact to get it fixed.</li>\r\n</ul>\r\n<p><strong>Committed to Quality.</strong></p>\r\n<ul>\r\n<li>We don&rsquo;t pursue every company that needs computer support. We choose only clients that share in our values. Serving a company&rsquo;s IT and critical network needs is a HUGE responsibility that we take that very seriously. It takes teamwork and a solid commitment to good communication, excellence, and industry best practices to serve a company in an excellent manner. If we cannot succeed in an excellent manner because of value differences &ndash; we simply don&rsquo;t pursue the opportunity.</li>\r\n</ul>\r\n<p><strong>Our efficiencies allow us to pass benefits to you.</strong></p>\r\n<ul>\r\n<li style=\"box-sizing: border-box; margin: 0px; padding: 0px 0px 0px 0.9rem;\">We&rsquo;re proud that we invest in the best service management technology available and we do it quickly. While we say that technology should be used to achieve organisational success, we also practice what we preach.We use cutting-edge service technologies behind the scenes, and this puts us at an advantage over our competitors. We can work optimally &ndash; with better accuracy and efficiently. Lower running costs for us are passed to you as cost-effectiveness and better service quality.</li>\r\n</ul>\r\n<div class=\"styled-list chevron\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: #212121; font-family: myriad-pro, \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif; font-size: 16px;\">&nbsp;</div>\r\n</body>\r\n</html>','pages/why-choose-us.jpg',NULL,NULL,NULL,NULL,1,0,0,'2018-08-07 10:08:00','2018-08-09 14:11:10');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'Apply Job','http://localhost:8000','partner/apply-job.png',1,'2018-06-22 09:59:49','2018-06-22 09:59:49'),(2,'Acs','http://localhost:8000','partner/acs.jpg',1,'2018-06-22 10:00:28','2018-06-22 10:00:28'),(3,'Edy','http://localhost:8000','partner/edy.png',1,'2018-06-22 10:01:51','2018-06-22 10:01:51');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,NULL,NULL),(1,2,NULL,NULL),(1,3,NULL,NULL),(1,4,NULL,NULL),(1,5,NULL,NULL),(1,6,NULL,NULL),(1,7,NULL,NULL),(1,8,NULL,NULL),(1,9,NULL,NULL),(1,10,NULL,NULL),(1,11,NULL,NULL),(1,12,NULL,NULL),(1,13,NULL,NULL),(1,14,NULL,NULL),(1,15,NULL,NULL),(1,16,NULL,NULL),(1,17,NULL,NULL),(1,18,NULL,NULL),(1,19,NULL,NULL),(1,20,NULL,NULL),(1,21,NULL,NULL),(1,22,NULL,NULL),(1,23,NULL,NULL),(1,24,NULL,NULL),(1,25,NULL,NULL),(1,26,NULL,NULL),(1,27,NULL,NULL),(1,28,NULL,NULL),(1,29,NULL,NULL),(1,30,NULL,NULL),(1,31,NULL,NULL),(1,32,NULL,NULL),(1,33,NULL,NULL),(1,34,NULL,NULL),(1,35,NULL,NULL),(1,36,NULL,NULL),(1,37,NULL,NULL),(1,38,NULL,NULL),(1,39,NULL,NULL),(1,40,NULL,NULL),(1,41,NULL,NULL),(1,42,NULL,NULL),(1,43,NULL,NULL),(1,44,NULL,NULL),(1,45,NULL,NULL),(1,46,NULL,NULL),(1,47,NULL,NULL),(1,48,NULL,NULL),(1,49,NULL,NULL),(1,50,NULL,NULL),(1,51,NULL,NULL),(1,52,NULL,NULL),(1,53,NULL,NULL),(1,54,NULL,NULL),(1,55,NULL,NULL),(1,56,NULL,NULL),(1,57,NULL,NULL),(1,58,NULL,NULL),(1,59,NULL,NULL),(1,60,NULL,NULL),(1,61,NULL,NULL),(1,62,NULL,NULL),(1,63,NULL,NULL),(1,64,NULL,NULL),(1,65,NULL,NULL),(1,66,NULL,NULL),(1,67,NULL,NULL),(1,68,NULL,NULL),(1,69,NULL,NULL),(1,70,NULL,NULL),(1,71,NULL,NULL),(1,72,NULL,NULL),(1,73,NULL,NULL),(1,74,NULL,NULL),(1,75,NULL,NULL),(1,76,NULL,NULL),(1,77,NULL,NULL),(1,78,NULL,NULL),(1,79,NULL,NULL),(1,80,NULL,NULL),(1,81,NULL,NULL),(1,82,NULL,NULL),(1,83,NULL,NULL),(1,84,NULL,NULL),(1,85,NULL,NULL),(1,86,NULL,NULL),(1,87,NULL,NULL),(1,88,NULL,NULL),(1,89,NULL,NULL),(1,90,NULL,NULL),(1,91,NULL,NULL),(1,92,NULL,NULL),(1,93,NULL,NULL),(1,94,NULL,NULL),(1,95,NULL,NULL),(1,96,NULL,NULL),(1,97,NULL,NULL),(1,98,NULL,NULL),(1,99,NULL,NULL),(1,100,NULL,NULL),(1,101,NULL,NULL),(1,102,NULL,NULL),(1,103,NULL,NULL),(1,1,NULL,NULL),(1,2,NULL,NULL),(1,3,NULL,NULL),(1,4,NULL,NULL),(1,5,NULL,NULL),(1,6,NULL,NULL),(1,7,NULL,NULL),(1,8,NULL,NULL),(1,9,NULL,NULL),(1,10,NULL,NULL),(1,11,NULL,NULL),(1,12,NULL,NULL),(1,13,NULL,NULL),(1,14,NULL,NULL),(1,15,NULL,NULL),(1,16,NULL,NULL),(1,17,NULL,NULL),(1,18,NULL,NULL),(1,19,NULL,NULL),(1,20,NULL,NULL),(1,21,NULL,NULL),(1,22,NULL,NULL),(1,23,NULL,NULL),(1,24,NULL,NULL),(1,25,NULL,NULL),(1,26,NULL,NULL),(1,27,NULL,NULL),(1,28,NULL,NULL),(1,29,NULL,NULL),(1,30,NULL,NULL),(1,31,NULL,NULL),(1,32,NULL,NULL),(1,33,NULL,NULL),(1,34,NULL,NULL),(1,35,NULL,NULL),(1,36,NULL,NULL),(1,37,NULL,NULL),(1,38,NULL,NULL),(1,39,NULL,NULL),(1,40,NULL,NULL),(1,41,NULL,NULL),(1,42,NULL,NULL),(1,43,NULL,NULL),(1,44,NULL,NULL),(1,45,NULL,NULL),(1,46,NULL,NULL),(1,47,NULL,NULL),(1,48,NULL,NULL),(1,49,NULL,NULL),(1,50,NULL,NULL),(1,51,NULL,NULL),(1,52,NULL,NULL),(1,53,NULL,NULL),(1,54,NULL,NULL),(1,55,NULL,NULL),(1,56,NULL,NULL),(1,57,NULL,NULL),(1,58,NULL,NULL),(1,59,NULL,NULL),(1,60,NULL,NULL),(1,61,NULL,NULL),(1,62,NULL,NULL),(1,63,NULL,NULL),(1,64,NULL,NULL),(1,65,NULL,NULL),(1,66,NULL,NULL),(1,67,NULL,NULL),(1,68,NULL,NULL),(1,69,NULL,NULL),(1,70,NULL,NULL),(1,71,NULL,NULL),(1,72,NULL,NULL),(1,73,NULL,NULL),(1,74,NULL,NULL),(1,75,NULL,NULL),(1,76,NULL,NULL),(1,77,NULL,NULL),(1,78,NULL,NULL),(1,79,NULL,NULL),(1,80,NULL,NULL),(1,81,NULL,NULL),(1,82,NULL,NULL),(1,83,NULL,NULL),(1,84,NULL,NULL),(1,85,NULL,NULL),(1,86,NULL,NULL),(1,87,NULL,NULL),(1,88,NULL,NULL),(1,89,NULL,NULL),(1,90,NULL,NULL),(1,91,NULL,NULL),(1,92,NULL,NULL),(1,93,NULL,NULL),(1,94,NULL,NULL),(1,95,NULL,NULL),(1,96,NULL,NULL),(1,97,NULL,NULL),(1,98,NULL,NULL),(1,99,NULL,NULL),(1,100,NULL,NULL),(1,101,NULL,NULL),(1,102,NULL,NULL),(1,103,NULL,NULL),(1,104,NULL,NULL),(1,105,NULL,NULL);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `object` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'backend','dashboard','backend.dashboard','2018-08-09 03:46:28','2018-08-09 03:46:28'),(2,'category','index','category.index','2018-08-09 03:46:28','2018-08-09 03:46:28'),(3,'category','create','category.create','2018-08-09 03:46:28','2018-08-09 03:46:28'),(4,'category','store','category.store','2018-08-09 03:46:28','2018-08-09 03:46:28'),(5,'category','show','category.show','2018-08-09 03:46:28','2018-08-09 03:46:28'),(6,'category','edit','category.edit','2018-08-09 03:46:28','2018-08-09 03:46:28'),(7,'category','update','category.update','2018-08-09 03:46:28','2018-08-09 03:46:28'),(8,'category','destroy','category.destroy','2018-08-09 03:46:28','2018-08-09 03:46:28'),(9,'page','index','page.index','2018-08-09 03:46:28','2018-08-09 03:46:28'),(10,'page','create','page.create','2018-08-09 03:46:28','2018-08-09 03:46:28'),(11,'page','store','page.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(12,'page','show','page.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(13,'page','edit','page.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(14,'page','update','page.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(15,'page','destroy','page.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(16,'post','index','post.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(17,'post','create','post.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(18,'post','store','post.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(19,'post','show','post.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(20,'post','edit','post.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(21,'post','update','post.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(22,'post','destroy','post.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(23,'product','index','product.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(24,'product','create','product.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(25,'product','store','product.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(26,'product','show','product.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(27,'product','edit','product.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(28,'product','update','product.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(29,'product','destroy','product.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(30,'metas','index','metas.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(31,'metas','create','metas.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(32,'metas','store','metas.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(33,'metas','show','metas.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(34,'metas','edit','metas.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(35,'metas','update','metas.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(36,'metas','destroy','metas.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(37,'teams','index','teams.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(38,'teams','create','teams.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(39,'teams','store','teams.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(40,'teams','show','teams.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(41,'teams','edit','teams.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(42,'teams','update','teams.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(43,'teams','destroy','teams.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(44,'gallery','index','gallery.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(45,'gallery','create','gallery.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(46,'gallery','store','gallery.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(47,'gallery','show','gallery.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(48,'gallery','edit','gallery.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(49,'gallery','update','gallery.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(50,'gallery','destroy','gallery.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(51,'menu','index','menu.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(52,'menu','create','menu.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(53,'menu','store','menu.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(54,'menu','show','menu.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(55,'menu','edit','menu.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(56,'menu','update','menu.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(57,'menu','destroy','menu.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(58,'user','index','user.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(59,'user','create','user.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(60,'user','store','user.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(61,'user','show','user.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(62,'user','edit','user.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(63,'user','update','user.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(64,'user','destroy','user.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(65,'role','index','role.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(66,'role','create','role.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(67,'role','store','role.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(68,'role','show','role.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(69,'role','edit','role.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(70,'role','update','role.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(71,'role','destroy','role.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(72,'faq','index','faq.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(73,'faq','create','faq.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(74,'faq','store','faq.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(75,'faq','show','faq.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(76,'faq','edit','faq.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(77,'faq','update','faq.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(78,'faq','destroy','faq.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(79,'partner','index','partner.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(80,'partner','create','partner.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(81,'partner','store','partner.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(82,'partner','show','partner.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(83,'partner','edit','partner.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(84,'partner','update','partner.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(85,'partner','destroy','partner.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(86,'testimonial','index','testimonial.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(87,'testimonial','create','testimonial.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(88,'testimonial','store','testimonial.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(89,'testimonial','show','testimonial.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(90,'testimonial','edit','testimonial.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(91,'testimonial','update','testimonial.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(92,'testimonial','destroy','testimonial.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(93,'advertisement','index','advertisement.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(94,'advertisement','create','advertisement.create','2018-08-09 03:46:29','2018-08-09 03:46:29'),(95,'advertisement','store','advertisement.store','2018-08-09 03:46:29','2018-08-09 03:46:29'),(96,'advertisement','show','advertisement.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(97,'advertisement','edit','advertisement.edit','2018-08-09 03:46:29','2018-08-09 03:46:29'),(98,'advertisement','update','advertisement.update','2018-08-09 03:46:29','2018-08-09 03:46:29'),(99,'advertisement','destroy','advertisement.destroy','2018-08-09 03:46:29','2018-08-09 03:46:29'),(100,'contact','index','contact.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(101,'contact','show','contact.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(102,'comment','index','comment.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(103,'comment','show','comment.show','2018-08-09 03:46:29','2018-08-09 03:46:29'),(104,'setting','index','setting.index','2018-08-09 03:46:29','2018-08-09 03:46:29'),(105,'setting','store','setting.store','2018-08-09 03:46:29','2018-08-09 03:46:29');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Customers Reviews In Testimonials Services','customers-reviews-in-testimonials-services','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #555555; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: #fafafa;\">I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happier. Will definitely be coming back for more in the future.</span></p>\r\n</body>\r\n</html>','posts/customers-reviews-in-testimonials-services.jpg',NULL,NULL,'I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happ',1,13,1,1,0,'2018-06-22 09:53:42','2018-08-14 01:10:21'),(2,'Short natural product reviews.','short-natural-product-reviews','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">I will create a fast video review of your product or company.&nbsp;</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">This is a very natural looking review. It is NOT slick and sales.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">I will need access to your product.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">For Digital:</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">I will go to your website and be using Camtasia create a video of it, while I talk about the benefits of your product. This looks more realistic if I am actually inside the member\'s area.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">If there are certain points that you wish to cover, let me know.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">Physical Product Reviews:</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">Please contact me first and then send the physical product. I will create the custom order after I receive it.</span></p>\r\n</body>\r\n</html>','posts/short-natural-product-reviews-this-is-me-reviewing-10-digital-or-physical-products.jpg',NULL,NULL,'I will create a fast video review of your product or company. \r\nThis is a very natural looking review. It is NOT slick and sales.\r\nI will need access to your product.',1,28,1,1,0,'2018-06-22 09:56:16','2018-08-14 00:49:03'),(3,'Outstanding Experience!','outstanding-experience','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #555555; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">The seller did a real and honest review of our product, as promised. She did it in the most timely way (note that we had to send her product and give her time to use it). The review is authentic. I recommend this gig!</span></p>\r\n</body>\r\n</html>','posts/outstanding-experience.jpg',NULL,NULL,'The seller did a real and honest review of our product, as promised. She did it in the most timely way (note that we had to send her product and give her time to use it). The review is authentic. I recommend this gig!',1,46,1,1,0,'2018-06-22 09:58:16','2018-08-14 01:15:51'),(4,'Need and Importance of Digital Marketing these days','need-and-importance-of-digital-marketing-these-days','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>With the unfurling of current advancements and prevalence of Digital Marketing, organizations are doing all that they can to catch up to the pace. Organizations are either changing their plans of activity into the advanced one, or increasing existing promoting systems with computerized publicizing procedures And the principal question that may emerge here is-Why Digital Marketing is vital for your organizations.Let\'s walk through some factual impacts of Digital Marketing perused through marketo.com.</p>\r\n<p>1.Digital Advertising is Targeted</p>\r\n<p>DM levels the internet playing field and offers meet open doors for a wide range of organizations. It is not any more like the past times situation when multinationals and huge business houses for the most part grasped Digital Marketing. Presently, DM truly levels the chances, giving nearly nothing and medium organizations the chance to go up against the set up organizations and attract their offer of activity.Little and medium scale organizations or new businesses currently have the advantages to perform deals and advertising forms that were in advance open to enormous organizations.While thinking about the significance of Digital Media Marketing, the most observable advantage is its capacity to draw in with various clients without utilizing call focus administrations.Indeed, even the changes related with DM rank higher than different methods of advertising and correspondences.</p>\r\n<p>&nbsp;</p>\r\n<p>2. Digital Advertising Provides Flexibility Across Channels</p>\r\n<p>The catchphrase for accomplishment in advanced publicizing is \"multi-channel.\" With computerized promoting, a bit of substance can be shared over an assortment of stages, which implies that you are never again bound to one page, 30 seconds, or whatever organization your customary advertisement was intended for.Cisco\'s new crusade is an incredible case of how to gain by various stages. The organization is refining complex tech subjects into six-second recordings on the interpersonal organization Vine, and afterward expanding that scrap with comparing content, for example, connections to articles about security and investigation. This multi-stage methodology is enabling their clients to expend content the manner in which they need to&mdash;as a speedy bite or a full feast. This can work for any advertiser. Take a short Vine video and form it into a vlog to offer more top to bottom data. Reconsider long-frame content by changing key information focuses or insights into an infographic or a SlideShare introduction. Repurposing existing substance is a win for both your procedure and your financial plan.</p>\r\n<p>&nbsp;</p>\r\n<p>3.Digital Advertising Invites Mobile Engagement</p>\r\n<p>Advanced and versatile advertising go as one. Advanced promoting enables you to contact your crowd in a hurry, wherever they are, at whatever point they are on the web. It\'s a particularly incredible place to achieve Gen Z and Millennial clients. As indicated by an examination led by the Interactive Advertising Bureau (IAB), cell phone are the place Gen Z and Millennial are destined to see pertinent advertisements. Far better, about portion of all common cell phone clients have made a move in view of seeing significant promotions on their cell phones.Truth be told, it may even be more viable to promote on portable than work area. AdRoll\'s \"Facebook by the Numbers\" report found that the active visitor clicking percentage for Facebook News Feed advertisements is commonly 10% higher for portable than work area and produces a cost-per-click that was 61% lower than on work area.Best of all, this does not mean you require yet another remarkable battle. Responsive plan is a way to deal with advanced outline that naturally streamlines the substance for whatever gadget it is seen on. You can begin amplifying versatile commitment just by ensuring that your messages, presentation pages, and substance pieces make an interpretation of easily from work area to cell phone.</p>\r\n<p>&nbsp;</p>\r\n<p>4. Digital Advertising is Faster to Market</p>\r\n<p>Economic situations and patterns change rapidly, and that requires deft promoting. Lead times for conventional promoting techniques are so 1990s. Between the time it takes to make an advertisement, to the long lead times required to purchase time or space, your message and your financial plan experience the ill effects of being bolted into an outline that was made a long time before it will see the light of day.Advanced battles can be executed significantly more rapidly, and they can be altered on the fly if necessary. Think about that as of late as April, Subway was touting another advertisement crusade with its long-term representative, Jared. After an embarrassment this mid year that brought about his capture, Subway couldn\'t separate itself from their pitchman sufficiently quick. Regardless of whether you\'re attempting to profit by the news or separation yourself from it, enormous brands are insightful to hold however much rotate control as could be expected.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>5. Digital Advertising is Easily Measurable</p>\r\n<p>Each division needs to indicate how it is including esteem, and with an advanced crusade the measurements are manufactured right in. You know precisely who tapped on your substance, opened it, alluded it, shared it, and so forth. You likewise can follow the wellspring of your activity from section to the shopping basket and inevitable buy. This will enable you to figure out which stages and systems yield the best outcomes, and which merit greater speculation.The key explanation behind following measurements is to talk a similar dialect that your CEO and your CFO do. While delicate measurements like brand mindfulness, impressions, natural inquiry rankings, and reach are essential, their genuine esteem lies by they way they can be quantifiably associated with hard measurements like pipeline, income, and benefit. Advanced publicizing offers a stage for following the two sorts of measurements to demonstrate a solid ROI.Since advanced publicizing rushes to quantify&mdash;regularly with ongoing outcomes and measurements&mdash;advertisers may find that it encourages them make their crusades more financially savvy. On the off chance that one of your computerized promoting programs isn\'t performing great or working, it\'s anything but difficult to rapidly modify your program to guarantee that your financial plan is spent well and offers a solid ROI.</p>\r\n<p>&nbsp;</p>\r\n<p>6. Digital Advertising Maintains Top-of-Mind Awareness</p>\r\n<p>Does your client like that sweater? Perhaps she does, yet perhaps insufficient to get it. An investigation by Baymard Institute uncovered that the normal internet shopping basket deserting rate is 68.53%. Also, that is the place computerized publicizing has a noteworthy preferred standpoint. While it would be frightening and frantic to waylay that client outside your store in the shopping center and beseech her to return to influence a buy, with advanced publicizing you to can connect with her again without appearing a stalker.Through remarketing and retargeting, you can remain before her to propose once again that she buy that sweater. Retargeting implies that the sweater will up in her Facebook sidebar whenever she sign in or shows up in a promotion spot on her most loved blog that utilizations Google advertisements. With remarketing, you can ping her again with an email to help her to remember that forlorn shopping basket. Truth be told, relinquished shopping baskets can be recoverable by retailers who utilize showcasing mechanization programming to offer a sweet arrangement, similar to a rebate or free sending. Computerized promoting can fend off that deal from slipping.</p>\r\n<p>&nbsp;</p>\r\n<p>7. Digital Advertising Can Go Viral</p>\r\n<p>(At least, that&rsquo;s what the fox says!) With a sturdy digital advertising and marketing factor, you, too, ought to turn out to be the following viral sensation.Take Dove&rsquo;s &ldquo;Real Beauty Sketches,&rdquo; which featured actual ladies considering themselves thru some other&rsquo;s eyes. Just one video, in an ongoing marketing campaign, garnered greater than 114 million views the first month alone, just because humans chose to share it on social media web sites. When you recall that 114 million is ready the identical quantity of folks who noticed the average 2015 Super Bowl industrial, consistent with NBC Sports, the electricity of viral content material will become obvious. Dove would have spent $4.Five million in line with 30 seconds for a Super Bowl commercial, and still wouldn&rsquo;t are becoming the sturdy social evidence inherent in social media shares.There&rsquo;s no magic formula for the way or why something goes viral, but, in standard, the content material must communicate to our feelings. Content that makes humans snort or sense good about themselves is very shareable. Beyond that, it&rsquo;s hard to expect what\'s going to make some thing take off, however as soon as it does it is able to tackle a life of its own.</p>\r\n</body>\r\n</html>','posts/.png',NULL,'significance of digital advertising nowadays, importance of search engine marketing in digital advertising and marketing, Why Digital Marketing is essence and importance,','With the unfurling of current advancements and prevalence of Digital Marketing, Organizations are taking their initiations to take advantage of Digital marketing and advertisement......',1,47,1,1,0,'2018-06-25 13:08:03','2018-08-14 01:18:25'),(5,'General Start Up Guide to Establish a Business','general-start-up-guide-to-establish-a-business','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">There are nearly 1 milllion small businesses in Nepal. When you think about the absolute most well known motivations to begin a business, including having a special business thought, outlining a vocation that has the adaptability to develop with you, progressing in the direction of money related autonomy, and putting resources into yourself &mdash; it\'s no big surprise that independent companies are all around.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Be that as it may, few out of every odd private company is situated for progress. Actually, just around 66% of organizations with representatives get by no less than two years, and about half survive five years. So you might be in for a genuine test when you choose to dive in, jettison your normal everyday employment, and turn into an entrepreneur. The stage is regularly set at the outset, so ensuring you take after the majority of the essential advances when beginning your business can set the establishment for progress.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Here are 10 stages that are required to begin a business effectively. Approach slowly and carefully, and you\'ll be headed to fruitful private company proprietorship.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 1:Do your Research</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">In all likelihood you have effectively recognized a business thought, so currently it\'s an ideal opportunity to adjust it with a little reality. Does your thought can possibly succeed? You should maintain your business thought through an approval procedure before you go any further.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">All together for an independent venture to be fruitful, it must take care of an issue, satisfy a need or offer something the market needs.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">There are various ways you can recognize this need, including research, center gatherings, and even experimentation. As you investigate the market, a portion of the inquiries you should answer include:</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Is there a requirement for your foreseen items/administrations?</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Who needs it?</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Are there different organizations offering comparative items/benefits now?</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">How is the opposition?</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">By what method will your business fit into the market?</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 2:Planning</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">You require an arrangement so as to make your business thought a reality. A strategy for success is a plan that will control your business from the start-up stage through foundation and in the long run business development, and it is an unquestionable requirement have for every single new business.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Fortunately there are distinctive sorts of marketable strategies for various kinds of organizations.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">On the off chance that you mean to look for monetary help from a speculator or money related establishment, a customary strategy for success is an absolute necessity. This sort of strategy for success is by and large long and intensive and has a typical arrangement of areas that speculators and banks search for when they are approving your thought.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">On the off chance that you don\'t envision looking for monetary help, a basic one-page marketable strategy can give you clearness about what you want to accomplish and how you intend to do it. Truth be told, you can even make a working marketable strategy on the back of a napkin, and enhance it after some time. Some sort of plan in composing is constantly superior to nothing.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 3: Finance Planning</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Beginning a private venture doesn\'t need to require a considerable measure of cash, yet it will include some underlying speculation and the capacity to cover progressing costs before you are turning a benefit. Set up together a spreadsheet that gauges the one-time startup costs for your business (licenses and allows, hardware, legitimate charges, protection, marking, statistical surveying, stock, trademarking, great opening occasions, property leases, and so forth.), and additionally what you envision you should keep your business running for no less than a year (lease, utilities, promoting and publicizing, creation, supplies, travel costs, worker pay rates, your own particular compensation, and so forth.).</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Those numbers consolidated is the underlying venture you will require.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 4: Pick a Business Structure</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Your independent venture can be a sole proprietorship, an organization, a restricted risk organization (LLC) or a partnership. The business element you pick will affect numerous variables from your business name, to your risk, to how you document your expenses.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">You may pick an underlying business structure, and after that reexamine and change your structure as your business develops and needs change.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Contingent upon the unpredictability of your business, it might be worth putting resources into a meeting from a lawyer or CPA to guarantee you are settling on the correct structure decision for your business.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 5: Pick and Register Your Business Name</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Your business name assumes a part in relatively every part of your business, so you need it to be a decent one. Ensure you thoroughly consider the greater part of the potential ramifications as you investigate your alternatives and pick your business name.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">When you have picked a name for your business, you should check if it\'s trademarked or as of now being used. At that point, you should enlist it. A sole proprietor must enroll their business name with either their state or district representative. Companies, LLCs, or constrained associations normally enroll their business name when the arrangement printed material is recorded.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Bear in mind to enroll your space name once you have chosen your business name. Attempt these alternatives if your optimal space name is taken.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 6:Get Licenses and Permits</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Printed material is a piece of the procedure when you begin your own business.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">There are an assortment of independent venture licenses and allows that may apply to your circumstance, contingent upon the kind of business you are beginning and where you are found. You should inquire about what licenses and allows apply to your business amid the start-up process.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 7: Choosing Accounting System</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Independent companies run most successfully when there are frameworks set up. A standout amongst the most essential frameworks for an independent venture is a bookkeeping framework.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Your bookkeeping framework is essential keeping in mind the end goal to make and deal with your financial plan, set your rates and costs, lead business with others, and record your duties. You can set up your bookkeeping framework yourself, or contract a bookkeeper to take away a portion of the mystery. In the event that you choose to begin without anyone else, ensure you consider these inquiries that are indispensable while picking bookkeeping programming.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Your bookkeeping framework is essential keeping in mind the end goal to make and deal with your financial plan, set your rates and costs, lead business with others, and record your duties. You can set up your bookkeeping framework yourself, or contract a bookkeeper to take away a portion of the mystery. In the event that you choose to begin without anyone else, ensure you consider these inquiries that are indispensable while picking bookkeeping programming.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 8: Business Location</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Independent companies run most successfully when there are frameworks set up. A standout amongst the most essential frameworks for an independent venture is a bookkeeping framework.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Independent companies run most successfully when there are frameworks set up. A standout amongst the most essential frameworks for an independent venture is a bookkeeping framework.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Your bookkeeping framework is essential keeping in mind the end goal to make and deal with your financial plan, set your rates and costs, lead business with others, and record your duties. You can set up your bookkeeping framework yourself, or contract a bookkeeper to take away a portion of the mystery. In the event that you choose to begin without anyone else, ensure you consider these inquiries that are indispensable while picking bookkeeping programming.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Your bookkeeping framework is essential keeping in mind the end goal to make and deal with your financial plan, set your rates and costs, lead business with others, and record your duties. You can set up your bookkeeping framework yourself, or contract a bookkeeper to take away a portion of the mystery. In the event that you choose to begin without anyone else, ensure you consider these inquiries that are indispensable while picking bookkeeping programming.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\"><strong>Step 9: Get Your Team Ready</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">On the off chance that you will enlist representatives, this is the ideal opportunity to begin the procedure. Ensure you set aside the opportunity to diagram the positions you have to fill, and the activity obligations that are a piece of each position. The Small Business Administration has an astounding manual for contracting your first representative that is valuable for new entrepreneurs.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">In the event that you are not procuring representatives, but rather outsourcing work to self employed entities, right now is an ideal opportunity to work with a lawyer to get your self employed entity assention set up and begin your hunt.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">Finally, in the event that you are a genuine solopreneur hitting the independent venture street alone, you may not require workers or contractual workers, but rather you will in any case require your own particular help group. This group can be involved a guide, private venture mentor, or even your family, and fills in as your go-to asset for exhortation, inspiration and consolation when the street gets rough.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"margin-bottom: 0in; line-height: 100%;\">&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h3 class=\"western\" style=\"font-weight: normal;\"><strong><span style=\"font-size: medium;\">Step 10: Promote Your Small Business</span></strong></h3>\r\n<p>&nbsp;</p>\r\n<p>Once your business is up and running, you have to begin pulling in customers and clients. You\'ll need to begin with the nuts and bolts by composing an interesting offering recommendation (USP) and making a showcasing plan. At that point, investigate whatever number independent venture advertising thoughts as could reasonably be expected so you can choose how to advance your business generally viably. When you have finished these business start-up exercises, you will have the greater part of the most essential bases secured. Remember that achievement doesn\'t occur incidentally. Yet, utilize the arrangement you\'ve made to reliably chip away at your business, and you will expand your odds of progress.</p>\r\n<p></p>\r\n</body>\r\n</html>',NULL,NULL,'How to begin and establish a business,business start-up guide, business starting process,begin a business effectively, guide to establish a company, start up a company','Here is the general overview of the steps to be followed while establishing business, companies and organizations. The appropriate approach to proceed should be slow and steady with heedful stages orderly to lead to fruitful private company',1,16,2,1,0,'2018-08-10 16:54:41','2018-08-13 23:18:29');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_gallery`
--

DROP TABLE IF EXISTS `product_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_gallery` (
  `product_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_gallery`
--

LOCK TABLES `product_gallery` WRITE;
/*!40000 ALTER TABLE `product_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `deal_price` double(8,2) DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `view` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Superadmin','2018-06-11 16:21:20','2018-06-11 16:21:20');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seos`
--

DROP TABLE IF EXISTS `seos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `visit` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seos`
--

LOCK TABLES `seos` WRITE;
/*!40000 ALTER TABLE `seos` DISABLE KEYS */;
INSERT INTO `seos` VALUES (1,'Contact','contact','contact','https://www.karkalo.com/contact',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(2,'Post','post','post','https://www.karkalo.com/post',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(3,'Portfolio','portfolio','portfolio','https://www.karkalo.com/portfolio',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(4,'About','about','about','https://www.karkalo.com/about','web and technology in Nepal, graphics designing in Nepal, all kinds of printing services, digital advertisements such as led display board, led video screen, 3d letter, acrylic raising baord','We provide web and technology in Nepal, graphics designing in Nepal, all kinds of printing services, digital advertisements such as led display board, led video screen, 3d letter, acrylic raising board in Kathmandu, Nepal.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(5,'Our Vision','our-vision','about','https://www.karkalo.com/our-vision',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(6,'Our Mission','our-mission','about','https://www.karkalo.com/our-mission',NULL,'Karkalo provides you with a deep partnership and commitment you need to realize your business and technology goals.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(7,'Privacy Policy','privacy-policy','page','https://www.karkalo.com/privacy-policy','Privacy Policy','Privacy Policy',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(8,'Terms and conditions','terms-and-conditions','page','https://www.karkalo.com/terms-and-conditions','Terms and conditions','Terms and conditions',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(9,'Web Development','web-development','service','https://www.karkalo.com/web-development','Web Development in Nepal,create website,website services in Kathmandu, job-portal site,e-commerce site, enterprise management  system,news  portal website, Web development company in Nepal','We are competent with extensive experience on E-commerce, Job-portal, News-portal, Enterprise management system and many more with standards- based markup code for the creation of powerful, effective and engaging websites.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(10,'Web Designing','web-designing','service','https://www.karkalo.com/web-designing','Web Designing  company in Nepal, website creation, improve website  graphics,','We focus on making our work simple yet ingenious which is aesthetically appealing, highly responsive and functional with cohesive web designs to match your unique personality.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(11,'Mobile Application','mobile-application','service','https://www.karkalo.com/mobile-application','Mobile Application development in Nepal,mobile application company in Kathmandu, develop mobile application,','Reach your customers through mobile phones. Karkalo Tech will provide you with felicitous Mobile Application to meet your application requirements.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(12,'Web Hosting','web-hosting','service','https://www.karkalo.com/web-hosting','Web Hosting in Nepal,domain register in Nepal,how to host website,','Karkalo gives dependable, secured and best hosting services in Nepal at affordable price. Contact us for  superlative Web Hosting services.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(13,'Domain Register','domain-register','service','https://www.karkalo.com/domain-register','Domain Register in nepal, how to register domain,','Karkalo Tech provide domain services with traditional domains such as .com .edu .org .net .mil .gov  .int and new domain names along with geographical, second level domain and sub domains.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(14,'Search Engine Optimization(SEO)','search-engine-optimizationseo','service','https://www.karkalo.com/search-engine-optimizationseo','Seo services  in Kathmandu, search engine optimization services in Nepal, increase my website page rank, improve SEO,  increase google page rank','We are fully fledged to improve and increase  your website page ranking with felicitous Search Engine Optimization(SEO) as per your needs and targeted areas.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(15,'Business Card Printing','business-card-printing','service','https://www.karkalo.com/business-card-printing','Business Card Design,Business Card Printing','Business cards printing services on high-quality paper at karkalo.com. Now Print 500 Business cards in 500 Rs only. Contact us for Business cards printing.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(16,'Logo Design','logo-design','service','https://www.karkalo.com/logo-design','Logo Design,Logo Design in Nepal,custom logo design services in nepal,business logo design in Nepal,cheap and best logo design services in Nepal','We are catering logo design services form small to big organizations and companies in Nepal. Consideration upon your branding needs our team will offer tailored logo designs as well as enhance your custom needs.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(17,'T-shirt Printing','t-shirt-printing','service','https://www.karkalo.com/t-shirt-printing','Affordable T-shirt Printing, Best T-shirt Printing in Nepal, Best font for T-shirt Printing, Cheap T-shirt Printing','Our operation quality custom T-shirt printing in Nepal. Mass garments printing Nepal pros. Exciting client benefit.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(18,'Brochure Printing & Flyers','brochure-printing-flyers','service','https://www.karkalo.com/brochure-printing-flyers','Flyer and Brochure services in Nepal,Brochure and Flyers maker,Brochure and Flyers Printing,Brochure and Flyers in Budget,Customized Brochure and Flyers Design,','Affordable  Flyers and Brochure Services in Nepal.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(19,'ID Card Printing','id-card-printing','service','https://www.karkalo.com/id-card-printing','ID Card Printing, ID Card Printing in Nepal, Affordable ID Card Printing, Custom ID Card Printing, Business ID Card Printing, Company ID Card Printing','Our ID card printing administration is quick and practical. Karkalo presents its national ID card printing arrangement.Outsource your association\'s plastic card printing with IdentiSys. Get in touch with us.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(20,'Interior Design and Decor','interior-design-and-decor','service','https://www.karkalo.com/interior-design-and-decor','Interior Design and Decor in Nepal,best interior design in Nepal,best interior designs in Nepal,Interior Design and Decor','Interior Design and Decor is the art of  upgrading the inside of a workspace or a building to create more pleasing condition for the people using the space incorporating space planning, site inspections,and execution of designs.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(21,'LED Scrolling Board','led-scrolling-board','service','https://www.karkalo.com/led-scrolling-board','LED Scrolling Board in Nepal,','LED Scrolling Board is used to display moving message using LED that comes in various shapes and size,colors and fonts. Karkalo tech provides services at very reasonable cost for digital advertisement.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(22,'LED Video Screen','led-video-screen','service','https://www.karkalo.com/led-video-screen','LED Video Screen in Nepal, LED advertisement board in Nepal,video scrolling board in Nepal','The concept for digital advertisement in LED Video Screen is increasing progressively in Nepal. We provide Video screen with best quality that can be viewed from any angle .',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(23,'Acrylic Letter Board','acrylic-letter-board','service','https://www.karkalo.com/acrylic-letter-board','Acrylic Letter Board in Nepal,stylish letter for advertisement in nepal,designs for company name,','Karkalo provides Acrylic Letter Board with multiple colours and fonts with premium quality.You can also provide detailed specifications by visiting us in Kathmandu,Nepal.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(24,'Metal Letter Board','metal-letter-board','service','https://www.karkalo.com/metal-letter-board','Metal Letter Board in Nepal,metal letter in nepal,metal letter for advertising,metal letters for company name,stylish letter for organization name','We understand your advertisement needs of your company. Karkalo provides Metal Letter Board with sleek and eye catching finish with alluring designs.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(25,'Slim Light Box','slim-light-box','service','https://www.karkalo.com/slim-light-box','Slim Light Box in Nepal,light box in Nepal,advertisement light box in Kathmandu','We offer Slim Light Box with wide range of dimensions for light box for digital advertisement in Nepal.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(26,'Light Box and Flex Board','light-box-and-flex-board','service','https://www.karkalo.com/light-box-and-flex-board','Light and Flex Board','We offer Light Box and flex board for advertisement with wide range of dimensions that are durable and do not wear off in harsh environmental conditions of Nepal with attention grabbing output at standard market rates.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(27,'Job Portal Sites - Applyjob','job-portal-sites-applyjob','portfolio','https://www.karkalo.com/job-portal-sites-applyjob','Job Portal Sites - Applyjob.com.np','Job Portal Sites - Applyjob.com.np',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(28,'Cementry Management System - (Western Charity Foundation)','cementry-management-system-western-charity-foundation','portfolio','https://www.karkalo.com/cementry-management-system-western-charity-foundation','Cementry Management System - (Western Charity Foundation)','Cementry Management System - (Western Charity Foundation) \r\nhttp://www.westerncharitablefoundation.com/',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(29,'Introduction','introduction','page','https://www.karkalo.com/introduction',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(30,'Advertising & Marketing','advertising-marketing','category','https://www.karkalo.com/advertising-marketing','Digital Advertisement','Karkalo Pvt. Ltd. Limited is one of the leading companies among Creates new promotional ideas, designs, print, radio, television, and internet advertisements, book advertisement space and time, provide other such services that help a client',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(31,'Information Technology','information-technology','category','https://www.karkalo.com/information-technology','Information Technology Web Technology in Nepal. Web Development in Nepal, E-commerce sites in Nepal, website in nepal.','We offer Web and Technology services in Nepal. Web Technology in Nepal. Web Development in Nepal, E-commerce sites in Nepal, website in nepal.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(32,'Designing and Printing','designing-and-printing','category','https://www.karkalo.com/designing-and-printing','printing','Karkalo Tech provides all kinds of   Graphic design and printing services for every business needs.  We also offer business cards, letterhead, Company Profiles, Logo design, T-shirt print,  Cup Print in Nepal.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(33,'Customers Reviews In Testimonials Services','customers-reviews-in-testimonials-services','post','https://www.karkalo.com/customers-reviews-in-testimonials-services',NULL,'I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happ',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(34,'Short natural product reviews.','short-natural-product-reviews','post','https://www.karkalo.com/short-natural-product-reviews',NULL,'I will create a fast video review of your product or company. \r\nThis is a very natural looking review. It is NOT slick and sales.\r\nI will need access to your product.',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(35,'Outstanding Experience!','outstanding-experience','post','https://www.karkalo.com/outstanding-experience',NULL,'The seller did a real and honest review of our product, as promised. She did it in the most timely way (note that we had to send her product and give her time to use it). The review is authentic. I recommend this gig!',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(36,'Need and Importance of Digital Marketing these days','need-and-importance-of-digital-marketing-these-days','post','https://www.karkalo.com/need-and-importance-of-digital-marketing-these-days','significance of digital advertising nowadays, importance of search engine marketing in digital advertising and marketing, Why Digital Marketing is essence and importance,','With the unfurling of current advancements and prevalence of Digital Marketing, Organizations are taking their initiations to take advantage of Digital marketing and advertisement......',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(37,'General Start Up Guide to Establish a Business','general-start-up-guide-to-establish-a-business','post','https://www.karkalo.com/general-start-up-guide-to-establish-a-business','How to begin and establish a business,business start-up guide, business starting process,begin a business effectively, guide to establish a company, start up a company','Here is the general overview of the steps to be followed while establishing business, companies and organizations. The appropriate approach to proceed should be slow and steady with heedful stages orderly to lead to fruitful private company',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(38,'Test-10','test-10','gallery','https://www.karkalo.com/test-10',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(39,'35151356_1873951802626562_3791355279300362240_n (1).png','35151356-1873951802626562-3791355279300362240-n-1png','gallery','https://www.karkalo.com/35151356-1873951802626562-3791355279300362240-n-1png',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(40,'acrylic tools','acrylic-tools','gallery','https://www.karkalo.com/acrylic-tools',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(41,'Karkalo start seo','karkalo-start-seo','gallery','https://www.karkalo.com/karkalo-start-seo','Karkalo start seo','Karkalo start seo',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(42,'Connect your business to digital','connect-your-business-to-digital','gallery','https://www.karkalo.com/connect-your-business-to-digital','Web pages','Web pages',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(43,'karkalo-google.jpg','karkalo-googlejpg','gallery','https://www.karkalo.com/karkalo-googlejpg',NULL,NULL,NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(44,'Printing and Designing','printing-and-designing','gallery','https://www.karkalo.com/printing-and-designing','Printing and Designing','Printing and Designing',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(45,'Acrylic 3D Letter','acrylic-3d-letter','gallery','https://www.karkalo.com/acrylic-3d-letter','Acrylic 3D Letter','Acrylic 3D Letter',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25'),(46,'Led video screen board in nepal','led-video-screen-board-in-nepal','gallery','https://www.karkalo.com/led-video-screen-board-in-nepal','Led scrolling board in nepal, Led screen board','Led scrolling board in nepal, Led screen board',NULL,1,'2018-08-14 01:18:25','2018-08-14 01:18:25');
/*!40000 ALTER TABLE `seos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'company_name','Karkalo Tech Pvt. Ltd.','Company Name','text','2018-06-11 16:21:22','2018-07-31 11:43:39'),(2,'email','info@karkalo.com','Email Address','text','2018-06-11 16:21:22','2018-07-31 11:43:39'),(3,'phone','+977-9811960012','Phone Number','text','2018-06-11 16:21:22','2018-08-09 15:41:47'),(4,'mobile','+977-9847502170','Mobile Number','text','2018-06-11 16:21:22','2018-07-31 11:43:39'),(5,'address','Shankhamul Road, Kathmandu','Address','text','2018-06-11 16:21:22','2018-08-09 16:20:50'),(6,'facebook','https://facebook.com/karkalopvtltd','Facebook','text','2018-06-11 16:21:22','2018-07-31 11:43:39'),(7,'twitter','https://twitter.com/karkalo','Twitter','text','2018-06-11 16:21:22','2018-07-31 11:43:39'),(8,'google','https://plus.google.com/user/karkalo','Google Plus','text','2018-06-11 16:21:22','2018-07-31 11:43:39'),(9,'linkedin','https://linkedin.com/karkalo','Linkedin','text','2018-06-11 16:21:22','2018-07-31 11:43:39'),(10,'about','Karkalo Tech is the professional organization providing high-end solutions and services in the domains of business.','About','text','2018-06-11 16:21:22','2018-07-31 11:44:13'),(11,'working_hour','Mon- Fri: 10:00 AM - 6:00 PM','Working Hour','text','2018-06-11 16:21:22','2018-06-11 16:21:22'),(12,'longitude','85.3345075','Map Longitude','text','2018-06-11 16:21:22','2018-08-09 15:41:47'),(13,'latitude','27.6855997','Map Laltitude','text','2018-06-11 16:21:22','2018-08-09 15:41:47'),(14,'copy_right','copyright @2016, <a href=\"https://karkalo.com\">Karkalo Pvt. Ltd.</a>. All Rights Reserved.','Copy Right','text','2018-06-11 16:21:22','2018-07-31 11:43:39');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribes`
--

DROP TABLE IF EXISTS `subscribes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribes`
--

LOCK TABLES `subscribes` WRITE;
/*!40000 ALTER TABLE `subscribes` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Bikash Silwal','Project Manager','bikash-silwal','teams/bikash-silwal.jpeg','bikash@karkalo.com','9847502170','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci sed quia non numquam modi tempora eius.','https://facebook.com/bsilwal2','https://twitter.com/xilwal','https://plus.google.com/xilwal','https://linkedin.com/xilwal',1,'2018-06-22 14:17:36','2018-06-22 14:17:36'),(2,'Raj Bhatta','Frontend Developer','raj-bhatta','teams/raj-bhatta.png','raj@karkalo.com','98502354646','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci sed quia non numquam modi tempora eius.',NULL,NULL,NULL,NULL,1,'2018-06-22 14:22:13','2018-06-22 14:22:13'),(3,'Kosish Kandel','Sales Manager','kosish-kandel','teams/kosish-kandel.jpg','kocs2kocs@karkalo.com','784623256623','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci sed quia non numquam modi tempora eius.',NULL,NULL,NULL,NULL,1,'2018-06-22 14:37:03','2018-06-22 14:37:03');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial` text COLLATE utf8mb4_unicode_ci,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'Bikash Silwal','Web Developer','Karkalo Tech','I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happier. Will definitely be coming back for more in the future.','testimonial/bikash-silwal.jpeg',1,'2018-06-22 09:50:38','2018-06-22 09:50:38'),(2,'Raj Bhatta','CEO','Bhatta and Co.','I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happier. Will definitely be coming back for more in the future.','testimonial/raj-bhatta.png',1,'2018-06-22 09:52:58','2018-06-22 09:52:58');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Tim Xilwal','dev.xilwal@gmail.com','$2y$10$dpFXJtwb2FisQsWfJ3noL.8izUOPh1LYbhpWcsjfe08j/om2bPi.6',1,1,'kxhqt3kuGKInCMRj6fxRJhvjmNL1hz0LsueLw6XXiTtOwkX1anrwlL3fr2Q1','2018-06-11 16:21:21','2018-06-11 16:21:21'),(2,'Sandesh Paudel','sanzciz@outlook.com','$2y$10$pwQ4DunGo5JyUvO7UKpbT.QV/54fk0ITQm0.jhch/RIRjCooq18qe',1,1,NULL,'2018-06-27 09:32:02','2018-07-31 09:01:04'),(3,'Bibek Adhakari','bibek.adhikari007@gmail.com','$2y$10$IVoo7Yt.EKVCyigB44i6KOzP/re3NwhjTh41Ga0lK8WVuBD/8VEXi',1,1,'4CCuBNG49qnqKhwHj9nbBlP4ALogDOv7bP0exUz1FaQ8GkAvwRu5yujChcss','2018-06-29 09:18:29','2018-07-31 08:56:33'),(4,'Parbat Thap','thapaparbat9@gmail.com','$2y$10$jZ47oxBW3wu9mUtYUgFXxe7JAmo08ISnkMkPDz5S85dTHHaymBgP2',1,1,NULL,'2018-08-09 16:26:27','2018-08-09 16:26:27');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-13 11:33:30
