$(document).ready( function () {
    $(".filesUpload").change(function() {
        var file = $('.filesUpload');
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.preview-upload-image').show();
                $('.preview-upload-image').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').show();
            $('#imagePreview').attr('src', e.target.result);
            // $('#imageUpload').hide();
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function newImageItem() {
    var newImg = $('.img-repeter').last().clone();
    newImg.find('input').val('');
    newImg.find('.preview-upload-image').attr('src', '');
    newImg.appendTo('.appendImg');
}


$('.upload-new-file').on('change', function() {
    $('.file-upload-button').click();
    $('.upload-new-file').val('');
       
});

$('.select-gallery-button').on('click', function() {

    var val = [];
    $(':checkbox:checked').each(function(i){
        val[i] = $(this).val();

    }); 
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        async: true,
        success: function (data) {
            //alert(data)
            $('.error-show' ).hide();
            $('#newImage').append("<div class='col-md-3'><div class='edit-profile-photo'><img src='/nebulatravels.com/public/uploads/"+ 
                data.image +"' alt='Preview Image'/><div class='checkboxes in-row margin-bottom-20'><input type='checkbox' name='gallery_ids[]' id='"+
                data.id+"' value='"+data.id+"'><label for='"+data.id+"'>"+data.title+"</label></div></div></div>");  
        },
        error: function(data){
            if(data.status === 422) {
                var errors = data.responseJSON;
                errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>';
                $.each( errors , function( key, value ) 
                {
                    errorsHtml += '<p>' + value[0] + '</p>' ; 
                    //showing only the first error.
                });
                 errorsHtml += '</div>';
                    $('#sign-in-dialog').modal('show');
                    $( '.error-show' ).html( errorsHtml );
            }
        },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
});

$( document ).ajaxStart(function() {
     $( ".loading" ).show();
});

$( document ).ajaxStop(function() {
    $( ".loading" ).hide();
});

$(".upload-file-form").submit(function() {
    
    $( '.error-show').hide();
    var url = $(this).data('url');
    //alert(url)
    var formData = new FormData($(this)[0]);
    $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            async: true,
            success: function (data) {
                //alert(data)
                $('.error-show' ).hide();
                $('#newImage').append("<div class='col-md-4'><div class='edit-profile-photo'><img src="+ 
                    data.image +" alt='Preview Image' width=150 height=150/><div class='checkboxes in-row margin-bottom-20'><input type='checkbox' name='gallery_ids[]' id='"+
                    data.id+"' value='"+data.id+"'><label for='"+data.id+"'>"+data.title+"</label></div></div></div>");  
            },
            error: function(data){
               if(data.status === 422) {
                    var errors = data.responseJSON;
                    errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>';
                    $.each( errors , function( key, value ) 
                    {
                        errorsHtml += '<p>' + value[0] + '</p>' ; 
                        //showing only the first error.
                    });
                     errorsHtml += '</div>';
                        $( '.error-show' ).html( errorsHtml );
                }
            },
                cache: false,
                contentType: false,
                processData: false
            });
    return false;
});

$('.url-upload-button').click(function() {
    var image_url = $('input[name=image_url]');
    var data = 'image_url=' + image_url.val();
    var url = $(this).data('url');
    $.ajax({
        type: 'POST',
        url : url,
        data : data,
        async: true,
        success: function (data)
        {
            $('.error-show' ).hide();
             $('#newImage').append("<div class='col-md-4'><div class='edit-profile-photo'><img src="+ 
                data.image +" alt='Preview Image'  width=150 height=150/><div class='checkboxes in-row margin-bottom-20'><input type='checkbox' name='gallery_ids[]' id='"+
                data.id+"' value='"+data.id+"'><label for='"+data.id+"'>"+data.title+"</label></div></div></div>");  
        },
        error: function(data){
           if(data.status === 422) {
                var errors = data.responseJSON;
                errorsHtml = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>';
                $.each( errors , function( key, value ) 
                {
                    errorsHtml += '<p>' + value[0] + '</p>' ; 
                    //showing only the first error.
                });
                 errorsHtml += '</div>';
                   
                    $( '.error-show' ).html( errorsHtml );
            }
        },
        cache: false,
        contentType: false,
        processData: false
        });
    return false;
});


