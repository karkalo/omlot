-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: karkalo
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advertisements`
--

DROP TABLE IF EXISTS `advertisements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `click` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisements`
--

LOCK TABLES `advertisements` WRITE;
/*!40000 ALTER TABLE `advertisements` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertisements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Display Advertising','display-advertising','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Karkalo Pvt. Ltd. Limited is one of the leading companies among Creates new promotional ideas, designs, print, radio, television, and internet advertisements, book advertisement space and time, provide other such services that help a client in entering and succeeding in a chosen market. The company utilizes the latest technology blended with a mechanism, software, and electronics to design an exciting and excellent range of electronic display systems and electronic display boards for various commercial requirements. Our range of LED signs and display boards including a token display system, status display board, 3D acrylic led letters and metal letters are high in performance and quality and serve an excellent media for indoor and outdoor advertising. Attractive in looks, these offer long visibility, easily communicate with customers and help businesses such as shops, offices and other commercial entities to attract customers and generate more business. Available at competitive prices, our display boards can also be effectively used for applications such as road signals, security equipment, and information display systems as in foggy and smoked conditions; these have good visibility from long distances.</p>\r\n</body>\r\n</html>','categories/display-advertising.png','Advertisement Technology','Karkalo Pvt. Ltd. Limited is one of the leading companies among Creates new promotional ideas, designs, print, radio, television, and internet advertisements, book advertisement space and time, provide other such services that help a client',1,1,1,'2018-06-12 07:25:21','2018-08-06 08:34:25'),(2,'Web Technology','web-technology','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #555555; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">We offer Web and Technology services in Nepal.</span></p>\r\n</body>\r\n</html>','categories/web-technology.png',NULL,'We offer Web and Technology services in Nepal.',1,1,1,'2018-06-22 05:59:06','2018-08-06 08:41:03'),(3,'Graphic Design and Printing','graphic-design-and-printing','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">Karkalo Tech provides&nbsp;all kinds of &nbsp; Graphic design and printing services for every business needs.&nbsp;</span><span style=\"color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">&nbsp;We also offer</span><span style=\"box-sizing: border-box; font-weight: bold; color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">&nbsp;business cards, letterhead, Company Profiles, Logo design, T-shirt print,&nbsp; Cup Print, Magazine, Books, Prospectus, Brochures and Flyers, Bulletins, Restaurant Menu, Invitation Cards, Visiting Cards, Certificates, Wedding Cards, PVC ID Cards, envelopes&nbsp; &amp; Pree-Ink Stamp and self-ink Stamps in Nepal.</span><span style=\"color: #454545; font-family: Lato, sans-serif; font-size: 15px;\">&nbsp;We maintain relationships with companies that produce continuous forms, foil stamping, and embossing, die to cut and many other services.</span></p>\r\n</body>\r\n</html>','categories/graphic-design-and-printing.png','printing','Karkalo Tech provides all kinds of   Graphic design and printing services for every business needs.  We also offer business cards, letterhead, Company Profiles, Logo design, T-shirt print,  Cup Print in Nepal.',1,1,1,'2018-06-22 05:59:20','2018-08-05 09:34:24');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,NULL,NULL,'asdfg','admin@gmail.com','+977-01-4786970','asadfghhjm','127.0.0.1','comment/2018-08-06 10:25.jpg',NULL,0,0,'2018-08-06 16:40:09','2018-08-06 16:40:09',21);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `is_response` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_slider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_featured` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Test-10','test-10','gallery/test.png','http://localhost:8000','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>',NULL,NULL,'0','1','0','2018-06-12 07:26:00','2018-06-12 07:26:01'),(2,'35151356_1873951802626562_3791355279300362240_n (1).png','35151356-1873951802626562-3791355279300362240-n-1png','gallery/35151356-1873951802626562-3791355279300362240-n-1png.png',NULL,NULL,NULL,NULL,'0','0','0','2018-06-12 08:49:50','2018-06-12 08:49:50'),(3,'35151356_1873951802626562_3791355279300362240_n (1).png','35151356-1873951802626562-3791355279300362240-n-1png','gallery/35151356-1873951802626562-3791355279300362240-n-1png.png',NULL,NULL,NULL,NULL,'0','0','0','2018-06-12 08:50:38','2018-06-12 08:50:38'),(4,'35043542_2086792994928510_1027257234041602048_n.jpg','35043542-2086792994928510-1027257234041602048-njpg','gallery/35043542-2086792994928510-1027257234041602048-njpg.jpg',NULL,NULL,NULL,NULL,'0','1','0','2018-06-12 08:52:56','2018-06-12 08:52:56'),(5,'35151356_1873951802626562_3791355279300362240_n.png','35151356-1873951802626562-3791355279300362240-npng','gallery/35151356-1873951802626562-3791355279300362240-npng.png',NULL,NULL,NULL,NULL,'0','1','0','2018-06-12 08:53:45','2018-06-12 08:53:45'),(6,'karkalo-google.jpg','karkalo-googlejpg','gallery/karkalo-googlejpg.jpg',NULL,NULL,NULL,NULL,'0','1','0','2018-06-12 08:54:14','2018-06-12 08:54:14'),(7,'Graphic Designing and Printings','graphic-designing-and-printings','gallery/graphic-designing-and-printings.jpg','http://localhost:8000','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Graphic Design and Printing</p>\r\n</body>\r\n</html>','Graphic Design and Printing','Graphic Design and Printing','1','1','1','2018-06-18 12:03:56','2018-08-06 08:23:11'),(8,'Web technology for you','web-technology-for-you','gallery/web-technology-for-you.jpg','http://localhost:8000','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Web and Technology</p>\r\n</body>\r\n</html>','Web and Technology','Web and Technology','1','1','1','2018-06-18 12:05:47','2018-08-06 08:22:41'),(9,'Display your brand with 3D LED Letter.','display-your-brand-with-3d-led-letter','gallery/display-your-brand-with-3d-led-letter.png','http://localhost:8000','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Our product range includes Advertising Letters such as Solid Acrylic Letter, Acrylic Raised Letters, Acrylic Cutting Letter, Stainless Steel Letter, Acrylic Letter, Indoor Reception Acrylic Letter.</p>\r\n</body>\r\n</html>','Acrylic Board','Our product range includes Advertising Letters such as Solid Acrylic Letter, Acrylic Raised Letters, Acrylic Cutting Letter, Stainless Steel Letter, Acrylic Letter, Indoor Reception Acrylic Letter.','1','1','1','2018-06-18 12:06:12','2018-08-06 08:22:17');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Header','http://localhost:8000',NULL,NULL,1,'2018-06-25 06:14:55','2018-06-25 07:51:53'),(2,'Home','http://localhost:8000',2,1,1,'2018-06-25 06:15:15','2018-06-25 06:22:48'),(3,'About Us','http://localhost:8000/about',2,1,1,'2018-06-25 06:22:25','2018-06-25 06:23:08'),(4,'Services','https://localhost:8000/service',3,1,1,'2018-06-25 06:23:08','2018-06-25 06:31:35'),(5,'Web and Technology','http://localhost:8000/web-development',2,4,1,'2018-06-25 06:25:55','2018-06-25 06:29:36'),(6,'Graphic Design and Printing','http://localhost:8000/graphic-design-and-printing',2,4,1,'2018-06-25 06:26:09','2018-06-25 06:30:59'),(7,'Digital Advertisement','http://localhost:8000/digital-advertisement',1,4,1,'2018-06-25 06:30:59','2018-06-25 06:30:59'),(8,'Portfolio','http://localhost:8000/portfolio',4,1,1,'2018-06-25 06:31:35','2018-06-25 07:50:35'),(9,'Contact Us','https://localhost:8000/contact',1,1,1,'2018-06-25 07:51:01','2018-06-25 07:51:01'),(10,'Footer','http://localhost:8000',1,NULL,1,'2018-06-25 07:51:53','2018-06-25 07:51:53'),(11,'Quick Link','http://localhost:8000',1,10,1,'2018-06-25 07:52:21','2018-06-25 07:52:21'),(12,'Home','http://localhost:8000',2,11,1,'2018-06-25 07:52:40','2018-06-25 07:53:30'),(13,'About','http://localhost:8000/about',3,11,1,'2018-06-25 07:52:59','2018-06-25 07:54:23'),(14,'Portfolio','http://localhost:8000/portfolio',4,11,1,'2018-06-25 07:53:30','2018-06-25 07:54:47'),(15,'Privacy Policy','http://localhost:8000/privacy-policy',1,11,1,'2018-06-25 07:54:23','2018-06-25 07:54:23'),(16,'Terms & Conditions','http://localhost:8000/terms-and-conditions',1,11,1,'2018-06-25 07:54:47','2018-06-25 07:54:47');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metas`
--

DROP TABLE IF EXISTS `metas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metas`
--

LOCK TABLES `metas` WRITE;
/*!40000 ALTER TABLE `metas` DISABLE KEYS */;
/*!40000 ALTER TABLE `metas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_06_13_060835_create_permissions_table',1),(4,'2017_06_13_060857_create_roles_table',1),(5,'2017_06_13_061359_create_permission_role_table',1),(6,'2017_07_31_101302_create_testimonials_table',1),(7,'2017_09_04_130628_create_faqs_table',1),(8,'2017_09_10_115427_create_partners_table',1),(9,'2017_09_23_001946_create_categories_table',1),(10,'2017_09_23_002424_create_pages_table',1),(11,'2017_09_23_003309_create_metas_table',1),(12,'2017_09_23_004611_create_contacts_table',1),(13,'2017_09_23_005728_create_teams_table',1),(14,'2017_09_23_131130_create_products_table',1),(15,'2017_09_23_132058_create_galleries_table',1),(16,'2017_09_23_133244_create_comments_table',1),(17,'2017_09_23_135222_create_settings_table',1),(18,'2017_09_23_135351_create_subscribes_table',1),(19,'2017_11_07_112459_create_seos_table',1),(20,'2017_11_22_044306_create_posts_table',1),(21,'2017_11_25_144241_create_menus_table',1),(22,'2017_12_16_184732_create_advertisements_table',1),(23,'2017_06_13_061359_create_product_gallery_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'page',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'About','about','about','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; font-family: MyriadPro; font-size: 16px; padding-top: 15px; padding-bottom: 20px; color: #333333;\">Karkalo Tech is the professional organization providing high-end solutions and services in the domains of business. Karkalo Tech&nbsp;is founded and led by a group of young professionals, with a long-time vision and a very high level of commitment to the client&rsquo;s satisfaction. Karkalo Tech&nbsp;aims to be a world leader in the field of technology systems integration and related business. We are committed to excellence in quality, cost effectiveness, and customer satisfaction.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; font-family: MyriadPro; font-size: 16px; padding-top: 15px; padding-bottom: 20px; color: #333333;\">We unquenchable partial for quality has led us to get prestigious clients and keeping our growth at a very high rate. We offering total solutions in Web and Information Technology, Graphics Design &amp; Printing Services, and Digital Advertisements fields. With its expertise in solutions and services, Karkalo offers value-added services in key areas of Systems and Services.</p>\r\n</body>\r\n</html>','pages/about.png',NULL,'web and technology in Nepal, graphics designing in Nepal, all kinds of printing services, digital advertisements such as led display board, led video screen, 3d letter, acrylic raising baord','We provide web and technology in Nepal, graphics designing in Nepal, all kinds of printing services, digital advertisements such as led display board, led video screen, 3d letter, acrylic raising board in Kathmandu, Nepal.',NULL,1,1,1,'2018-06-11 12:21:23','2018-07-31 06:54:42'),(2,'Our Vision','our-vision','about','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #333333; font-family: MyriadPro; font-size: 16px; text-align: justify;\">&nbsp;At Karkalo, we aim to provide innovative product solutions and know that our customers place their trust in us. We in turn make a commitment to uphold that trust and cater to all the creative needs of our clients. Our values of innovation and integrity are underpinned by our belief that customers always come first. As a company, we create an ambience that is impressive and inspiring to all and take pride in our expertise, capability, reliability and quality of work. The objective of management is to provide these services in a manner which conforms to contractual and regulatory requirements. This unquenchable thirst for quality has led us to getting prestigious clients and keeping our growth at a very high rate.&nbsp;</span></p>\r\n</body>\r\n</html>','pages/our-vision.png',NULL,NULL,NULL,NULL,1,0,0,'2018-06-22 09:53:34','2018-08-06 08:27:16'),(3,'Our Mission','our-mission','about','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #333333; font-family: MyriadPro; font-size: 16px; text-align: justify;\">Karkalo provides you with a deep partnership and commitment you need to realize your business and technology goals. It starts with making your goals our goals and ends with an inventive and flexible solution for your most pressing challenges. At Karkalo every project is important, and this attitude is adhered to at all levels in the company. As a client, you will appreciate the dedication of effort that goes into making your dream project a reality.</span></p>\r\n</body>\r\n</html>','pages/our-mission.png',NULL,NULL,'Karkalo provides you with a deep partnership and commitment you need to realize your business and technology goals.',NULL,1,0,0,'2018-06-22 09:58:08','2018-07-31 07:03:26'),(4,'Privacy Policy','privacy-policy','page','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Privacy Policy</p>\r\n</body>\r\n</html>','pages/privacy-policy.jpeg',NULL,'Privacy Policy','Privacy Policy',NULL,1,1,1,'2018-06-25 07:53:48','2018-06-29 03:18:27'),(5,'Terms and conditions','terms-and-conditions','page','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Terms and conditions</p>\r\n</body>\r\n</html>','pages/terms-and-conditions.jpeg',NULL,'Terms and conditions','Terms and conditions',NULL,1,1,1,'2018-06-25 07:53:58','2018-06-29 03:18:09'),(6,'Web Development','web-development','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Karkalo Tech developes responsive Standard websites enhancing your digital experience,we are dedicated to create powerful, effective and engaging websites able to serve your best purpose.We use ultra-clean and standards-based markup code to produce websites with prodigious search engine results.</p>\r\n<p>We are competent with extensive experience on E-commerce, Job-protal, News-portal, Enterprise management system and many more.Professional website creation is our job while giving you the edge on online platform is our responsibility.Based on discovery sessions and regular discussions with your team,&nbsp;our expertise advocate best technology for your website to satisfy your needs ensuring your web presense as well as guide you through the process of&nbsp; enhancing the administrative interface. For front-end technology we use HTML,JavaScript and CSS along with JavaScript Frameworks such as VueJS, ReactJS, NodeJS and AngularJS to&nbsp;simplify and provide more agility and for back-end technology we use PHP, Python,Ruby, and Java.</p>\r\n</body>\r\n</html>','pages/web-development.png',NULL,'Web Development in Nepal,create website,website services in Kathmandu, job-portal site,e-commerce site, enterprise management  system,news  portal website, Web development company in Nepal','We are competent with extensive experience on E-commerce, Job-portal, News-portal, Enterprise management system and many more with standards- based markup code for the creation of powerful, effective and engaging websites.',2,1,1,1,'2018-06-29 03:12:43','2018-07-31 06:50:45'),(7,'Web Designing','web-designing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Are you searching for Best Web Design and Development in Nepal?&nbsp;</p>\r\n<p>The prior objective of web designing is transforming guests into customers by developing&nbsp; feelings of delight, trust,and confidence while they navigate through your website.Effective Website Design is concerned abouthe visualization of an organization and core of&nbsp; web based advertising activities.We put balanced&nbsp;approach to content,design and development to our website which are completely Search Engine Optimized.So why not give us a chance to make your design consolidate your goals.</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>','pages/web-designing.png',NULL,'Web Designing  company in Nepal, website creation, improve website  graphics,','We focus on making our work simple yet ingenious which is aesthetically appealing, highly responsive and functional with cohesive web designs to match your unique personality.',2,1,1,1,'2018-06-29 03:13:41','2018-07-31 07:40:43'),(8,'Mobile Application','mobile-application','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>The improvement approach for web development in Nepal is Mobile Application yet it needs more incessant correspondence with the customer.Disclose to us your Application concepts and we create the best versatile applications possible with no more sensible expenses.</p>\r\n</body>\r\n</html>','pages/mobile-application.png',NULL,'Mobile Application development in Nepal,mobile application company in Kathmandu, develop mobile application,','Reach your customers through mobile phones. Karkalo Tech will provide you with felicitous Mobile Application to meet your application requirements.',2,1,1,1,'2018-06-29 03:14:40','2018-07-31 08:02:15'),(9,'Web Hosting','web-hosting','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>We are rendering web hosting services in Nepal with general support with user friendly control panel with&nbsp;the assistance of most recent web innovations. Karkalo gives dependable, secured and best hosting services&nbsp;in Nepal at least expenses. Our web hosting administrations in Nepal guarantees your sites are working fast with every one of your information secured with standard regular backups . We offer Linux and also windows web&nbsp;hosting services in Nepal .Get in touch with us for best web hosting services in Nepal .&nbsp;</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>','pages/web-hosting.png',NULL,'Web Hosting in Nepal,domain register in Nepal,how to host website,','Karkalo gives dependable, secured and best hosting services in Nepal at affordable price. Contact us for  superlative Web Hosting services.',2,1,1,1,'2018-06-29 03:16:14','2018-07-31 09:03:09'),(10,'Domain Register','domain-register','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>A domain name is the embodiment of your organization\'s online personality. It features the company\'s type and the mission. We offer .com, .net, .organization domain registration benefit in Nepal, where you can register&nbsp;your domain with us.Before choosing domain, picking proper accessible domain name that suits your business is a standout amongst the most vital things to consider. We provide guidelines to enroll domain name with .np extension as a cost-free exercise.</p>\r\n</body>\r\n</html>','pages/domain-register.jpg',NULL,'Domain Register in nepal, how to register domain,','Karkalo Tech provide domain services with traditional domains such as .com .edu .org .net .mil .gov  .int and new domain names along with geographical, second level domain and sub domains.',2,1,1,1,'2018-06-29 03:19:51','2018-07-31 09:28:07'),(11,'Search Engine Optimization(SEO)','search-engine-optimizationseo','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Search engine optimization is a set of rules and good practices. Seo can be followed by your website to optimize your websites for search engines and thus improve their search engine ranks. The majority of search engines users&nbsp; will probably tap on one of the best proposals in the first page, so to&nbsp;exploit this and pick up guests to your site or clients to your online store you have to in the best positions. Good SEO hones client experience and ease&nbsp;of use of a site.Search engines are the foundation of user trust and having your online presence on the top while user is looking for your product and services,&nbsp;increases the web site&rsquo;s trust.Given two web sites are selling the same product, the web site whose search engine is optimized is more likely to connect to&nbsp;more customers and make more deals online.</p>\r\n<p>We Karkalo Tech understand the mechanism of search, we ensure organic, natural and earned web results for your website high page ranking.You could be the biggest shop in the town, but if you don\'t show up in local searches online, you\'re missing out on a significant amount of clients and sales. We provide Global and National SEO, Ecommerce SEO, Enterprise SEO, Local SEO, Content Marketing, Off-site SEO, On-site SEO and Google Recovery&nbsp;Services. Remember us inorder to take advantages of felicitous&nbsp;SEO services in kathmandu as well as all over Nepal.</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>','pages/search-engine-optimizationseo.png',NULL,'Seo services  in Kathmandu, search engine optimization services in Nepal, increase my website page rank, improve SEO,  increase google page rank','We are fully fledged to improve and increase  your website page ranking with felicitous Search Engine Optimization(SEO) as per your needs and targeted areas.Reach out to Karkalo Tech, Shankhamul, Kathmandu, Nepal.',2,1,1,1,'2018-06-29 03:20:46','2018-08-06 08:46:47'),(12,'Business Card Printing','business-card-printing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h3 style=\"padding: 0px; margin: 14px 0px 0px; box-sizing: border-box; font-size: 18px; color: #333333; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: 300; line-height: 1.1;\">Give a Brand Recognition to Your Business</h3>\r\n<p style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-family: Roboto, sans-serif; color: #666666; line-height: 19px; text-align: justify;\">Business cards are the ignition toggle that initiates the conversation between your projections and your brand. This most basic yet effective tool for business use as well works ideally for exchanging contact information. If you are in seek of business card printing, printexperto.com is here to aid you from all perspectives. We offer many services for businesses or individuals. Our online content is specifically designed for our potential customers, so that ordering is quite fast, easy and affordable.</p>\r\n</body>\r\n</html>','pages/business-card-printing.jpg',NULL,'Business Card Design,Business Card Printing','Business cards printing services on high-quality paper at karkalo.com. Now Print 500 Business cards in 500 Rs only. Contact us for Business cards printing.',3,1,1,1,'2018-06-29 03:22:50','2018-07-04 13:39:04'),(13,'Logo Design','logo-design','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<div class=\"uk-margin-medium-bottom uk-container uk-container-small\" style=\"box-sizing: content-box; max-width: 900px; padding-left: 0px; padding-right: 0px; color: #696969; font-family: \'work sans\'; font-size: 16px; margin: 0px !important auto 40px !important auto;\">\r\n<p style=\"box-sizing: border-box; padding: 20px; margin: 0px 0px 1rem; color: #212529; font-family: Roboto, sans-serif;\">The logo is the branding of your company,&nbsp; we understand the aesthetics and focus on the message and impact of your logo to earmark your market requirements. We offer tailored-made, impressive, relevant, highly complex to simple, minimalist logo designs. After much research and conceptualizing, our branding group will make distinctive plan headings and ideas and present them to you in excellent marking introductions so you can perceive how the outlines connect in reality and help you imagine how each outline would help have the correct effect. Submit your requirements, review the innovative design concepts exclusively for you from our team and finalize your logo.</p>\r\n</div>\r\n</body>\r\n</html>','pages/logo-design.jpg',NULL,'Logo Design','We are catering logo design services form small to big organizations and companies in Nepal. Consideration upon your branding needs our team will offer tailored logo designs as well as enhance your custom needs.',3,1,1,1,'2018-06-29 03:24:36','2018-08-06 08:45:51'),(14,'T-shirt Printing','t-shirt-printing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h4 style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-size: 20px; margin: 0px 0px 30px; outline: 0px; padding: 0px; vertical-align: baseline; font-family: \'Roboto Condensed\', sans-serif; color: #4d4d4d; line-height: normal; text-transform: uppercase;\">&nbsp;</h4>\r\n<h4 style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-size: 20px; margin: 0px 0px 30px; outline: 0px; padding: 0px; vertical-align: baseline; font-family: \'Roboto Condensed\', sans-serif; color: #4d4d4d; line-height: normal; text-transform: uppercase;\">WE PRINT FOR:</h4>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Schools</strong>: Little Leagues, Soccer, La Cross, Basket Ball, Cheerleaders, Booster Clubs, Private Schools, Public Schools, Elementary, Middle School, High Schools, and Colleges.</p>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Cooperate &amp; Elections</strong>: Offices, Clerks, and Police Departments.</p>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Personal</strong>: Families, Individual, and Gifts.</p>\r\n<p style=\"box-sizing: border-box; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px 0px 10px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: 28px; color: #4d4d4d; font-family: Raleway, sans-serif;\"><strong style=\"box-sizing: border-box; background: 0px 0px; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">Events</strong>: Festivals, Family Reunions, Concerts, Birthdays, Holidays, and &nbsp;Cooperate Functions.</p>\r\n</body>\r\n</html>','pages/t-shirt-printing.jpg',NULL,'T-shirt Printing','T-shirt Printing',3,1,1,1,'2018-06-29 03:27:07','2018-07-04 13:48:57'),(15,'Brochure Printing & Flyers','brochure-printing-flyers','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>','pages/brochure-printing-flyers.jpg',NULL,'Flyer and Brochure','Flyer and Brochure',3,1,1,1,'2018-06-29 03:30:03','2018-07-04 13:50:45'),(16,'ID Card Printing','id-card-printing','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>ID Card Printing</p>\r\n</body>\r\n</html>','pages/id-card-printing.jpg',NULL,'ID Card Printing','ID Card Printing',3,1,1,1,'2018-06-29 03:34:52','2018-07-04 13:53:08'),(17,'Interior Design and Decor','interior-design-and-decor','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Interior Design and Decor</p>\r\n</body>\r\n</html>','pages/interior-design-and-decor.jpg',NULL,'Interior Design and Decor','Interior Design and Decor',3,1,1,1,'2018-06-29 03:38:58','2018-06-29 03:38:58'),(18,'LED Scrolling Board','led-scrolling-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>These displays are microprocessor-based Moving Message Display, made up of light emitting diode [LEDs] arranged in an array. The moving message or data can be fed &amp; set with the help of keyboard on the corded remote. The system can also be customized to remote operate through GPRS system, MMS, etc.</p>\r\n<p><strong>It can be of any color:-</strong></p>\r\n<ul>\r\n<li>Single color</li>\r\n<li>Tricolor</li>\r\n<li>Full-color</li>\r\n<li>Feature Led moving display boards</li>\r\n</ul>\r\n<p><strong>Product details </strong></p>\r\n<ul>\r\n<li>Display shape Square, Rectangle</li>\r\n<li>Display Size 12&rdquo;/6&rdquo;</li>\r\n<li>Lighting Color Red, Blue, Green, White, Single Colors</li>\r\n<li>Operating Voltage 5V DC</li>\r\n</ul>\r\n<p><strong>P10 Moving Led Message Sign&rsquo;s Key Feature</strong></p>\r\n<ul>\r\n<li>&nbsp;Available at different colors and size</li>\r\n<li>Available indoor and outdoor display</li>\r\n<li>Support Temperature sensor</li>\r\n<li>Long viewing distance and wide viewing angle</li>\r\n<li>Low power consumption and low maintenance cost</li>\r\n<li>Full language support</li>\r\n<li>Real-time clock and calendar</li>\r\n<li>Auto power on/off function</li>\r\n<li>Multiple fonts and moving effects</li>\r\n<li>Support at most 200 programs each program support 16 partitions simultaneous play each image-text 200 messages. (quantity in storage space under the premise of memory)</li>\r\n<li>Led moving sign can be widely used in shopping mall, retails store, Caf&eacute;, hotel, school, college, railway station, college, station, highway, building and other commercial sectors.</li>\r\n</ul>\r\n</body>\r\n</html>','pages/led-scrolling-board.jpg',NULL,'LED Scrolling Board in Nepal,','LED Scrolling Board is used to display moving message using LED that comes in various shapes and size,colors and fonts. Karkalo tech provides services at very reasonable cost for digital advertisement.',1,1,1,1,'2018-06-29 03:41:30','2018-08-06 08:25:15'),(19,'LED Video Screen','led-video-screen','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>LED Video Screen&nbsp; is increasing progressively&nbsp; because of its capacity to convey seamless pictures,versatility to any size or shape, and fantastic ideal qualities that influence LED video screen&nbsp; look&nbsp; extraordinary from any angle.We offer LED items in a few setups adaptable to your application. Select your choices beneath and see accessible models based on viewing distance upto 60 metres and pixel pitch upto 20mm.</p>\r\n<p>APPLICATIONS</p>\r\n<ul>\r\n<li>Indoor</li>\r\n<li>Outdoor</li>\r\n<li>Fixed&nbsp;Rental</li>\r\n<li>Staging</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>FEATURES</p>\r\n<ul>\r\n<li>Ultra Lightweight</li>\r\n<li>Redundant Video</li>\r\n<li>Ultra HD</li>\r\n<li>Low power</li>\r\n</ul>\r\n</body>\r\n</html>','pages/led-video-screen.jpg',NULL,'LED Video Screen in Nepal, LED advertisement board in Nepal,video scrolling board in Nepal','The concept for digital advertisement in LED Video Screen is increasing progressively in Nepal.We provide Video screen with best quality that can be viewed from any angle .',1,1,1,1,'2018-06-29 03:43:11','2018-07-31 06:45:09'),(20,'Acrylic Letter Board','acrylic-letter-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>We are offering a wide cluster of 3D LED Letter Board that is accessible in different examples to take care of the particular demand of customers. These items are produced utilizing quality material.&nbsp;</p>\r\n<p>Features:</p>\r\n<ul>\r\n<li>Smooth surface finish</li>\r\n<li>Unique designs</li>\r\n<li>Eye-catching appearance</li>\r\n<li>Easy to use</li>\r\n<li>High strength</li>\r\n<li>Lightweight</li>\r\n</ul>\r\n</body>\r\n</html>','pages/acrylic-letter-board.jpg',NULL,'Acrylic Letter Board in Nepal,stylish letter for advertisement in nepal,designs for company name,','Karkalo provides Acrylic Letter Board with multiple colours and fonts with premium quality.You can also provide detailed specifications by visiting us in Kathmandu,Nepal.',1,1,1,1,'2018-06-29 03:45:52','2018-07-31 06:13:22'),(21,'Metal Letter Board','metal-letter-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>By keeping our adjust with the advancement occurring in this industry domain, we have been immersed in presenting of Stainless Metal Letter Board . Particularly planned and built up, these letters are outlined by making utilization of optimum-class basic&nbsp; material alongside modernized methods couple with the market set standards and standards. Besides, these letters are possible at spending affordable rates.</p>\r\n<p>Features</p>\r\n<ul>\r\n<li>Fine finish</li>\r\n<li>Excellent strength</li>\r\n<li>Alluring designs</li>\r\n</ul>\r\n</body>\r\n</html>','pages/metal-letter-board.jpg',NULL,'Metal Letter Board in Nepal,metal letter in nepal,metal letter for advertising,metal letters for company name,stylish letter for organization name','We understand your advertisement needs of your company. Karkalo provides Metal Letter Board with sleek and eye catching finish with alluring designs.',1,1,1,1,'2018-06-29 03:47:03','2018-07-31 05:54:25'),(22,'Slim Light Box','slim-light-box','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>From the recent years, we are occupied with assembling and providing a tremendous collection of LED Slim light boxes. Available in varied details, these light boxes are created from quality endorsed raw materials that have been sources from trusted and legitimate sellers of the business. Every last thing offered by us is thoroughly checked and analyzed on set industry standards.</p>\r\n<p>Features:</p>\r\n<ul>\r\n<li>Modern designed</li>\r\n<li>Unique patterns</li>\r\n<li>Durable finish</li>\r\n<li>Full visibility</li>\r\n<li>Quick and easy assembly</li>\r\n</ul>\r\n</body>\r\n</html>','pages/slim-light-box.jpg',NULL,'Slim Light Box in Nepal,light box in Nepal,advertisement light box in Kathmandu','We offer Slim Light Box with wide range of dimensions for light box for digital advertisement in Nepal.',1,1,1,1,'2018-06-29 03:48:31','2018-07-31 05:32:45'),(23,'Light Box and Flex Board','light-box-and-flex-board','service','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>We are one of the reputed organization involve in providing Flex Print, Flex Board Services to our most reliable clients. These are available in different designs which are eye-catching and very beautiful. We are able to full fill the requirement of our customers. We provide these service at the most competitive price.</p>\r\n</body>\r\n</html>','pages/light-box-and-flex-board.jpeg',NULL,'Light and Flex Board','We offer Light Box and flex board for advertisement with wide range of dimensions that are durable and do not wear off in harsh environmental conditions of Nepal with attention grabbing output at standard market rates.',1,1,1,1,'2018-06-29 03:53:29','2018-07-31 07:28:04'),(24,'Job Portal Sites - Applyjob','job-portal-sites-applyjob','portfolio','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Job Portal Sites - Applyjob.com.np</p>\r\n</body>\r\n</html>','pages/job-portal-sites-applyjob.png',NULL,'Job Portal Sites - Applyjob.com.np','Job Portal Sites - Applyjob.com.np',2,1,0,0,'2018-06-29 04:09:20','2018-06-29 04:59:45'),(25,'Cementry Management System - (Western Charity Foundation)','cementry-management-system-western-charity-foundation','portfolio','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Cementry Management System - (Western Charity Foundation)</p>\r\n</body>\r\n</html>','pages/cementry-management-system-western-charity-foundation.jpeg',NULL,'Cementry Management System - (Western Charity Foundation)','Cementry Management System - (Western Charity Foundation) \r\nhttp://www.westerncharitablefoundation.com/',NULL,1,0,0,'2018-06-29 04:15:00','2018-06-29 04:56:32');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'Apply Job','http://localhost:8000','partner/apply-job.png',1,'2018-06-22 05:59:49','2018-06-22 05:59:49'),(2,'Acs','http://localhost:8000','partner/acs.jpg',1,'2018-06-22 06:00:28','2018-06-22 06:00:28'),(3,'Edy','http://localhost:8000','partner/edy.png',1,'2018-06-22 06:01:51','2018-06-22 06:01:51');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,NULL,NULL),(1,2,NULL,NULL),(1,3,NULL,NULL),(1,4,NULL,NULL),(1,5,NULL,NULL),(1,6,NULL,NULL),(1,7,NULL,NULL),(1,8,NULL,NULL),(1,9,NULL,NULL),(1,10,NULL,NULL),(1,11,NULL,NULL),(1,12,NULL,NULL),(1,13,NULL,NULL),(1,14,NULL,NULL),(1,15,NULL,NULL),(1,16,NULL,NULL),(1,17,NULL,NULL),(1,18,NULL,NULL),(1,19,NULL,NULL),(1,20,NULL,NULL),(1,21,NULL,NULL),(1,22,NULL,NULL),(1,23,NULL,NULL),(1,24,NULL,NULL),(1,25,NULL,NULL),(1,26,NULL,NULL),(1,27,NULL,NULL),(1,28,NULL,NULL),(1,29,NULL,NULL),(1,30,NULL,NULL),(1,31,NULL,NULL),(1,32,NULL,NULL),(1,33,NULL,NULL),(1,34,NULL,NULL),(1,35,NULL,NULL),(1,36,NULL,NULL),(1,37,NULL,NULL),(1,38,NULL,NULL),(1,39,NULL,NULL),(1,40,NULL,NULL),(1,41,NULL,NULL),(1,42,NULL,NULL),(1,43,NULL,NULL),(1,44,NULL,NULL),(1,45,NULL,NULL),(1,46,NULL,NULL),(1,47,NULL,NULL),(1,48,NULL,NULL),(1,49,NULL,NULL),(1,50,NULL,NULL),(1,51,NULL,NULL),(1,52,NULL,NULL),(1,53,NULL,NULL),(1,54,NULL,NULL),(1,55,NULL,NULL),(1,56,NULL,NULL),(1,57,NULL,NULL),(1,58,NULL,NULL),(1,59,NULL,NULL),(1,60,NULL,NULL),(1,61,NULL,NULL),(1,62,NULL,NULL),(1,63,NULL,NULL),(1,64,NULL,NULL),(1,65,NULL,NULL),(1,66,NULL,NULL),(1,67,NULL,NULL),(1,68,NULL,NULL),(1,69,NULL,NULL),(1,70,NULL,NULL),(1,71,NULL,NULL),(1,72,NULL,NULL),(1,73,NULL,NULL),(1,74,NULL,NULL),(1,75,NULL,NULL),(1,76,NULL,NULL),(1,77,NULL,NULL),(1,78,NULL,NULL),(1,79,NULL,NULL),(1,80,NULL,NULL),(1,81,NULL,NULL),(1,82,NULL,NULL),(1,83,NULL,NULL),(1,84,NULL,NULL),(1,85,NULL,NULL),(1,86,NULL,NULL),(1,87,NULL,NULL),(1,88,NULL,NULL),(1,89,NULL,NULL),(1,90,NULL,NULL),(1,91,NULL,NULL),(1,92,NULL,NULL),(1,93,NULL,NULL),(1,94,NULL,NULL),(1,95,NULL,NULL),(1,96,NULL,NULL),(1,97,NULL,NULL),(1,98,NULL,NULL),(1,99,NULL,NULL),(1,100,NULL,NULL),(1,101,NULL,NULL),(1,102,NULL,NULL),(1,103,NULL,NULL),(1,1,NULL,NULL),(1,2,NULL,NULL),(1,3,NULL,NULL),(1,4,NULL,NULL),(1,5,NULL,NULL),(1,6,NULL,NULL),(1,7,NULL,NULL),(1,8,NULL,NULL),(1,9,NULL,NULL),(1,10,NULL,NULL),(1,11,NULL,NULL),(1,12,NULL,NULL),(1,13,NULL,NULL),(1,14,NULL,NULL),(1,15,NULL,NULL),(1,16,NULL,NULL),(1,17,NULL,NULL),(1,18,NULL,NULL),(1,19,NULL,NULL),(1,20,NULL,NULL),(1,21,NULL,NULL),(1,22,NULL,NULL),(1,23,NULL,NULL),(1,24,NULL,NULL),(1,25,NULL,NULL),(1,26,NULL,NULL),(1,27,NULL,NULL),(1,28,NULL,NULL),(1,29,NULL,NULL),(1,30,NULL,NULL),(1,31,NULL,NULL),(1,32,NULL,NULL),(1,33,NULL,NULL),(1,34,NULL,NULL),(1,35,NULL,NULL),(1,36,NULL,NULL),(1,37,NULL,NULL),(1,38,NULL,NULL),(1,39,NULL,NULL),(1,40,NULL,NULL),(1,41,NULL,NULL),(1,42,NULL,NULL),(1,43,NULL,NULL),(1,44,NULL,NULL),(1,45,NULL,NULL),(1,46,NULL,NULL),(1,47,NULL,NULL),(1,48,NULL,NULL),(1,49,NULL,NULL),(1,50,NULL,NULL),(1,51,NULL,NULL),(1,52,NULL,NULL),(1,53,NULL,NULL),(1,54,NULL,NULL),(1,55,NULL,NULL),(1,56,NULL,NULL),(1,57,NULL,NULL),(1,58,NULL,NULL),(1,59,NULL,NULL),(1,60,NULL,NULL),(1,61,NULL,NULL),(1,62,NULL,NULL),(1,63,NULL,NULL),(1,64,NULL,NULL),(1,65,NULL,NULL),(1,66,NULL,NULL),(1,67,NULL,NULL),(1,68,NULL,NULL),(1,69,NULL,NULL),(1,70,NULL,NULL),(1,71,NULL,NULL),(1,72,NULL,NULL),(1,73,NULL,NULL),(1,74,NULL,NULL),(1,75,NULL,NULL),(1,76,NULL,NULL),(1,77,NULL,NULL),(1,78,NULL,NULL),(1,79,NULL,NULL),(1,80,NULL,NULL),(1,81,NULL,NULL),(1,82,NULL,NULL),(1,83,NULL,NULL),(1,84,NULL,NULL),(1,85,NULL,NULL),(1,86,NULL,NULL),(1,87,NULL,NULL),(1,88,NULL,NULL),(1,89,NULL,NULL),(1,90,NULL,NULL),(1,91,NULL,NULL),(1,92,NULL,NULL),(1,93,NULL,NULL),(1,94,NULL,NULL),(1,95,NULL,NULL),(1,96,NULL,NULL),(1,97,NULL,NULL),(1,98,NULL,NULL),(1,99,NULL,NULL),(1,100,NULL,NULL),(1,101,NULL,NULL),(1,102,NULL,NULL),(1,103,NULL,NULL);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `object` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'backend','dashboard','backend.dashboard','2018-08-06 16:52:20','2018-08-06 16:52:20'),(2,'category','index','category.index','2018-08-06 16:52:20','2018-08-06 16:52:20'),(3,'category','create','category.create','2018-08-06 16:52:20','2018-08-06 16:52:20'),(4,'category','store','category.store','2018-08-06 16:52:20','2018-08-06 16:52:20'),(5,'category','show','category.show','2018-08-06 16:52:20','2018-08-06 16:52:20'),(6,'category','edit','category.edit','2018-08-06 16:52:20','2018-08-06 16:52:20'),(7,'category','update','category.update','2018-08-06 16:52:20','2018-08-06 16:52:20'),(8,'category','destroy','category.destroy','2018-08-06 16:52:20','2018-08-06 16:52:20'),(9,'page','index','page.index','2018-08-06 16:52:20','2018-08-06 16:52:20'),(10,'page','create','page.create','2018-08-06 16:52:20','2018-08-06 16:52:20'),(11,'page','store','page.store','2018-08-06 16:52:20','2018-08-06 16:52:20'),(12,'page','show','page.show','2018-08-06 16:52:20','2018-08-06 16:52:20'),(13,'page','edit','page.edit','2018-08-06 16:52:21','2018-08-06 16:52:21'),(14,'page','update','page.update','2018-08-06 16:52:21','2018-08-06 16:52:21'),(15,'page','destroy','page.destroy','2018-08-06 16:52:21','2018-08-06 16:52:21'),(16,'post','index','post.index','2018-08-06 16:52:21','2018-08-06 16:52:21'),(17,'post','create','post.create','2018-08-06 16:52:21','2018-08-06 16:52:21'),(18,'post','store','post.store','2018-08-06 16:52:21','2018-08-06 16:52:21'),(19,'post','show','post.show','2018-08-06 16:52:21','2018-08-06 16:52:21'),(20,'post','edit','post.edit','2018-08-06 16:52:21','2018-08-06 16:52:21'),(21,'post','update','post.update','2018-08-06 16:52:21','2018-08-06 16:52:21'),(22,'post','destroy','post.destroy','2018-08-06 16:52:21','2018-08-06 16:52:21'),(23,'product','index','product.index','2018-08-06 16:52:21','2018-08-06 16:52:21'),(24,'product','create','product.create','2018-08-06 16:52:21','2018-08-06 16:52:21'),(25,'product','store','product.store','2018-08-06 16:52:21','2018-08-06 16:52:21'),(26,'product','show','product.show','2018-08-06 16:52:21','2018-08-06 16:52:21'),(27,'product','edit','product.edit','2018-08-06 16:52:21','2018-08-06 16:52:21'),(28,'product','update','product.update','2018-08-06 16:52:21','2018-08-06 16:52:21'),(29,'product','destroy','product.destroy','2018-08-06 16:52:21','2018-08-06 16:52:21'),(30,'metas','index','metas.index','2018-08-06 16:52:21','2018-08-06 16:52:21'),(31,'metas','create','metas.create','2018-08-06 16:52:21','2018-08-06 16:52:21'),(32,'metas','store','metas.store','2018-08-06 16:52:21','2018-08-06 16:52:21'),(33,'metas','show','metas.show','2018-08-06 16:52:21','2018-08-06 16:52:21'),(34,'metas','edit','metas.edit','2018-08-06 16:52:22','2018-08-06 16:52:22'),(35,'metas','update','metas.update','2018-08-06 16:52:22','2018-08-06 16:52:22'),(36,'metas','destroy','metas.destroy','2018-08-06 16:52:22','2018-08-06 16:52:22'),(37,'teams','index','teams.index','2018-08-06 16:52:22','2018-08-06 16:52:22'),(38,'teams','create','teams.create','2018-08-06 16:52:22','2018-08-06 16:52:22'),(39,'teams','store','teams.store','2018-08-06 16:52:22','2018-08-06 16:52:22'),(40,'teams','show','teams.show','2018-08-06 16:52:22','2018-08-06 16:52:22'),(41,'teams','edit','teams.edit','2018-08-06 16:52:22','2018-08-06 16:52:22'),(42,'teams','update','teams.update','2018-08-06 16:52:22','2018-08-06 16:52:22'),(43,'teams','destroy','teams.destroy','2018-08-06 16:52:22','2018-08-06 16:52:22'),(44,'gallery','index','gallery.index','2018-08-06 16:52:22','2018-08-06 16:52:22'),(45,'gallery','create','gallery.create','2018-08-06 16:52:22','2018-08-06 16:52:22'),(46,'gallery','store','gallery.store','2018-08-06 16:52:22','2018-08-06 16:52:22'),(47,'gallery','show','gallery.show','2018-08-06 16:52:22','2018-08-06 16:52:22'),(48,'gallery','edit','gallery.edit','2018-08-06 16:52:22','2018-08-06 16:52:22'),(49,'gallery','update','gallery.update','2018-08-06 16:52:22','2018-08-06 16:52:22'),(50,'gallery','destroy','gallery.destroy','2018-08-06 16:52:22','2018-08-06 16:52:22'),(51,'menu','index','menu.index','2018-08-06 16:52:22','2018-08-06 16:52:22'),(52,'menu','create','menu.create','2018-08-06 16:52:22','2018-08-06 16:52:22'),(53,'menu','store','menu.store','2018-08-06 16:52:22','2018-08-06 16:52:22'),(54,'menu','show','menu.show','2018-08-06 16:52:22','2018-08-06 16:52:22'),(55,'menu','edit','menu.edit','2018-08-06 16:52:22','2018-08-06 16:52:22'),(56,'menu','update','menu.update','2018-08-06 16:52:22','2018-08-06 16:52:22'),(57,'menu','destroy','menu.destroy','2018-08-06 16:52:22','2018-08-06 16:52:22'),(58,'user','index','user.index','2018-08-06 16:52:22','2018-08-06 16:52:22'),(59,'user','create','user.create','2018-08-06 16:52:22','2018-08-06 16:52:22'),(60,'user','store','user.store','2018-08-06 16:52:23','2018-08-06 16:52:23'),(61,'user','show','user.show','2018-08-06 16:52:23','2018-08-06 16:52:23'),(62,'user','edit','user.edit','2018-08-06 16:52:23','2018-08-06 16:52:23'),(63,'user','update','user.update','2018-08-06 16:52:23','2018-08-06 16:52:23'),(64,'user','destroy','user.destroy','2018-08-06 16:52:23','2018-08-06 16:52:23'),(65,'role','index','role.index','2018-08-06 16:52:23','2018-08-06 16:52:23'),(66,'role','create','role.create','2018-08-06 16:52:23','2018-08-06 16:52:23'),(67,'role','store','role.store','2018-08-06 16:52:23','2018-08-06 16:52:23'),(68,'role','show','role.show','2018-08-06 16:52:23','2018-08-06 16:52:23'),(69,'role','edit','role.edit','2018-08-06 16:52:23','2018-08-06 16:52:23'),(70,'role','update','role.update','2018-08-06 16:52:23','2018-08-06 16:52:23'),(71,'role','destroy','role.destroy','2018-08-06 16:52:23','2018-08-06 16:52:23'),(72,'faq','index','faq.index','2018-08-06 16:52:23','2018-08-06 16:52:23'),(73,'faq','create','faq.create','2018-08-06 16:52:23','2018-08-06 16:52:23'),(74,'faq','store','faq.store','2018-08-06 16:52:23','2018-08-06 16:52:23'),(75,'faq','show','faq.show','2018-08-06 16:52:23','2018-08-06 16:52:23'),(76,'faq','edit','faq.edit','2018-08-06 16:52:23','2018-08-06 16:52:23'),(77,'faq','update','faq.update','2018-08-06 16:52:23','2018-08-06 16:52:23'),(78,'faq','destroy','faq.destroy','2018-08-06 16:52:23','2018-08-06 16:52:23'),(79,'partner','index','partner.index','2018-08-06 16:52:23','2018-08-06 16:52:23'),(80,'partner','create','partner.create','2018-08-06 16:52:23','2018-08-06 16:52:23'),(81,'partner','store','partner.store','2018-08-06 16:52:23','2018-08-06 16:52:23'),(82,'partner','show','partner.show','2018-08-06 16:52:23','2018-08-06 16:52:23'),(83,'partner','edit','partner.edit','2018-08-06 16:52:23','2018-08-06 16:52:23'),(84,'partner','update','partner.update','2018-08-06 16:52:23','2018-08-06 16:52:23'),(85,'partner','destroy','partner.destroy','2018-08-06 16:52:23','2018-08-06 16:52:23'),(86,'testimonial','index','testimonial.index','2018-08-06 16:52:23','2018-08-06 16:52:23'),(87,'testimonial','create','testimonial.create','2018-08-06 16:52:23','2018-08-06 16:52:23'),(88,'testimonial','store','testimonial.store','2018-08-06 16:52:23','2018-08-06 16:52:23'),(89,'testimonial','show','testimonial.show','2018-08-06 16:52:24','2018-08-06 16:52:24'),(90,'testimonial','edit','testimonial.edit','2018-08-06 16:52:24','2018-08-06 16:52:24'),(91,'testimonial','update','testimonial.update','2018-08-06 16:52:24','2018-08-06 16:52:24'),(92,'testimonial','destroy','testimonial.destroy','2018-08-06 16:52:24','2018-08-06 16:52:24'),(93,'advertisement','index','advertisement.index','2018-08-06 16:52:24','2018-08-06 16:52:24'),(94,'advertisement','create','advertisement.create','2018-08-06 16:52:24','2018-08-06 16:52:24'),(95,'advertisement','store','advertisement.store','2018-08-06 16:52:24','2018-08-06 16:52:24'),(96,'advertisement','show','advertisement.show','2018-08-06 16:52:24','2018-08-06 16:52:24'),(97,'advertisement','edit','advertisement.edit','2018-08-06 16:52:24','2018-08-06 16:52:24'),(98,'advertisement','update','advertisement.update','2018-08-06 16:52:24','2018-08-06 16:52:24'),(99,'advertisement','destroy','advertisement.destroy','2018-08-06 16:52:24','2018-08-06 16:52:24'),(100,'contact','index','contact.index','2018-08-06 16:52:24','2018-08-06 16:52:24'),(101,'contact','show','contact.show','2018-08-06 16:52:24','2018-08-06 16:52:24'),(102,'comment','index','comment.index','2018-08-06 16:52:24','2018-08-06 16:52:24'),(103,'comment','show','comment.show','2018-08-06 16:52:24','2018-08-06 16:52:24'),(104,'setting','index','setting.index','2018-08-06 16:52:24','2018-08-06 16:52:24'),(105,'setting','store','setting.store','2018-08-06 16:52:24','2018-08-06 16:52:24');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Customers Reviews In Testimonials Services','customers-reviews-in-testimonials-services','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #555555; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; background-color: #fafafa;\">I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happier. Will definitely be coming back for more in the future.</span></p>\r\n</body>\r\n</html>','posts/customers-reviews-in-testimonials-services.jpg',NULL,NULL,'I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happ',1,2,1,1,0,'2018-06-22 05:53:42','2018-08-06 10:47:17'),(2,'Short natural product reviews.','short-natural-product-reviews','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">I will create a fast video review of your product or company.&nbsp;</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">This is a very natural looking review. It is NOT slick and sales.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">I will need access to your product.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">For Digital:</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">I will go to your website and be using Camtasia create a video of it, while I talk about the benefits of your product. This looks more realistic if I am actually inside the member\'s area.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">If there are certain points that you wish to cover, let me know.</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">Physical Product Reviews:</span><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><br style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\" /><span style=\"color: #0e0e0f; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">Please contact me first and then send the physical product. I will create the custom order after I receive it.</span></p>\r\n</body>\r\n</html>','posts/short-natural-product-reviews-this-is-me-reviewing-10-digital-or-physical-products.jpg',NULL,NULL,'I will create a fast video review of your product or company. \r\nThis is a very natural looking review. It is NOT slick and sales.\r\nI will need access to your product.',1,11,1,1,0,'2018-06-22 05:56:16','2018-08-06 15:45:20'),(3,'Outstanding Experience!','outstanding-experience','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><span style=\"color: #555555; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;\">The seller did a real and honest review of our product, as promised. She did it in the most timely way (note that we had to send her product and give her time to use it). The review is authentic. I recommend this gig!</span></p>\r\n</body>\r\n</html>','posts/outstanding-experience.jpg',NULL,NULL,'The seller did a real and honest review of our product, as promised. She did it in the most timely way (note that we had to send her product and give her time to use it). The review is authentic. I recommend this gig!',1,18,1,1,0,'2018-06-22 05:58:16','2018-08-06 10:50:29'),(4,'Test','test','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>',NULL,NULL,NULL,NULL,2,8,1,1,0,'2018-06-25 09:08:03','2018-08-06 15:45:13');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_gallery`
--

DROP TABLE IF EXISTS `product_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_gallery` (
  `product_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_gallery`
--

LOCK TABLES `product_gallery` WRITE;
/*!40000 ALTER TABLE `product_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `deal_price` double(8,2) DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `view` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Superadmin','2018-06-11 12:21:20','2018-06-11 12:21:20');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seos`
--

DROP TABLE IF EXISTS `seos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `visit` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seos`
--

LOCK TABLES `seos` WRITE;
/*!40000 ALTER TABLE `seos` DISABLE KEYS */;
INSERT INTO `seos` VALUES (1,'About','about','about','http://localhost:8000/about',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(2,'Contact','contact','contact','http://localhost:8000/contact',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(3,'Service','service','service','http://localhost:8000/service',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(4,'FAQ','faq','faq','http://localhost:8000/faq',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(5,'Product','product','product','http://localhost:8000/product',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(6,'Gallery','gallery','gallery','http://localhost:8000/gallery',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(7,'Post','post','post','http://localhost:8000/post',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(8,'Category','category','category','http://localhost:8000/category',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(9,'Career','career','career','http://localhost:8000/career',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(10,'Portfolio','portfolio','portfolio','http://localhost:8000/portfolio',NULL,NULL,NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(11,'Web Development','web-development','service','http://localhost:8000/web-development','Web Development in Nepal,create website,website services in Kathmandu, job-portal site,e-commerce site, enterprise management  system,news  portal website, Web development company in Nepal','We are competent with extensive experience on E-commerce, Job-portal, News-portal, Enterprise management system and many more with standards- based markup code for the creation of powerful, effective and engaging websites.',NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(12,'Web Designing','web-designing','service','http://localhost:8000/web-designing','Web Designing  company in Nepal, website creation, improve website  graphics,','We focus on making our work simple yet ingenious which is aesthetically appealing, highly responsive and functional with cohesive web designs to match your unique personality.',NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(13,'Mobile Application','mobile-application','service','http://localhost:8000/mobile-application','Mobile Application development in Nepal,mobile application company in Kathmandu, develop mobile application,','Reach your customers through mobile phones. Karkalo Tech will provide you with felicitous Mobile Application to meet your application requirements.',NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(14,'Web Hosting','web-hosting','service','http://localhost:8000/web-hosting','Web Hosting in Nepal,domain register in Nepal,how to host website,','Karkalo gives dependable, secured and best hosting services in Nepal at affordable price. Contact us for  superlative Web Hosting services.',NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(15,'Domain Register','domain-register','service','http://localhost:8000/domain-register','Domain Register in nepal, how to register domain,','Karkalo Tech provide domain services with traditional domains such as .com .edu .org .net .mil .gov  .int and new domain names along with geographical, second level domain and sub domains.',NULL,1,'2018-08-06 16:52:18','2018-08-06 16:52:18'),(16,'Search Engine Optimization(SEO)','search-engine-optimizationseo','service','http://localhost:8000/search-engine-optimizationseo','Seo services  in Kathmandu, search engine optimization services in Nepal, increase my website page rank, improve SEO,  increase google page rank','We are fully fledged to improve and increase  your website page ranking with felicitous Search Engine Optimization(SEO) as per your needs and targeted areas.Reach out to Karkalo Tech, Shankhamul, Kathmandu, Nepal.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(17,'Business Card Printing','business-card-printing','service','http://localhost:8000/business-card-printing','Business Card Design,Business Card Printing','Business cards printing services on high-quality paper at karkalo.com. Now Print 500 Business cards in 500 Rs only. Contact us for Business cards printing.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(18,'Logo Design','logo-design','service','http://localhost:8000/logo-design','Logo Design','We are catering logo design services form small to big organizations and companies in Nepal. Consideration upon your branding needs our team will offer tailored logo designs as well as enhance your custom needs.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(19,'T-shirt Printing','t-shirt-printing','service','http://localhost:8000/t-shirt-printing','T-shirt Printing','T-shirt Printing',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(20,'Brochure Printing & Flyers','brochure-printing-flyers','service','http://localhost:8000/brochure-printing-flyers','Flyer and Brochure','Flyer and Brochure',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(21,'ID Card Printing','id-card-printing','service','http://localhost:8000/id-card-printing','ID Card Printing','ID Card Printing',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(22,'Interior Design and Decor','interior-design-and-decor','service','http://localhost:8000/interior-design-and-decor','Interior Design and Decor','Interior Design and Decor',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(23,'LED Scrolling Board','led-scrolling-board','service','http://localhost:8000/led-scrolling-board','LED Scrolling Board in Nepal,','LED Scrolling Board is used to display moving message using LED that comes in various shapes and size,colors and fonts. Karkalo tech provides services at very reasonable cost for digital advertisement.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(24,'LED Video Screen','led-video-screen','service','http://localhost:8000/led-video-screen','LED Video Screen in Nepal, LED advertisement board in Nepal,video scrolling board in Nepal','The concept for digital advertisement in LED Video Screen is increasing progressively in Nepal.We provide Video screen with best quality that can be viewed from any angle .',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(25,'Acrylic Letter Board','acrylic-letter-board','service','http://localhost:8000/acrylic-letter-board','Acrylic Letter Board in Nepal,stylish letter for advertisement in nepal,designs for company name,','Karkalo provides Acrylic Letter Board with multiple colours and fonts with premium quality.You can also provide detailed specifications by visiting us in Kathmandu,Nepal.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(26,'Metal Letter Board','metal-letter-board','service','http://localhost:8000/metal-letter-board','Metal Letter Board in Nepal,metal letter in nepal,metal letter for advertising,metal letters for company name,stylish letter for organization name','We understand your advertisement needs of your company. Karkalo provides Metal Letter Board with sleek and eye catching finish with alluring designs.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(27,'Slim Light Box','slim-light-box','service','http://localhost:8000/slim-light-box','Slim Light Box in Nepal,light box in Nepal,advertisement light box in Kathmandu','We offer Slim Light Box with wide range of dimensions for light box for digital advertisement in Nepal.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(28,'Light Box and Flex Board','light-box-and-flex-board','service','http://localhost:8000/light-box-and-flex-board','Light and Flex Board','We offer Light Box and flex board for advertisement with wide range of dimensions that are durable and do not wear off in harsh environmental conditions of Nepal with attention grabbing output at standard market rates.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(29,'Display Advertising','display-advertising','category','http://localhost:8000/display-advertising','Advertisement Technology','Karkalo Pvt. Ltd. Limited is one of the leading companies among Creates new promotional ideas, designs, print, radio, television, and internet advertisements, book advertisement space and time, provide other such services that help a client',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(30,'Web Technology','web-technology','category','http://localhost:8000/web-technology',NULL,'We offer Web and Technology services in Nepal.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(31,'Graphic Design and Printing','graphic-design-and-printing','category','http://localhost:8000/graphic-design-and-printing','printing','Karkalo Tech provides all kinds of   Graphic design and printing services for every business needs.  We also offer business cards, letterhead, Company Profiles, Logo design, T-shirt print,  Cup Print in Nepal.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(32,'Customers Reviews In Testimonials Services','customers-reviews-in-testimonials-services','post','http://localhost:8000/customers-reviews-in-testimonials-services',NULL,'I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happ',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(33,'Short natural product reviews.','short-natural-product-reviews','post','http://localhost:8000/short-natural-product-reviews',NULL,'I will create a fast video review of your product or company. \r\nThis is a very natural looking review. It is NOT slick and sales.\r\nI will need access to your product.',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(34,'Outstanding Experience!','outstanding-experience','post','http://localhost:8000/outstanding-experience',NULL,'The seller did a real and honest review of our product, as promised. She did it in the most timely way (note that we had to send her product and give her time to use it). The review is authentic. I recommend this gig!',NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(35,'Test','test','post','http://localhost:8000/test',NULL,NULL,NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(36,'Test-10','test-10','gallery','http://localhost:8000/test-10',NULL,NULL,NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(37,'35151356_1873951802626562_3791355279300362240_n (1).png','35151356-1873951802626562-3791355279300362240-n-1png','gallery','http://localhost:8000/35151356-1873951802626562-3791355279300362240-n-1png',NULL,NULL,NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(38,'35151356_1873951802626562_3791355279300362240_n (1).png','35151356-1873951802626562-3791355279300362240-n-1png','gallery','http://localhost:8000/35151356-1873951802626562-3791355279300362240-n-1png',NULL,NULL,NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(39,'35043542_2086792994928510_1027257234041602048_n.jpg','35043542-2086792994928510-1027257234041602048-njpg','gallery','http://localhost:8000/35043542-2086792994928510-1027257234041602048-njpg',NULL,NULL,NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(40,'35151356_1873951802626562_3791355279300362240_n.png','35151356-1873951802626562-3791355279300362240-npng','gallery','http://localhost:8000/35151356-1873951802626562-3791355279300362240-npng',NULL,NULL,NULL,1,'2018-08-06 16:52:19','2018-08-06 16:52:19'),(41,'karkalo-google.jpg','karkalo-googlejpg','gallery','http://localhost:8000/karkalo-googlejpg',NULL,NULL,NULL,1,'2018-08-06 16:52:20','2018-08-06 16:52:20'),(42,'Graphic Designing and Printings','graphic-designing-and-printings','gallery','http://localhost:8000/graphic-designing-and-printings','Graphic Design and Printing','Graphic Design and Printing',NULL,1,'2018-08-06 16:52:20','2018-08-06 16:52:20'),(43,'Web technology for you','web-technology-for-you','gallery','http://localhost:8000/web-technology-for-you','Web and Technology','Web and Technology',NULL,1,'2018-08-06 16:52:20','2018-08-06 16:52:20'),(44,'Display your brand with 3D LED Letter.','display-your-brand-with-3d-led-letter','gallery','http://localhost:8000/display-your-brand-with-3d-led-letter','Acrylic Board','Our product range includes Advertising Letters such as Solid Acrylic Letter, Acrylic Raised Letters, Acrylic Cutting Letter, Stainless Steel Letter, Acrylic Letter, Indoor Reception Acrylic Letter.',NULL,1,'2018-08-06 16:52:20','2018-08-06 16:52:20');
/*!40000 ALTER TABLE `seos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'company_name','Karkalo Tech Pvt. Ltd.','Company Name','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(2,'email','info@karkalo.com','Email Address','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(3,'phone','+977-01-4786970','Phone Number','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(4,'mobile','+977-9847502170','Mobile Number','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(5,'address','Shankhamul Road, Kathmandu, Nepal','Address','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(6,'facebook','https://facebook.com/karkalopvtltd','Facebook','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(7,'twitter','https://twitter.com/karkalo','Twitter','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(8,'google','https://plus.google.com/user/karkalo','Google Plus','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(9,'linkedin','https://linkedin.com/karkalo','Linkedin','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(10,'about','Karkalo Tech is the professional organization providing high-end solutions and services in the domains of business.','About','text','2018-06-11 12:21:22','2018-07-31 07:44:13'),(11,'working_hour','Mon- Fri: 10:00 AM - 6:00 PM','Working Hour','text','2018-06-11 12:21:22','2018-06-11 12:21:22'),(12,'longitude','27.6856937','Map Longitude','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(13,'latitude','85.2645901','Map Laltitude','text','2018-06-11 12:21:22','2018-07-31 07:43:39'),(14,'copy_right','copyright @2016, <a href=\"https://karkalo.com\">Karkalo Pvt. Ltd.</a>. All Rights Reserved.','Copy Right','text','2018-06-11 12:21:22','2018-07-31 07:43:39');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribes`
--

DROP TABLE IF EXISTS `subscribes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribes`
--

LOCK TABLES `subscribes` WRITE;
/*!40000 ALTER TABLE `subscribes` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'Bikash Silwal','Project Manager','bikash-silwal','teams/bikash-silwal.jpeg','bikash@karkalo.com','9847502170','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci sed quia non numquam modi tempora eius.','https://facebook.com/bsilwal2','https://twitter.com/xilwal','https://plus.google.com/xilwal','https://linkedin.com/xilwal',1,'2018-06-22 10:17:36','2018-06-22 10:17:36'),(2,'Raj Bhatta','Frontend Developer','raj-bhatta','teams/raj-bhatta.png','raj@karkalo.com','98502354646','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci sed quia non numquam modi tempora eius.',NULL,NULL,NULL,NULL,1,'2018-06-22 10:22:13','2018-06-22 10:22:13'),(3,'Kosish Kandel','Sales Manager','kosish-kandel','teams/kosish-kandel.jpg','kocs2kocs@karkalo.com','784623256623','Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci sed quia non numquam modi tempora eius.',NULL,NULL,NULL,NULL,1,'2018-06-22 10:37:03','2018-06-22 10:37:03');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `testimonial` text COLLATE utf8mb4_unicode_ci,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'Bikash Silwal','Web Developer','Karkalo Tech','I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happier. Will definitely be coming back for more in the future.','testimonial/bikash-silwal.jpeg',1,'2018-06-22 05:50:38','2018-06-22 05:50:38'),(2,'Raj Bhatta','CEO','Bhatta and Co.','I\'ve worked with some really great sellers on Fiverr, but this might have been one of the best experiences I\'ve ever had. Seriously. So responsive, such great value for money. My video turned out exactly how I wanted, and I couldn\'t be happier. Will definitely be coming back for more in the future.','testimonial/raj-bhatta.png',1,'2018-06-22 05:52:58','2018-06-22 05:52:58');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Tim Xilwal','dev.xilwal@gmail.com','$2y$10$dpFXJtwb2FisQsWfJ3noL.8izUOPh1LYbhpWcsjfe08j/om2bPi.6',1,1,'Ildh4wWqd2UVNDd8CURBvUNdIMRfdgFuaM3CuAYqQeVQ0K6HHARR2wkZlSEk','2018-06-11 12:21:21','2018-06-11 12:21:21'),(2,'Sandesh Paudel','sanzciz@outlook.com','$2y$10$pwQ4DunGo5JyUvO7UKpbT.QV/54fk0ITQm0.jhch/RIRjCooq18qe',1,1,NULL,'2018-06-27 05:32:02','2018-07-31 05:01:04'),(3,'Bibek Adhakari','bibek.adhikari007@gmail.com','$2y$10$IVoo7Yt.EKVCyigB44i6KOzP/re3NwhjTh41Ga0lK8WVuBD/8VEXi',1,1,'4CCuBNG49qnqKhwHj9nbBlP4ALogDOv7bP0exUz1FaQ8GkAvwRu5yujChcss','2018-06-29 05:18:29','2018-07-31 04:56:33');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-06 22:37:25
